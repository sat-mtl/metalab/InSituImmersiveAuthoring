### About
This repository holds a few Blender addons geared toward adding immersive capabilities to Blender.

### License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version (http://www.gnu.org/licenses/).

### Authors
* François Ubald Brien
* Emmanuel Durand
* Michal Seta
* Jérémie Soria

### Projet URL
This project can be found on the [SAT Metalab repository](https://paperManu@gitlab.com/sat-metalab/InSituImmersiveAuthoring.git)

### Sponsors
This project is made possible thanks to the [Society for Arts and Technologies](http://www.sat.qc.ca) (also known as SAT).
Thanks to the Ministère du Développement économique, de l'Innovation et de l'Exportation du Québec (MDEIE).
