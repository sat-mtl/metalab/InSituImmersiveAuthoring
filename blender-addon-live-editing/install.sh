#!/bin/bash
cd live-editing
version=`blender --version | egrep -o "Blender [0-9.]+" | egrep -o "[0-9.]+"`
mkdir -p ~/.config/blender/$version/scripts/addons/metalab-live-editing
cp -rv * ~/.config/blender/$version/scripts/addons/metalab-live-editing/
