#!/bin/bash
cd live-editing
version=`blender --version | egrep -o "Blender [0-9.]+" | egrep -o "[0-9.]+"`
mkdir -p ~/Library/Application\ Support/Blender/$version/scripts/addons/metalab-live-editing
cp -rv * ~/Library/Application\ Support/Blender/$version/scripts/addons/metalab-live-editing/
