import os

FONT_BASE_PATH = os.path.dirname(os.path.realpath(__file__)) + "/assets/fonts/"
FONT_PPM = 100  # Pixels-per-meter

DEBUG_MACHINE = False
DEBUG_ENGINE = False
DEBUG_GL_GEO = False
DEBUG_GL_TEX = False
DEBUG_LAYOUT = False
DEBUG_LAYOUT_VISUAL = False
DEBUG_STATES = False
LOG_STATES = False
DEBUG_CONTROLLERS = False
SHOW_WAND = False
DEBUG_VRPN = False
DEBUG_VRPN_BUTTON = False
DEBUG_VRPN_ANY = DEBUG_VRPN or DEBUG_VRPN_BUTTON
DEBUG_FPS = False
PROFILE = False
PROFILE_SAMPLES = 100
PROFILE_MACHINE = False
PROFILE_ENGINE = False
ENABLE_VRPN = True
EXIT_ON_LOOP_CONFLICT = True