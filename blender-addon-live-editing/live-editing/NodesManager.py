import bpy
from bpy.types import NodeTree

import nodeitems_utils
from nodeitems_utils import NodeCategory, NodeItem

from .nodes import *


class LiveEditingTree(NodeTree):
    """Tree used to configure interactions with the scene"""

    bl_idname = 'LiveEditingTree'
    bl_label = 'Live Editing Tree'
    bl_icon = 'POSE_HLT'

    def update(self):
        for node in self.nodes:
            if isinstance(node, LiveEditingBaseNode):
                node.treeUpdate(self)

    def run(self):
        # Pre Run
        for node in self.nodes:
            if isinstance(node, LiveEditingBaseNode):
                node.preRun()

        # Run
        for node in self.nodes:
            if isinstance(node, LiveEditingOutputNode):
                node._run()


class LiveEditingNodeCategory(NodeCategory):
    """Node Category for the Live Editing Plugin"""
    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == "LiveEditingTree"


def walk_trees(scene):
    for tree in bpy.data.node_groups:
        if tree.users > 0 and tree.bl_idname == "LiveEditingTree":
            tree.run()


node_categories = [
    LiveEditingNodeCategory("MATH_NODES", "Math", items=[
        NodeItem("LogicNode"),
        NodeItem("ComparatorNode"),
        NodeItem("IntegratorNode"),
        NodeItem("DeltaAccumulatorNode"),
        NodeItem("OperationNode"),
        NodeItem("QuaternionOperationNode"),
        NodeItem("QuaternionDeltaAccumulatorNode"),
        NodeItem("VectorDeltaAccumulatorNode"),
        NodeItem("VectorDistanceNode"),
        NodeItem("VectorOperationNode"),
        NodeItem("RotateVectorNode"),
        NodeItem("SetVectorMagnitudeNode"),
        NodeItem("QuaternionToEulerXYZNode"),
        NodeItem("EulerXYZToQuaternionNode"),
        NodeItem("XYZToVectorNode"),
        NodeItem("VectorToXYZNode")
    ]),
    LiveEditingNodeCategory("GEOM_NODES", "Geom", items=[
        NodeItem("LocalToWorldNode"),
        NodeItem("WorldToLocalNode"),
        NodeItem("ComponentsToMatrixNode")
    ]),
    LiveEditingNodeCategory("EDITING_NODES", "Editing", items=[
        NodeItem("SelectObjectNode"),
        NodeItem("ObjectInfoNode"),
        NodeItem("SceneRayCastNode"),
        NodeItem("ObjectRayCastNode"),
        NodeItem("DomeRayCastNode"),
        NodeItem("MeshRayCastNode"),
        NodeItem("ObjectGrabberNode"),
        NodeItem("ImmersiveObjectGrabberNode"),
        NodeItem("ObjectManipulatorNode"),
        NodeItem("RotateInMatrixSpaceNode"),
        NodeItem("ObjectDuplicateNode")
    ]),
    LiveEditingNodeCategory("UTILS_NODES", "Utils", items=[
        NodeItem("CaptureValueNode"),
        NodeItem("CaptureVectorNode"),
        NodeItem("CaptureQuaternionNode"),
        NodeItem("InputSplitterNode"),
        NodeItem("SwitchNode"),
        NodeItem("TestNode")
    ]),
    LiveEditingNodeCategory("GROUP_NODES", "Groups", items=[
        NodeItem("LiveEditingNodeGroup"),
    ])
]

from .nodes import vrpn
if vrpn.hasVRPN is True:
    node_categories.append(
        LiveEditingNodeCategory("VRPN_NODES", "VRPN", items=[
            NodeItem("VRPNTrackerNode"),
            NodeItem("VRPNAnalogNode"),
            NodeItem("VRPNButtonNode")
        ])
    )

if joystick.hasJoystick is True:
    node_categories.append(
        LiveEditingNodeCategory("JOYSTICK_NODES", "Joystick", items=[
            NodeItem("JoystickNode"),
        ])
    )


def register():
    try:
        nodeitems_utils.register_node_categories("LIVE_EDITING_NODES", node_categories)
    except:
        nodeitems_utils.unregister_node_categories("LIVE_EDITING_NODES")
        nodeitems_utils.register_node_categories("LIVE_EDITING_NODES", node_categories)


def unregister():
    nodeitems_utils.unregister_node_categories("LIVE_EDITING_NODES")
