from bpy.types import PropertyGroup, AddonPreferences
from bpy.props import BoolProperty, StringProperty, FloatProperty, FloatVectorProperty, PointerProperty
from mathutils import Matrix


class VRPNSettings(PropertyGroup):
    """VRPN Settings"""

    connected = BoolProperty(
        default=False
    )

    host = StringProperty(
        name="VRPN Host",
        description="VRPN Server hostname/ip and port",
        default="localhost"
    )

    controller_1 = StringProperty(
        name="Controller 1",
        description="Controller 1 Device Name",
        default="openvr/controller/1"
    )

    controller_2 = StringProperty(
        name="Controller 2",
        description="Controller 2 Device Name",
        default="openvr/controller/2"
    )


class TrackingSettings(PropertyGroup):
    """Tracking Settings"""

    tracking_space_position = FloatVectorProperty(
        name="Tracking Space Position",
        description="Position of the tracking space in the scene",
        size=3,
        subtype='TRANSLATION',
        default=(0.00, 0.00, 0.00)
    )

    tracking_space_rotation = FloatVectorProperty(
        name="Tracking Space Rotation",
        description="Rotation of the tracking space in the scene",
        size=3,
        subtype='EULER',
        default=(0.00, 0.00, 0.00)
    )

    tracker_rotation = FloatVectorProperty(
        name="Tracker Rotation",
        description="Rotation offset the tracker in the scene",
        size=3,
        subtype='EULER',
        default=(0.00, 0.00, 0.00)
    )


class ProjectionSettings(PropertyGroup):
    """Projection Settings"""

    mesh = StringProperty(
        name="Mesh",
        description="Mesh used for the projection mapping",
        default="Mapping Mesh"
    )

    pod = StringProperty(
        name="Pod Group",
        description="Group name of the objects following the user in the immersive space",
        default="[Pod]"
    )

    pod_camera = StringProperty(
        name="Pod Camera",
        description="Virtual camera/projector location",
        default="Pod Camera"
    )

    pod_camera_rotation = FloatVectorProperty(
        name="Pod Camera Rotation",
        description="Rotation offset the pod camera",
        size=3,
        subtype='EULER',
        default=(0.00, 0.00, 0.00)
    )

    cursor_camera = StringProperty(
        name="Cursor Camera",
        description="Camera looking at the cursor",
        default="Cursor Camera"
    )

    camera = StringProperty(
        name="Camera",
        description="Main Camera",
        default="Camera"
    )

    ui_scale = FloatProperty(
        name="UI Scale",
        description="Scaling of the UI elements",
        default=1.00
    )


class UISettings(PropertyGroup):
    """Interface Settings"""

    ui_scale = FloatProperty(
        name="UI Scale",
        description="Scaling of the UI elements",
        default=1.00
    )


class GallerySettings(PropertyGroup):
    """Gallery Settings"""

    import os
    assets_path = StringProperty(
        name="Assets gallery folder",
        description="Folder that will be parsed for assets",
        default=os.path.join(os.path.expanduser("~"), 'Pictures/assets_insitu'),
        subtype='DIR_PATH'
    )

    join_models = BoolProperty(
        name="Join 3D models",
        description="Join 3D models when importing them",
        default=False
    )


class LiveEditingAddonPreferences(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __package__

    vrpn = PointerProperty(type=VRPNSettings)
    tracking = PointerProperty(type=TrackingSettings)
    projection = PointerProperty(type=ProjectionSettings)
    ui = PointerProperty(type=UISettings)
    gallery = PointerProperty(type=GallerySettings)


class StateSettings(PropertyGroup):
    """State Settings"""

    show_axis = BoolProperty(
        name="Show axis",
        description="Show axis when editing objects",
        default=False
    )

    show_axis_ref = StringProperty(
        name="Show axis ref",
        description="Axis shown when editing objects",
        default='WORLD'
    )

    lock_x_axis = BoolProperty(
        name="Lock X Axis",
        description="Lock X Axis",
        default=False
    )

    lock_y_axis = BoolProperty(
        name="Lock Y Axis",
        description="Lock Y Axis",
        default=False
    )

    lock_z_axis = BoolProperty(
        name="Lock Z Axis",
        description="Lock Z Axis",
        default=False
    )

    paint_brush_size = FloatProperty(
        name="Paint Brush Size",
        description="Size of the paint brush",
        default=10
    )

    sculpt_brush_size = FloatProperty(
        name="Sculpt Brush Size",
        description="Size of the sculpt brush",
        default=10
    )


class LiveEditingSceneSettings(PropertyGroup):
    """Live Editing settings for scenes"""

    setup = BoolProperty(
        name="Setup",
        description="Flag to check if the scene is setup and ready to be used",
        default=False
    )

    saved_pod_matrix = FloatVectorProperty(
        name="Saved pod matrix world",
        description='Matrix to apply when calling setup live scene if it is set',
        size=16,
        subtype='MATRIX',
        default=[Matrix.Identity(4)[j][i] for i in range(4) for j in range(4)]
    )


class LiveEditingObjectSettings(PropertyGroup):
    """Live Editing settings for objects"""

    lock = BoolProperty(
        name="Lock",
        description="Lock object from immersive editing",
        default=False
    )

    non_editable = BoolProperty(
        name="Non-editable",
        description="Makes the object non-editable (but still transformable)",
        default=False
    )
