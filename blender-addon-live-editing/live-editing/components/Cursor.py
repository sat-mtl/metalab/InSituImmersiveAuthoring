import bpy
import math
from mathutils import Matrix, Vector
from ..engine import Object3D, TextField
from ..engine.geometry import Shape
from ..engine.geometry.shapes import Circle
from ..engine.materials import LineBasicMaterial
from ..utils import Colors


class Chevron(Object3D):
    def __init__(self):
        super().__init__(
            geometry=Shape([
                -0.50, 0.00, 0.50,
                0.00, 0.00, 0.00,
                -0.50, 0.00, -0.50
            ]),
            material=LineBasicMaterial(color=(1.0, 0.75, 0.05))
        )


class Cursor(Object3D):

    def __init__(
        self,
        label="",
        detail="",
        radius=1.0,
        width=0.02,
        text_color=(1.00, 1.00, 1.00),
        color=(1.00, 1.00, 1.00),
        custom_cursor=None,
        selection_chevron=False,
        highlight_on_hover=False
    ):
        super().__init__()

        self._editor = None

        self._ignore_depth = True
        self._minimal = False
        self._width = width
        self._color = color
        self._highlight_on_hover = highlight_on_hover

        self._container = Object3D()
        self.add_child(self._container)

        self._cursor = Object3D(
            geometry=Circle(
                radius=radius,
                segments=64,
                line_width=2
            ),
            material=LineBasicMaterial()
        )
        self._cursor.material.color = self._color
        self._container.add_child(self._cursor)

        # Chevron
        self._chevron = None
        self._selection_chevron = False
        self.selection_chevron = selection_chevron  # Just to trigger the setter

        self._custom_cursor = custom_cursor
        if self._custom_cursor is not None:
            self._cursor.visible = False
            self._container.add_child(self._custom_cursor)

        self._label = TextField(text=label, color=text_color, size=.30, font="SourceSansPro-Bold.ttf", shadow=0.01)
        self._label.y = -1.25
        self._container.add_child(self._label)
        
        self._detail = TextField(text=detail, color=Colors.orange, size=.20, font="SourceSansPro-BoldItalic.ttf", shadow=0.01)
        self._detail.y = -1.5
        self._container.add_child(self._detail)

    @property
    def editor(self):
        return self._editor

    @property
    def custom_cursor(self):
        return self._custom_cursor

    @custom_cursor.setter
    def custom_cursor(self, value):
        if self._custom_cursor != value:
            if self._custom_cursor is not None:
                self.remove_child(self._custom_cursor)
            self._custom_cursor = value
            self._cursor.visible = self._custom_cursor is None
            if self._custom_cursor is not None:
                self._custom_cursor.material.color = self.color
                self.add_child(self._custom_cursor)

    @property
    def radius(self):
        return self._cursor.geometry.radius

    @radius.setter
    def radius(self, value):
        if self._cursor.geometry.radius != value:
            self._cursor.geometry.radius = value

    @property
    def color(self):
        return self._color
        #return self._cursor.material.color

    @color.setter
    def color(self, value):
        if self._color != value:
            self._color = value
            self._set_color(self._color)

    def _set_color(self, color):
        self._cursor.material.color = color
        if self._custom_cursor is not None:
            self._custom_cursor.material.color = color

    @property
    def label(self):
        return self._label.text

    @label.setter
    def label(self, value):
        if self._label.text != value:
            self._label.text = value

    @property
    def detail(self):
        return self._detail.text

    @detail.setter
    def detail(self, value):
        if self._detail.text != value:
            self._detail.text = value
    
    @property
    def minimal(self):
        return self._minimal

    @minimal.setter
    def minimal(self, value):
        if self._minimal != value:
            self._minimal = value
            self._set_minimal(self._minimal)

    def _set_minimal(self, minimal):
        self.radius = 1.0 if not minimal else 0.25
        self._label.visible = not minimal
        self._detail.visible = not minimal
        if self._selection_chevron and self._chevron is not None:
            self._chevron.visible = not minimal
        if self._custom_cursor is not None:
            self._cursor.visible = minimal
            self._custom_cursor.visible = not minimal

    @property
    def highlight_on_hover(self):
        return self._highlight_on_hover

    @highlight_on_hover.setter
    def highlight_on_hover(self, value):
        if self._highlight_on_hover != value:
            self._highlight_on_hover = value

    @property
    def selection_chevron(self):
        return self._selection_chevron

    @selection_chevron.setter
    def selection_chevron(self, value):
        if self._selection_chevron != value:
            if self._selection_chevron:
                self._cursor.remove_child(self._chevron)
                self._chevron = None
            self._selection_chevron = value
            if self._selection_chevron:
                self._chevron = Chevron()
                self._cursor.add_child(self._chevron)

    def on_menu_opened(self):
        self._set_minimal(True)
        self._set_color((1.0, 0.75, 0.05))

    def on_menu_closed(self):
        self._set_minimal(self._minimal)
        self._set_color(self._color)

    def update_cursor(self):
        if self._highlight_on_hover:
            self._set_color((1.0, 0.75, 0.05) if self.editor.picker.object is not None else self._color)
        else:
            self._set_color((1.0, 0.75, 0.05) if self.editor.picker.object == self.editor.selected_object else self._color)

        # Chevron-Arrow pointing selected object
        if self.editor.selected_object is not None and self._selection_chevron and self._chevron is not None and self._chevron.visible:
            # Calculate angle between cursor and target to determine if we show the chevron
            target = self.editor.selected_object.matrix_world.to_translation() - self.editor.pod_camera.matrix_world.translation
            ray_length = self.editor.picker.ray_vector.length
            angle_to_target = self.editor.picker.ray_vector.angle(target) if target.length > 0 and self.editor.picker.ray_vector.length > 0 else 0.00
            minimum_angle = 2 * math.atan((self.radius * 2 * self.editor.ui_scale) / (2 * ray_length)) if ray_length > 0 else 0.00
            if angle_to_target > minimum_angle:
                # Project target onto cursor plane
                v = self.editor.selected_object.matrix_world.to_translation() - self.editor.picker.cursor_matrix.translation
                dist = v.dot(self.editor.picker.ray_vector)
                proj_target = self.editor.selected_object.matrix_world.to_translation() - dist * self.editor.picker.ray_vector

                # Find angle in cursor space
                in_cursor_target = self.editor.picker.cursor_matrix.inverted() * proj_target
                in_cursor_target.z = 0  # Prevent negative values from messing out rotation direction
                cursor_angle = in_cursor_target.to_track_quat('X', 'Z').to_matrix().to_4x4()

                # Draw chevron

                # Round scale, because we don't need that much precision and it throws matrix comparisons into thinking
                # that the matrices change every frame even when not changing at all
                scale = round(math.log10(1 + ((angle_to_target - minimum_angle) / (math.pi / 2)) * 9), 3)
                offset = Matrix.Translation(Vector((self.radius + scale + 0.1, 0, 0)))

                # We have to include the ui_scale because we're changing the world matrix
                ui_scale_matrix = Matrix.Scale(self.editor.ui_scale, 4)
                chevron_matrix = self.editor.picker.cursor_matrix * cursor_angle * ui_scale_matrix * offset * Matrix.Scale(scale, 4) * Matrix.Rotation(math.pi / 2, 4, 'X')
                self._chevron.matrix_world = chevron_matrix
                self._chevron.visible = True
            else:
                self._chevron.visible = False
