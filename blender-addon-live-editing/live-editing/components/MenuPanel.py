from . import Panel
from ..flex.components import VGroup, Label


class MenuPanel(Panel):

    def __init__(self, title="Menu", gap=0.01):
        super().__init__()

        self.ignore_depth = True
        self._title = title
        self._gap = gap

        self._container = None
        self._title_label = None

        # Special case, we don't want to manage a datagroup
        self._menu_container = VGroup(gap=self._gap, horizontal_align='JUSTIFY')
        self._menu_container.padding_left = 0.125
        self._menu_container.padding_right = 0.125
        self._menu_container.padding_top = 0.0
        self._menu_container.padding_bottom = 0.125

    def create_children(self):
        super().create_children()

        self._container = VGroup(horizontal_align='JUSTIFY')
        self._container.left = 0
        self._container.right = 0
        self._container.top = 0
        self._container.bottom = 0
        self.add_element(self._container)

        self._title_label = Label(self._title, font="SourceSansPro-Black.ttf")
        self._title_label.padding_top = 0.25
        self._title_label.padding_bottom = 0.25
        self._title_label.padding_left = 0.125
        self._title_label.padding_right = 0.125
        self._container.add_element(self._title_label)

        self._container.add_element(self._menu_container)

    def add_item(self, component):
        return self._menu_container.add_element(component)

    def remove_item(self, component):
        return self._menu_container.remove_element(component)

    def remove_all_items(self):
        return self._menu_container.remove_all_elements()

    @property
    def items(self):
        return self._menu_container.children
