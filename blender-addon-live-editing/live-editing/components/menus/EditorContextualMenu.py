from . import ContextualMenu


class EditorContextualMenu(ContextualMenu):

    def __init__(self):
        super().__init__()

    @property
    def editor(self):
        return self._manager.editor if self._manager is not None else None

    @property
    def container(self):
        return self._container

    def update(self, time, delta):
        # XXX: Testing UI scaling
        self.scale.x = self.editor.ui_scale
        self.scale_y = self.editor.ui_scale
        self.scale_z = self.editor.ui_scale

        super().update(time, delta)
