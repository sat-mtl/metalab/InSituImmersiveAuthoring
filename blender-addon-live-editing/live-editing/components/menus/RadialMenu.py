from . import Menu
from .radial import RadialButton
from ...engine import Object3D


class RadialMenu(Object3D, Menu):

    def __init__(self, inner_radius=1.00, outer_radius=3.00):
        super().__init__()

        self.ignore_depth = True

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius

        self._items_changed_flag = True
        self._last_hovered = None

        self._items_container = Object3D()
        self.add_child(self._items_container)

    @property
    def last_hovered(self):
        return self._last_hovered

    def add_item(self, *args, **kwargs):
        return self.add_item_at(len(self._items_container.children), *args, **kwargs)

    def add_item_at(self, index, label, callback, toggle=False, data=None):
        item_button = RadialButton(
            label=label,
            toggle=toggle,
            data=data,
            on_click=callback,
            on_hover=self._item_hover,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius
        )
        self._items_container.add_child_at(item_button, index)
        self._items_changed_flag = True
        self._dirty_attributes = True
        return item_button

    def remove_item(self, item):
        self._items_container.remove_child(item)
        self._items_changed_flag = True
        self._dirty_attributes = True

    def remove_all_items(self):
        self._items_container.remove_all_children()
        self._items_changed_flag = True
        self._dirty_attributes = True

    def reset_selection(self):
        for item in self._items_container.children:
            item.highlighted = False

    def _item_hover(self, target):
        self.reset_selection()

        self._last_hovered = target

        if self._last_hovered is not None:
            self._last_hovered.highlighted = True

    def update_attributes(self):
        super().update_attributes()

        if self._items_changed_flag:
            self._items_changed_flag = False
            self._last_hovered = None
            num_items = len(self._items_container.children)
            for index, item in enumerate(self._items_container.children):
                item.index = index
                item.total = num_items

    def on_opening(self):
        for item in self._items_container.children:
            item.on_opening()

    def on_opened(self):
        for item in self._items_container.children:
            item.on_opened()

    def on_closing(self, callback):
        for item in self._items_container.children:
            item.on_closing()
        callback()

    def on_closed(self):
        for item in self._items_container.children:
            item.on_closed()
