import math
from mathutils import Vector, Euler
from ....engine import Object3D, TextField
from ....engine.geometry import Ring
from ....engine.geometry.shapes import Ring as RingShape
from ....engine.materials import LineBasicMaterial, MeshBasicMaterial
from ....utils import Colors


class RadialButton(Object3D):
    segments_per_radian = 12

    def __init__(
            self,
            label="",
            toggle=False,
            data=None,
            on_hover=None,
            on_click=None,
            index=0,
            total=1,
            inner_radius=1.00,
            outer_radius=3.00
    ):
        super().__init__()

        self._interactive = True  # Important!

        self._data = data
        self._on_hover = on_hover
        self._on_click = on_click
        self._index = index
        self._total = total
        self._inner_radius = inner_radius
        self._outer_radius = outer_radius

        # self._animate_opening = False
        # self._animate_opening_time = None
        # self._animate_closing = False
        # self._animate_closing_time = None
        # self._close_callback = None

        self._toggle = toggle
        self._selected = False
        self._highlighted = False
        self._enabled = True

        self._background = Object3D(geometry=Ring(), material=MeshBasicMaterial())
        self._background.material.opacity = 0.85
        self.add_child(self._background)

        self._stroke = Object3D(geometry=RingShape(line_width=2),
                                material=LineBasicMaterial(color=(1.00, 1.00, 1.00), opacity=0.125))
        self._add_child(self._stroke)

        self._text_field = TextField(text=label, font="SourceSansPro-Bold.ttf")
        self.add_child(self._text_field)

    # region Properties

    @property
    def toggle(self):
        return self._toggle

    @toggle.setter
    def toggle(self, value):
        if self._toggle != value:
            self._toggle = value
            self._dirty_attributes = True

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, value):
        if self._selected != value:
            self._selected = value
            self._dirty_attributes = True

    @property
    def highlighted(self):
        return self._highlighted

    @highlighted.setter
    def highlighted(self, value):
        if self._highlighted != value:
            self._highlighted = value
            self._dirty_attributes = True

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        if self._enabled != value:
            self._enabled = value
            self._dirty_attributes = True

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        if self._data != value:
            self._data = value

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        if self._index != value:
            self._index = value
            self._dirty_attributes = True

    @property
    def total(self):
        return self._total

    @total.setter
    def total(self, value):
        if self._total != value:
            self._total = value
            self._dirty_attributes = True

    @property
    def inner_radius(self):
        return self._inner_radius

    @inner_radius.setter
    def inner_radius(self, value):
        if self._inner_radius != value:
            self._inner_radius = value
            self._dirty_attributes = True

    @property
    def outer_radius(self):
        return self._outer_radius

    @outer_radius.setter
    def outer_radius(self, value):
        if self._outer_radius != value:
            self._outer_radius = value
            self._dirty_attributes = True

    # endregion

    def do_action(self):
        if not self._enabled:
            return
        if self._toggle:
            self.selected = not self._selected
        if self._on_click is not None and callable(self._on_click):
            self._on_click(self)

    def on_cursor_entered(self):
        super().on_cursor_entered()
        if not self._enabled:
            return
        if self._on_hover is not None and callable(self._on_hover):
            self._on_hover(self)

    def on_cursor_released(self):
        super().on_cursor_released()
        self.do_action()

    def on_opening(self):
        pass
        # self._animate_opening = True
        # self.visible = False

    def on_opened(self):
        pass

    def on_closing(self, callback=None):
        pass
        # # Cancel opening
        # self._animate_opening = False
        # self._animate_opening_time = None
        # # Close
        # self._close_callback = callback
        # self._animate_closing = True

    def on_closed(self):
        pass

    def update_attributes(self):
        super().update_attributes()

        # Start at top and go counter clockwise
        length = (math.pi * 2) / self._total
        start = ((1 - (self._index / self._total)) * (math.pi * 2)) + (
        math.pi / 2 - length / 2)  # 1 - position + offset_for_starting at top
        segments = round(length * RadialButton.segments_per_radian)

        center = Vector((((self._outer_radius - self._inner_radius) / 2) + self._inner_radius, 0.00, 0.00))
        center.rotate(Euler((0.00, 0.00, start + (length / 2))))
        self._text_field.location = center

        offset = Vector((0.0625, 0.00, 0.00))
        offset.rotate(Euler((0.00, 0.00, start + (length / 2))))
        self.location = offset

        self._background.geometry.inner_radius = self._inner_radius
        self._background.geometry.outer_radius = self._outer_radius
        self._background.geometry.theta_segments = segments
        self._background.geometry.theta_start = start
        self._background.geometry.theta_length = length

        self._stroke.geometry.inner_radius = self._inner_radius
        self._stroke.geometry.outer_radius = self._outer_radius
        self._stroke.geometry.theta_segments = segments
        self._stroke.geometry.theta_start = start
        self._stroke.geometry.theta_length = length

        if not self._enabled:
            self._background.material.opacity = 0.5
            self._background.material.color = (0.125, 0.125, 0.125)
            self._text_field.material.opacity = 0.5
            self._text_field.color = Colors.white
        elif self._cursor_over or self._highlighted:
            self._background.material.opacity = 1.00
            self._background.material.color = Colors.blue_accent
            self._text_field.material.opacity = 1.0
            self._text_field.color = (0.125, 0.125, 0.125)
        elif self._selected:
            self._background.material.opacity = 1.00
            self._background.material.color = Colors.yellow
            self._text_field.material.opacity = 1.0
            self._text_field.color = (0.125, 0.125, 0.125)
        else:
            self._background.material.opacity = 0.85
            self._background.material.color = (0.125, 0.125, 0.125)
            self._text_field.material.opacity = 1.0
            self._text_field.color = Colors.white

            # def update(self, time, delta):
            #
            #     if self._animate_opening:
            #         if self._animate_opening_time is None:
            #             self._animate_opening_time = time + (self._index * 0.03125)  # Delay time
            #
            #         progress = (time - self._animate_opening_time) / (0.0625 / self._total)
            #         scale = max(sys.float_info.epsilon, min(progress, 1.00))
            #         scale = math.log10(1 + scale * 9)
            #         self.scale_x = self.scale_y = scale
            #         self.visible = True
            #         if scale >= 1.00:
            #             self._animate_opening = False
            #             self._animate_opening_time = None
            #
            #     elif self._animate_closing:
            #         if self._animate_closing_time is None:
            #             self._animate_closing_time = time + (((self._total - 1 ) - self._index) * 0.03125)  # Delay time
            #         progress = (time - self._animate_closing_time) / (0.0625 / self._total)
            #         scale = max(0.00, min(progress, 1.00 - sys.float_info.epsilon))
            #         scale = 1 - math.log10(1 + scale * 9)
            #         self.scale_x = self.scale_y = scale
            #         if progress >= 1.00:
            #             self._animate_closing = False
            #             self._animate_closing_time = None
            #             if self.index == 0:
            #                 # Only have the last one call the callback
            #                 print("CLOSE")
            #                 self._close_callback()
            #             self._close_callback = None
            #
            #     super().update(time, delta)
