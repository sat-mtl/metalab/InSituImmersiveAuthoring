import bpy
from math import pi
from mathutils import Matrix, Vector, Quaternion, Euler
from .controllers import ButtonState
from .. import vrpn_devices
from ..Constants import DEBUG_CONTROLLERS
from ..utils import Logger

if vrpn_devices.vrpn_enabled:
    from ..vrpn_devices import VRPNClient, VRPNTracker, VRPNAnalog, VRPNButton


class Controller:

    MENU_BUTTON = 1
    GRAB_BUTTON = 2
    TRACKPAD_BUTTON = 32
    TRIGGER_BUTTON = 33
    TRACKPAD_X = 0
    TRACKPAD_Y = 1
    TRIGGER_ANALOG = 2

    def __init__(self):
        if DEBUG_CONTROLLERS:
            self._logger = Logger(self)

        self._preferences = bpy.context.user_preferences.addons[__package__.split('.')[0]].preferences

        self.host = None
        self.device = None
        self._connected = False
        self.original_location = Vector((0.00, 0.00, 0.00))
        self.original_rotation = Quaternion((1.00, 0.00, 0.00, 0.00))
        self.location = Vector((0.00, 0.00, 0.00))
        self.rotation = Quaternion((1.00, 0.00, 0.00, 0.00))
        self.matrix = Matrix.Identity(4)
        self._camera_rotation_offset = Euler((-pi / 2.0, 0.0, 0.0)).to_quaternion()

        if vrpn_devices.vrpn_enabled:
            self.tracker = VRPNTracker()
            self.analog = VRPNAnalog()
            self.button = VRPNButton()

    def connect(self, host, device):
        self.host = host
        self.device = device

        if vrpn_devices.vrpn_enabled:
            self.tracker.connect(self.host, self.device)
            self.analog.connect(self.host, self.device)
            self.button.connect(self.host, self.device)

        self._connected = True

    def disconnect(self):
        self._connected = False
        if vrpn_devices.vrpn_enabled:
            self.tracker.disconnect()
            self.analog.disconnect()
            self.button.disconnect()

    def before_update(self):
        if DEBUG_CONTROLLERS:
            self._logger.open("Before update")

        if not self._connected:
            if DEBUG_CONTROLLERS:
                self._logger.close("Not Connected!")
            return

        if vrpn_devices.vrpn_enabled:
            self.tracker.before_update()
            self.analog.before_update()
            self.button.before_update()

        if DEBUG_CONTROLLERS:
            self._logger.close()

    def update(self):
        if DEBUG_CONTROLLERS:
            self._logger.open("Update")

        if not self._connected:
            if DEBUG_CONTROLLERS:
                self._logger.close("Not Connected!")
            return

        if vrpn_devices.vrpn_enabled:
            self.tracker.update()
            self.analog.update()
            self.button.update()

            loc = self.tracker.location.get(0)
            if loc is not None:
                self.original_location = loc.copy()
            else:
                self.original_location = Vector((0.00, 0.00, 0.00))

            rot = self.tracker.rotation.get(0)
            if rot is not None:
                self.original_rotation = rot.copy()
            else:
                self.original_rotation = Quaternion((1.00, 0.00, 0.00, 0.00))

        else:
            """
            If VRPN is not supported use the device names as object names for location & rotation input
            This is useful when debugging without a VRPN setup (when VRPN is supported it is instead used as a preview)
            """
            device_object = bpy.data.objects.get(self.device)
            if device_object is not None:
                self.original_location = device_object.location.copy()
                orig_rot_mode = device_object.rotation_mode
                if device_object.rotation_mode != "QUATERNION":
                    device_object.rotation_mode = "QUATERNION"
                    self.original_rotation = device_object.rotation_quaternion.copy()
                device_object.rotation_mode = orig_rot_mode
            else:
                self.original_location = Vector((0.00, 0.00, 0.00))
                self.original_rotation = Quaternion((1.00, 0.00, 0.00, 0.00))

        camera = bpy.data.objects[self._preferences.projection.pod_camera]
        camera_rotation = camera.matrix_world.to_quaternion() * self._camera_rotation_offset
        tracking_space_rotation = camera_rotation * self._preferences.tracking.tracking_space_rotation.to_quaternion()

        location = self.original_location + self._preferences.tracking.tracking_space_position
        location.rotate(tracking_space_rotation)
        location += camera.matrix_world.to_translation()
        self.location = location

        rotation = self.original_rotation * self._preferences.tracking.tracker_rotation.to_quaternion()
        rotation = tracking_space_rotation * rotation
        self.rotation = rotation

        self.matrix = Matrix.Translation(self.location) * self.rotation.to_matrix().to_4x4()

        if vrpn_devices.vrpn_enabled:
            """
            If VRPN is supported, check if an object matches the device name and update its position & rotation
            to use it as a debug preview (when VRPN is unsupported it is instead used as input)
            """
            device_object = bpy.data.objects.get(self.device)
            if device_object is not None:
                device_object.matrix_world = Matrix.Translation(self.location) * self.rotation.to_matrix().to_4x4()

        if DEBUG_CONTROLLERS:
            self._logger.close()

    def get_button(self, button_id):
        if vrpn_devices.vrpn_enabled:
            button = self.button.buttons.get(button_id)
            if button is None:
                button = ButtonState()
                self.button.buttons[button_id] = button
            return button
        else:
            return ButtonState()

    def get_analog(self, channel_id):
        if vrpn_devices.vrpn_enabled:
            if channel_id < len(self.analog.channels):
                channel = self.analog.channels[channel_id]
                return channel if channel is not None else 0.0
            else:
                return 0.0
        else:
            return 0.0

    @property
    def trigger_button(self):
        return self.get_button(Controller.TRIGGER_BUTTON)

    @property
    def trackpad_button(self):
        return self.get_button(Controller.TRACKPAD_BUTTON)

    @property
    def menu_button(self):
        return self.get_button(Controller.MENU_BUTTON)

    @property
    def grab_button(self):
        return self.get_button(Controller.GRAB_BUTTON)

    @property
    def trackpad_x(self):
        return self.get_analog(Controller.TRACKPAD_X)

    @property
    def trackpad_y(self):
        return self.get_analog(Controller.TRACKPAD_Y)

    @property
    def trigger_analog(self):
        return self.get_analog(Controller.TRIGGER_ANALOG)
