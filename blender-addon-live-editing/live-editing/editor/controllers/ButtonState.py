class ButtonState:
    def __init__(self):
        self.up = True
        self.touched = False
        self.touching = False
        self.untouched = False
        self.pressed = False
        self.down = False
        self.released = False

    def __str__(self):
        return "Up: " + str(self.up) + ", " + \
               "Touched: " + str(self.touched) + ", " + \
               "Touching: " + str(self.touching) + ", " + \
               "Untouched: " + str(self.untouched) + ", " + \
               "Pressed: " + str(self.pressed) + ", " + \
               "Down: " + str(self.down) + ", " + \
               "Released: " + str(self.released)
