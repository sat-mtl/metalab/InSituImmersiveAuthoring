import bpy
import time
from mathutils import Vector
from OpenGL.GL import *
from ..utils import MatrixUtils
from ..utils.GLUtils import GLMat4x4
from . import Input, Program, Scene, Object3D, Geometry


class Engine:

    def __init__(self, editor=None):
        self._editor = editor

        self._input = Input(self)

        self._scene = Scene()
        self._scene.engine = self

        self._program = None
        self._gl_view_matrix = GLMat4x4()
        self._gl_proj_matrix = GLMat4x4()

        self._last_time = time.time()

    @property
    def editor(self):
        return self._editor

    @property
    def input(self):
        return self._input

    @property
    def program(self):
        return self._program

    @property
    def scene(self):
        return self._scene

    @scene.setter
    def scene(self, scene):
        if not isinstance(scene, Scene):
            raise Exception("Trying to set an invalid scene")

        scene.engine = self
        self._scene = scene

    def update_input(self):
        """
        Update the engine
        In the context of the state-machine loop, so this runs once per "frame" per scene
        
        :return:
        """
        self._input.update()
        if self._scene:
            self._scene.update_input()

    def select_program(self, program):
        if self._program == program:
            return

        self._program = program

        if self._program is not None:
            glUseProgram(self._program.id)
            glUniformMatrix4fv(self._program.proj_matrix_uniform, 1, GL_FALSE, self._gl_proj_matrix)
            glUniformMatrix4fv(self._program.view_matrix_uniform, 1, GL_FALSE, self._gl_view_matrix)
        else:
            glUseProgram(0)

    def render(self):
        """
        Render the current scene
        In the context of the POST_VIEW callback, will run *ONCE PER 3D VIEW*
        :return:
        """

        if self._scene is None:
            return

        now = time.time()
        delta = now - self._last_time
        self._scene.update(now, delta)
        self._last_time = now

        # self._gl_proj_matrix = GLMat4x4(*MatrixUtils.flatten(bpy.context.space_data.region_3d.perspective_matrix))
        MatrixUtils.flatten_in(bpy.context.space_data.region_3d.perspective_matrix, self._gl_proj_matrix)
        # self._gl_view_matrix = GLMat4x4(*MatrixUtils.flatten(bpy.context.space_data.region_3d.view_matrix))
        MatrixUtils.flatten_in(bpy.context.space_data.region_3d.view_matrix, self._gl_view_matrix)

        # BEGIN: Blending
        was_blending = glGetBoolean(GL_BLEND)
        if not was_blending:
            glEnable(GL_BLEND)
            glBlendEquation(GL_FUNC_ADD)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        self._scene.render()

        # END: Blending
        if not was_blending:
            glDisable(GL_BLEND)

        self.select_program(None)

        # Dispose of trash
        Object3D.empty_trash()
        Geometry.empty_trash()

        # print(glGetError())
