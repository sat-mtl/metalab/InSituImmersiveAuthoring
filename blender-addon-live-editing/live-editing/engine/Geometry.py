import bpy
from OpenGL.GL import *
from mathutils import Vector
from mathutils.bvhtree import BVHTree
from .utils import Bounds
from ..utils import Logger
from ..Constants import DEBUG_ENGINE, DEBUG_GL_GEO


class Geometry:

    # Quick and dirty bag of gl stuff to dispose (by context)
    # We can't access engine when disposing so using a static variable for now
    trash = {}

    @staticmethod
    def empty_trash():
        """
        Deletes VAOs for the current context
        :return: 
        """
        context_trash = Geometry.trash.get(bpy.context.screen.name)
        if context_trash is None:
            return
        count = len(context_trash)
        if count == 0:
            return
        if DEBUG_GL_GEO:
            print('\033[1;31mGEO -', bpy.context.screen.name, '\t', *context_trash, '\033[0m')
        glDeleteVertexArrays(count, context_trash)
        context_trash.clear()

    def __init__(self):
        if DEBUG_ENGINE:
            self._logger = Logger(self)

        self._vbo = -1
        self._vibo = -1
        self._nbo = -1
        self._vaos = {}
        self._ubo = -1
        self._vertices = []
        self._vertices_len = 0
        self._indices = []
        self._indices_len = 0
        self._normals = []
        self._uvs = []

        self._dirty_geometry = True

        self._grouped_vertices = []
        self._grouped_indices = []

        self._bounds = Bounds()
        self._bvh_tree = BVHTree()

    def dispose(self):
        if DEBUG_ENGINE:
            self._logger.log("dispose")
        self.clean_buffers()

    # region Properties

    @property
    def vertices(self):
        return self._vertices

    @property
    def indices(self):
        return self._indices

    @property
    def bounds(self):
        return self._bounds

    @property
    def bvh_tree(self):
        """
        BVH Tree use for ray casting
        :return:
        """
        return self._bvh_tree

    # endregion

    def clean_buffers(self):
        if DEBUG_ENGINE:
            self._logger.open("clean_buffers")

        if self._vbo != -1:
            glDeleteBuffers(1, [self._vbo])
            self._vbo = -1
        if self._vibo != -1:
            glDeleteBuffers(1, [self._vibo])
            self._vibo = -1
        if self._nbo != -1:
            glDeleteBuffers(1, [self._nbo])
            self._nbo = -1
        if self._ubo != -1:
            glDeleteBuffers(1, [self._ubo])
            self._ubo = -1
        if len(self._vaos) > 0:
            # Add the vaos to the contextual trash for later disposing
            # TODO: Find a way to to this synchronously, this will wait one frame to clear
            for context, vao in self._vaos.items():
                context_trash = Geometry.trash.get(context)
                if context_trash is None:
                    context_trash = []
                    Geometry.trash[context] = context_trash
                context_trash.append(vao)
            self._vaos = {}

        if DEBUG_ENGINE:
            self._logger.close()

    def update_geometry(self):
        """
        Override this method to update geometry
        :return:
        """
        pass

    def update_bounds(self, object_3d):
        first = True
        bounds_min = Vector()
        bounds_max = Vector()
        for vertex in self._grouped_vertices:
            if first:
                bounds_min = Vector(vertex)
                bounds_max = Vector(vertex)
                first = False
            else:
                bounds_min.x = min(bounds_min.x, vertex[0])
                bounds_min.y = min(bounds_min.y, vertex[1])
                bounds_min.z = min(bounds_min.z, vertex[2])
                bounds_max.x = max(bounds_max.x, vertex[0])
                bounds_max.y = max(bounds_max.y, vertex[1])
                bounds_max.z = max(bounds_max.z, vertex[2])
        self._bounds = Bounds(bounds_min, bounds_max)
        
        # Size validation happens after geometry, so we won't get another cycle just for this
        object_3d._dirty_size = True

    def _update_geometry(self, time, delta, object_3d):
        """
        Internal geometry update process, do not call/override this method
        :return:
        """

        if DEBUG_ENGINE:
            self._logger.open("update_geometry")

        # Make the overriding class update its vertices
        self.update_geometry()

        # Useful values
        # This is the fastest way according to http://stackoverflow.com/a/1625023
        self._grouped_vertices = list(zip(*(iter(self._vertices),) * 3))
        self._grouped_indices = list(zip(*(iter(self._indices),) * 3))

        # Self Bounding Box
        self.update_bounds(object_3d)

        # Cache a a bvh tree of the geometry, this is used by the ray casting code
        self._bvh_tree = self._bvh_tree.FromPolygons(
            self._grouped_vertices,
            self._grouped_indices,
            all_triangles=True,
            epsilon=0.0
        )

        # Clean the buffers
        self.clean_buffers()

        # Update the buffers

        # VBO
        self._vertices_len = len(self._vertices)
        self._vbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self._vbo)
        vertices = (GLfloat * self._vertices_len)
        glBufferData(GL_ARRAY_BUFFER, self._vertices_len * 4, vertices(*self._vertices), GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        # VIBO
        self._indices_len = len(self._indices)
        if self._indices_len > 0:
            self._vibo = glGenBuffers(1)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self._vibo)
            indices = (GLuint * self._indices_len)
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, self._indices_len * 4, indices(*self._indices), GL_STATIC_DRAW)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)

        # NBO
        if len(self._normals) > 0:
            self._nbo = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, self._nbo)
            normals = (GLfloat * len(self._normals))
            glBufferData(GL_ARRAY_BUFFER, len(self._normals) * 4, normals(*self._normals), GL_STATIC_DRAW)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

        # UBO
        if len(self._uvs) > 0:
            self._ubo = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, self._ubo)
            uvs = (GLfloat * len(self._uvs))
            glBufferData(GL_ARRAY_BUFFER, len(self._uvs) * 4, uvs(*self._uvs), GL_STATIC_DRAW)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

        self._dirty_geometry = False

        if DEBUG_ENGINE:
            self._logger.close()

    def _generate_vao(self, object_3d):
        if DEBUG_ENGINE:
            self._logger.open("generate_vao")

        vao = glGenVertexArrays(1)
        self._vaos[bpy.context.screen.name] = vao

        if DEBUG_GL_GEO:
            print('\033[1;32mGEO +', bpy.context.screen.name, '\t', vao, '\033[0m')

        glBindVertexArray(vao)
        glEnableVertexAttribArray(object_3d.self_or_inherited_material.program.position_attribute)
        glBindBuffer(GL_ARRAY_BUFFER, self._vbo)
        glVertexAttribPointer(object_3d.self_or_inherited_material.program.position_attribute, 3, GL_FLOAT, GL_FALSE, 0, None)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        if self._nbo != -1 and object_3d.self_or_inherited_material.program.normal_attribute != -1:
            glEnableVertexAttribArray(object_3d.self_or_inherited_material.program.normal_attribute)
            glBindBuffer(GL_ARRAY_BUFFER, self._nbo)
            glVertexAttribPointer(object_3d.self_or_inherited_material.program.normal_attribute, 3, GL_FLOAT, GL_FALSE, 0, None)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

        if self._ubo != -1 and object_3d.self_or_inherited_material.program.uv_attribute != -1:
            glEnableVertexAttribArray(object_3d.self_or_inherited_material.program.uv_attribute)
            glBindBuffer(GL_ARRAY_BUFFER, self._ubo)
            glVertexAttribPointer(object_3d.self_or_inherited_material.program.uv_attribute, 2, GL_FLOAT, GL_FALSE, 0, None)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

        if self._vibo != -1:
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self._vibo)

        glBindVertexArray(0)

        if DEBUG_ENGINE:
            self._logger.close()

        return vao

    def update(self, time, delta, object_3d):
        if self._dirty_geometry:
            self._update_geometry(time, delta, object_3d)

    def pre_draw(self, object_3d):
        if DEBUG_ENGINE:
            self._logger.open("pre_draw")

        vao = self._vaos.get(bpy.context.screen.name)
        if vao is None:
            # We changed context?
            if DEBUG_ENGINE:
                self._logger.log("context changed? generating a new vao")
            vao = self._generate_vao(object_3d)

        if DEBUG_GL_GEO:
            print('\033[1;30mGEO ', bpy.context.screen.name, '\t', vao, 'valid' if glIsVertexArray(vao) else 'NOT_A_VAO', '\033[0m')

        if vao is not None:
            glBindVertexArray(vao)

    def draw(self, object_3d):
        if DEBUG_ENGINE:
            self._logger.log("draw")

        glDrawElements(GL_TRIANGLES, self._indices_len, GL_UNSIGNED_INT, None)

    def post_draw(self, object_3d):
        glBindVertexArray(0)

        if DEBUG_ENGINE:
            self._logger.close("post_draw")
