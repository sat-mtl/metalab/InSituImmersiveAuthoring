import math
from mathutils import Vector
from . import Geometry


class Input:
    def __init__(self, engine):
        self._engine = engine
        self.ray_origin = Vector()
        self.ray_direction = Vector((0.0, 1.0, 0.0))

        self._last_interactive = None
        self._last_location = None
        self._last_down = None

    # @property
    # def controller(self):
    #     return self._controller

    def update(self):
        """
        Update inputs
        In the context of the state-machine loop
        :return:
        """
        # Interactive objects
        if self._engine.scene is not None:
            self.pick_interactive()

    def pick_interactive(self):
        current_interactive = None
        closest_interactive = None
        closest_child = None
        closest_child_distance = math.inf
        closest_child_pick_location = Vector((0.0, 0.0, 0.0))

        def pick(obj):
            nonlocal self
            nonlocal current_interactive
            nonlocal closest_interactive
            nonlocal closest_child
            nonlocal closest_child_distance
            nonlocal closest_child_pick_location

            # Only visible objects with geometry can be picked
            if not obj.visible or obj.geometry is None:
                return

            obj_inv_mat = obj.matrix_world.inverted_safe()
            obj_ray_loc = obj_inv_mat * self.ray_origin
            obj_ray_vec = ((obj_inv_mat * self.ray_direction) - obj_inv_mat.translation).normalized()
            location, normal, index, distance = obj.geometry.bvh_tree.ray_cast(obj_ray_loc, obj_ray_vec)
            if location is not None and distance < closest_child_distance:
                closest_child = obj
                closest_interactive = current_interactive
                closest_child_distance = distance
                closest_child_pick_location = location

        def find_interactive(obj):
            nonlocal current_interactive

            if not obj.visible or obj.block_interaction:
                return False  # break

            if not obj.interactive:
                return True  # continue

            # Do picking on object and children
            current_interactive = obj
            obj.traverse(pick)

            return False  # break
        self._engine.scene.traverse_shallow(find_interactive)

        # TODO: Picking should set hover state on parents also in order to manage which component has the focus
        # TODO: We should have access to controllers in order to get button status

        location = closest_child.matrix_world * closest_child_pick_location if closest_child is not None else Vector()
        down = self._engine.editor.controller1.trigger_button.down

        def hover(obj):
            nonlocal self, closest_interactive, location
            obj.set_hover(obj is closest_interactive, location, down)

        # Only update the whole tree when we change the focus
        if self._last_interactive != closest_interactive:
            self._engine.scene.traverse(hover)
            self._last_interactive = closest_interactive

        # Otherwise update the currently focused object when either location or button status changes
        elif closest_interactive is not None and (self._last_location != location or self._last_down != down):
            closest_interactive.set_hover(True, location, down)
            self._last_location = location
            self._last_down = down
