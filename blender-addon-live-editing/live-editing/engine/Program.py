import os
from OpenGL.GL import *


class Program:

    programs = {}

    @staticmethod
    def get_program(name):
        program = Program.programs.get(name)
        if program is None:
            program = Program(name)
            Program.programs[name] = program
        return program

    @staticmethod
    def compile_shader(source, type):
        """
        Compile a shader
        :param source:
        :param type:
        :return:
        """

        if isinstance(source, str):
            source = [source]
        shader = glCreateShader(type)
        glShaderSource(shader, source)

        try:
            glCompileShader(shader)
            result = glGetShaderiv(shader, GL_COMPILE_STATUS)
        except:
            result = None

        if not result:
            raise RuntimeError(
                """Shader compile failure (%s): %s""" % (
                    result,
                    glGetShaderInfoLog(shader),
                ),
                source,
                type,
            )
        return shader

    def __init__(self, name):
        self.vertex_shader = -1
        self.geometry_shader = -1
        self.fragment_shader = -1

        self.id = glCreateProgram()

        shaders_dir = os.path.dirname(os.path.realpath(__file__)) + "/shaders/"

        vertex_shader_path = shaders_dir + name + ".vert"
        if os.path.exists(vertex_shader_path):
            with open(vertex_shader_path) as shader:
                self.vertex_shader = Program.compile_shader(shader.read(), GL_VERTEX_SHADER)
                glAttachShader(self.id, self.vertex_shader)

        geometry_shader_path = shaders_dir + name + ".geom"
        if os.path.exists(geometry_shader_path):
            with open(geometry_shader_path) as shader:
                self.geometry_shader = Program.compile_shader(shader.read(), GL_GEOMETRY_SHADER)
                glAttachShader(self.id, self.geometry_shader)

        fragment_shader_path = shaders_dir + name + ".frag"
        if os.path.exists(fragment_shader_path):
            with open(fragment_shader_path) as shader:
                self.fragment_shader = Program.compile_shader(shader.read(), GL_FRAGMENT_SHADER)
                glAttachShader(self.id, self.fragment_shader)

        try:
            glLinkProgram(self.id)
            result = glGetProgramiv(self.id, GL_LINK_STATUS)
        except:
            result = None

        if not result:
            raise RuntimeError(
                """Program linking failure (%s): %s""" % (
                    result,
                    glGetProgramInfoLog(self.id),
                )
            )

        self.position_attribute = glGetAttribLocation(self.id, "vertex_position")
        self.normal_attribute = glGetAttribLocation(self.id, "vertex_normal")
        self.uv_attribute = glGetAttribLocation(self.id, "vertex_uv")

        self.view_matrix_uniform = glGetUniformLocation(self.id, "view_matrix")
        self.proj_matrix_uniform = glGetUniformLocation(self.id, "proj_matrix")
        self.object_matrix_uniform = glGetUniformLocation(self.id, "obj_matrix")
        self.line_color_uniform = glGetUniformLocation(self.id, "color")
        self.diffuse_uniform = glGetUniformLocation(self.id, "diffuse")
        self.data_uniform = glGetUniformLocation(self.id, "data")
