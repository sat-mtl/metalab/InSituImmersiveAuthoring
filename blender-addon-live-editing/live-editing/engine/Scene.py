from OpenGL.GL import *
from . import Object3D


class Scene:

    def __init__(self):
        self._engine = None
        self._children = []

    @property
    def engine(self):
        return self._engine

    @engine.setter
    def engine(self, value):
        self._engine = value
        #for child in self._children:
        #    child.engine = self._engine

    def add_child(self, child):
        """
        Add a child object to the scene
        :param child:
        :return:
        """

        if not isinstance(child, Object3D):
            raise Exception("Trying to add an invalid object to the scene")

        if child.scene is not None:
            child.scene.remove_child(child)

        child.scene = self
        self._children.append(child)

    def remove_child(self, child):
        """
        Remove a child from the scene
        :param child:
        :return:
        """

        if not isinstance(child, Object3D):
            raise Exception("Trying to remove an invalid object from the scene")

        self._children.remove(child)
        child.scene = None
        child._parent = None

    def traverse(self, callback):
        for child in self._children:
            child.traverse(callback)

    def traverse_shallow(self, callback):
        for child in self._children:
            child.traverse_shallow(callback)

    def update_input(self):
        """
        Update the scene's input
        This runs in the state machine's loop, not the renderer's
        :return:
        """

        # Update children
        for child in self._children:
            child.update_input()

    def update(self, time, delta):
        """
        Update the scene
        :return:
        """

        # Update children
        for child in self._children:
            child.update(time, delta)

    def render(self):
        """
        Render the scene
        :return:
        """

        # Draw children
        for child in self._children:
            child.render()
