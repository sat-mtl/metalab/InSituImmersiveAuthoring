import math
from .. import Geometry


class Circle(Geometry):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/CircleGeometry.js
    """

    def __init__(self, radius=0.5, segments=32, theta_start=0, theta_length=math.pi * 2):
        super().__init__()

        self._radius = radius
        self._segments = segments
        self._theta_start = theta_start
        self._theta_length = theta_length

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, value):
        if self._radius != value:
            self._radius = value
            self._dirty_geometry = True

    @property
    def segments(self):
        return self._segments

    @segments.setter
    def segments(self, value):
        segments = max(3.00, math.floor(value))
        if self._segments != segments:
            self._segments = segments
            self._dirty_geometry = True

    @property
    def theta_start(self):
        return self._theta_start

    @theta_start.setter
    def theta_start(self, value):
        if self._theta_start != value:
            self._theta_start = value
            self._dirty_geometry = True

    @property
    def theta_length(self):
        return self._theta_length

    @theta_length.setter
    def theta_length(self, value):
        if self._theta_length != value:
            self._theta_length = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        self._indices = []
        self._vertices = []
        self._normals = []
        self._uvs = []

        # helper variables
        vertex = [0.00, 0.00, 0.00]
        uv = [0.00, 0.00]

        # center point

        self._vertices.extend([0, 0, 0])
        self._normals.extend([0, 0, 1])
        self._uvs.extend([0.5, 0.5])

        i = 0
        for s in range(0, self._segments + 1):
            i += 3
            segment = self._theta_start + s / self._segments * self._theta_length

            # vertex
            vertex[0] = self._radius * math.cos(segment)
            vertex[1] = self._radius * math.sin(segment)
            self._vertices.extend(vertex)

            # normal
            self._normals.extend([0, 0, 1])

            # uvs
            uv[0] = (self._vertices[i] / self._radius + 1) / 2
            uv[1] = (self._vertices[i + 1] / self._radius + 1) / 2
            self._uvs.extend(uv)

        # indices
        for i in range(1, self._segments + 1):
            self._indices.extend([i, i + 1, 0])
