import math
from mathutils import Vector
from .. import Geometry


class Cylinder(Geometry):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/CylinderGeometry.js
    """

    def __init__(self, radius_top=0.5, radius_bottom=0.5, height=1, radial_segments=32, height_segments=1,
                 open_ended=False, theta_start=0, theta_length=2.0 * math.pi):
        super().__init__()

        self._radius_top = radius_top
        self._radius_bottom = radius_bottom
        self._height = height
        self._radial_segments = radial_segments
        self._height_segments = height_segments
        self._open_ended = open_ended
        self._theta_start = theta_start
        self._theta_length = theta_length

        self._half_height = self._height / 2
        self._index = 0
        self._index_offset = 0
        self._index_array = []

    @property
    def radius_top(self):
        return self._radius_top

    @radius_top.setter
    def radius_top(self, value):
        if self._radius_top != value:
            self._radius_top = value
            self._dirty_geometry = True

    @property
    def radius_bottom(self):
        return self._radius_bottom

    @radius_bottom.setter
    def radius_bottom(self, value):
        if self._radius_bottom != value:
            self._radius_bottom = value
            self._dirty_geometry = True

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        if self._height != value:
            self._height = value
            self._dirty_geometry = True

    @property
    def radial_segments(self):
        return self._radial_segments

    @radial_segments.setter
    def radial_segments(self, value):
        radial_segments = max(8.0, math.floor(value))
        if self._radial_segments != radial_segments:
            self._radial_segments = radial_segments
            self._dirty_geometry = True

    @property
    def height_segments(self):
        return self._height_segments

    @height_segments.setter
    def height_segments(self, value):
        height_segments = max(1.0, math.floor(value))
        if self._height_segments != height_segments:
            self._height_segments = height_segments
            self._dirty_geometry = True

    @property
    def open_ended(self):
        return self._open_ended

    @open_ended.setter
    def open_ended(self, value):
        if self.open_ended != value:
            self._open_ended = value
            self._dirty_geometry = True

    @property
    def theta_start(self):
        return self._theta_start

    @theta_start.setter
    def theta_start(self, value):
        if self._theta_start != value:
            self._theta_start = value
            self._dirty_geometry = True

    @property
    def theta_length(self):
        return self._theta_length

    @theta_length.setter
    def theta_length(self, value):
        if self._theta_length != value:
            self._theta_length = value
            self._dirty_geometry = True

    def generate_torso(self):
        normal = Vector()
        vertex = [0.00, 0.00, 0.00]

        # this will be used to calculate the normal
        slope = (self._radius_bottom - self._radius_top) / self._height

        # generate vertices, normals and uvs
        for y in range(0, self._height_segments + 1):
            index_row = []
            v = y / self._height_segments

            # calculate the radius of the current row
            radius = v * (self._radius_bottom - self._radius_top) + self._radius_top

            for x in range(0, self._radial_segments + 1):
                u = x / self._radial_segments
                theta = u * self._theta_length + self._theta_start
                sin_theta = math.sin(theta)
                cos_theta = math.cos(theta)

                # vertex
                vertex[0] = radius * sin_theta
                vertex[1] = - v * self._height + self._half_height
                vertex[2] = radius * cos_theta
                self._vertices.extend(vertex)

                # normal
                normal.x = sin_theta
                normal.y = slope
                normal.z = cos_theta
                normal.normalize()
                self._normals.extend([normal.x, normal.y, normal.z])

                # uv
                self._uvs.extend([u, 1 - v])

                # save index of vertex in respective row
                index_row.append(self._index)
                self._index += 1

            # now save vertices of the row in our index array
            self._index_array.append(index_row)

        # generate indices
        for x in range(0, self._radial_segments):
            for y in range(0, self._height_segments):
                # we use the index array to access the correct indices
                a = self._index_array[y][x]
                b = self._index_array[y + 1][x]
                c = self._index_array[y + 1][x + 1]
                d = self._index_array[y][x + 1]

                # faces
                self._indices.extend([a, b, d, b, c, d])

    def generate_cap(self, top):
        uv = [0.00, 0.00]
        vertex = [0.00, 0.00, 0.00]

        radius = self._radius_top if top else self._radius_bottom
        sign = 1 if top else -1

        # save the index of the first center vertex
        center_index_start = self._index

        # first we generate the center vertex data of the cap.
        # because the geometry needs one set of uvs per face,
        # we must generate a center vertex per face/segment

        for x in range(1, self._radial_segments + 1):
            # vertex
            self._vertices.extend([0, self._half_height * sign, 0])

            # normal
            self._normals.extend([0, sign, 0])

            # uv
            self._uvs.extend([0.5, 0.5])

            # increase index
            self._index += 1

        # save the index of the last center vertex
        center_index_end = self._index

        # now we generate the surrounding vertices, normals and uvs
        for x in range(0, self._radial_segments + 1):
            u = x / self._radial_segments
            theta = u * self._theta_length + self._theta_start
            cos_theta = math.cos(theta)
            sin_theta = math.sin(theta)

            # vertex
            vertex[0] = radius * sin_theta
            vertex[1] = self._half_height * sign
            vertex[2] = radius * cos_theta
            self._vertices.extend(vertex)

            # normal
            self._normals.extend([0, sign, 0])

            # uv
            uv[0] = (cos_theta * 0.5) + 0.5
            uv[1] = (sin_theta * 0.5 * sign) + 0.5
            self._uvs.extend(uv)

            # increase index
            self._index += 1

        # generate indices
        for x in range(0, self._radial_segments):
            c = center_index_start + x
            i = center_index_end + x

            if top:
                # face top
                self._indices.extend([i, i + 1, c])
            else:
                # face bottom
                self._indices.extend([i + 1, i, c])

    def update_geometry(self):
        super().update_geometry()

        self._indices = []
        self._vertices = []
        self._normals = []
        self._uvs = []

        self._half_height = self._height / 2
        self._index = 0
        self._index_offset = 0
        self._index_array = []

        self.generate_torso()
        if not self._open_ended:
            if self._radius_top > 0:
                self.generate_cap(True)
            if self._radius_bottom > 0:
                self.generate_cap(False)
