import math
from mathutils import Vector
from .. import Geometry


class Sphere(Geometry):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/SphereGeometry.js
    """

    def __init__(self, radius=0.5, width_segments=32, height_segments=32, phi_start=0, phi_length=math.pi * 2, theta_start=0, theta_length=math.pi):
        super().__init__()

        self._radius = radius
        self._width_segments = width_segments
        self._height_segments = height_segments
        self._phi_start = phi_start
        self._phi_length = phi_length
        self._theta_start = theta_start
        self._theta_length = theta_length

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, value):
        if self._radius != value:
            self._radius = value
            self._dirty_geometry = True

    @property
    def width_segments(self):
        return self._width_segments

    @width_segments.setter
    def width_segments(self, value):
        width_segments = max(3.0, math.floor(value))
        if self._width_segments != width_segments:
            self._width_segments = width_segments
            self._dirty_geometry = True

    @property
    def height_segments(self):
        return self._height_segments

    @height_segments.setter
    def height_segments(self, value):
        height_segments = max(2.0, math.floor(value))
        if self._height_segments != height_segments:
            self._height_segments = height_segments
            self._dirty_geometry = True

    @property
    def phi_start(self):
        return self._phi_start

    @phi_start.setter
    def phi_start(self, value):
        if self._phi_start != value:
            self._phi_start = value
            self._dirty_geometry = True

    @property
    def phi_length(self):
        return self._phi_length

    @phi_length.setter
    def phi_length(self, value):
        if self._phi_length != value:
            self._phi_length = value
            self._dirty_geometry = True

    @property
    def theta_start(self):
        return self._theta_start

    @theta_start.setter
    def theta_start(self, value):
        if self._theta_start != value:
            self._theta_start = value
            self._dirty_geometry = True

    @property
    def theta_length(self):
        return self._theta_length

    @theta_length.setter
    def theta_length(self, value):
        if self._theta_length != value:
            self._theta_length = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        self._indices = []
        self._vertices = []
        self._normals = []
        self._uvs = []

        theta_end = self._theta_start + self._theta_length

        index = 0
        grid = []

        vertex = [0.00, 0.00, 0.00]
        normal = Vector((0.00, 0.00, 0.00))

        # generate vertices, normals and uvs

        for iy in range(0, self._height_segments + 1):
            vertices_row = []
            v = iy / self._height_segments
            for ix in range(0, self._width_segments + 1):
                u = ix / self._width_segments
                # vertex
                vertex[0] = - self._radius * math.cos(self._phi_start + u * self._phi_length) * math.sin(self._theta_start + v * self._theta_length)
                vertex[1] = self._radius * math.cos(self._theta_start + v * self._theta_length)
                vertex[2] = self._radius * math.sin(self._phi_start + u * self._phi_length) * math.sin(self._theta_start + v * self._theta_length)

                self._vertices.extend(vertex)

                # normal
                normal.x = vertex[0]
                normal.y = vertex[1]
                normal.z = vertex[2]
                normal.normalize()
                self._normals.extend([normal.x, normal.y, normal.z])

                # uv
                self._uvs.extend([u, 1 - v])

                vertices_row.append(index)
                index += 1

            grid.append(vertices_row)

        # indices
        for iy in range(0, self._height_segments):
            for ix in range(0, self._width_segments):
                a = grid[iy][ix + 1]
                b = grid[iy][ix]
                c = grid[iy + 1][ix]
                d = grid[iy + 1][ix + 1]

                if iy != 0 or self._theta_start > 0:
                    self._indices.extend([a, b, d])
                if iy != self._height_segments - 1 or theta_end < math.pi:
                    self._indices.extend([b, c, d])
