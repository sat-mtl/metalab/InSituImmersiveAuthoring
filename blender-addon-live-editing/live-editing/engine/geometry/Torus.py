import math
from mathutils import Vector
from .. import Geometry


class Torus(Geometry):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/TorusGeometry.js
    """

    def __init__(self, radius=0.5, tube=0.25, radial_segments=8, tubular_segments=16, arc=math.pi * 2):
        super().__init__()

        self._radius = radius
        self._tube = tube
        self._radial_segments = radial_segments
        self._tubular_segments = tubular_segments
        self._arc = arc

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, value):
        if self._radius != value:
            self._radius = value
            self._dirty_geometry = True

    @property
    def tube(self):
        return self._tube

    @tube.setter
    def tube(self, value):
        if self._tube != value:
            self._tube = value
            self._dirty_geometry = True

    @property
    def radial_segments(self):
        return self._radial_segments

    @radial_segments.setter
    def radial_segments(self, value):
        radial_segments = max(8.0, math.floor(value))
        if self._radial_segments != radial_segments:
            self._radial_segments = radial_segments
            self._dirty_geometry = True

    @property
    def tubular_segments(self):
        return self._tubular_segments

    @tubular_segments.setter
    def tubular_segments(self, value):
        tubular_segments = max(6.0, math.floor(value))
        if self._tubular_segments != tubular_segments:
            self._tubular_segments = tubular_segments
            self._dirty_geometry = True

    @property
    def arc(self):
        return self._arc

    @arc.setter
    def arc(self, value):
        if self._arc != value:
            self._arc = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        self._indices = []
        self._vertices = []
        self._normals = []
        self._uvs = []

        center = Vector()
        vertex = Vector()
        normal = Vector()

        # generate vertices, normals and uvs
        for j in range(0, self._radial_segments + 1):
            for i in range(0, self._tubular_segments + 1):
                u = i / self._tubular_segments * self._arc
                v = j / self._radial_segments * math.pi * 2

                # vertex
                vertex.x = (self._radius + self._tube * math.cos(v)) * math.cos(u)
                vertex.y = (self._radius + self._tube * math.cos(v)) * math.sin(u)
                vertex.z = self._tube * math.sin(v)

                self._vertices.extend([vertex.x, vertex.y, vertex.z])

                # normal
                center.x = self._radius * math.cos(u)
                center.y = self._radius * math.sin(u)
                normal = (vertex - center).normalized()

                self._normals.extend([normal.x, normal.y, normal.z])

                # uv
                self._uvs.extend([i / self._tubular_segments, j / self._radial_segments])

        # generate indices
        for j in range(1, self._radial_segments + 1):
            for i in range(1, self._tubular_segments + 1):
                # indices
                a = (self._tubular_segments + 1) * j + i - 1
                b = (self._tubular_segments + 1) * (j - 1) + i - 1
                c = (self._tubular_segments + 1) * (j - 1) + i
                d = (self._tubular_segments + 1) * j + i

                # faces
                self._indices.extend([a, b, d, b, c, d])
