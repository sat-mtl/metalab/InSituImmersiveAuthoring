from .. import Shape


class Rectangle(Shape):

    def __init__(self, width=1, height=1, line_width=1):
        super().__init__(line_width=line_width)

        self._width = width
        self._height = height
        self._line_width = line_width

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        if self._width != value:
            self._width = value
            self._dirty_geometry = True

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        if self._height != value:
            self._height = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        hw = self._width / 2
        hh = self._height / 2

        self._vertices = [
            -hw, -hh, 0,
            -hw, hh, 0,
            hw, hh, 0,
            hw, -hh, 0,
            -hw, -hh, 0
        ]
