import math
from .. import Shape


class Ring(Shape):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/RingGeometry.js
    """

    def __init__(self, inner_radius=0.25, outer_radius=0.5, theta_segments=32, phi_segments=1, theta_start=0,
                 theta_length=math.pi * 2, line_width=1):
        super().__init__(line_width=line_width)

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius
        self._theta_segments = theta_segments
        self._phi_segments = phi_segments
        self._theta_start = theta_start
        self._theta_length = theta_length

    @property
    def inner_radius(self):
        return self._inner_radius

    @inner_radius.setter
    def inner_radius(self, value):
        if self._inner_radius != value:
            self._inner_radius = value
            self._dirty_geometry = True

    @property
    def outer_radius(self):
        return self._outer_radius

    @outer_radius.setter
    def outer_radius(self, value):
        if self._outer_radius != value:
            self._outer_radius = value
            self._dirty_geometry = True

    @property
    def theta_segments(self):
        return self._theta_segments

    @theta_segments.setter
    def theta_segments(self, value):
        theta_segments = max(3.00, math.floor(value))
        if self._theta_segments != theta_segments:
            self._theta_segments = theta_segments
            self._dirty_geometry = True

    @property
    def phi_segments(self):
        return self._phi_segments

    @phi_segments.setter
    def phi_segments(self, value):
        phi_segments = max(1.00, math.floor(value))
        if self._phi_segments != phi_segments:
            self._phi_segments = phi_segments
            self._dirty_geometry = True

    @property
    def theta_start(self):
        return self._theta_start

    @theta_start.setter
    def theta_start(self, value):
        if self._theta_start != value:
            self._theta_start = value
            self._dirty_geometry = True

    @property
    def theta_length(self):
        return self._theta_length

    @theta_length.setter
    def theta_length(self, value):
        if self._theta_length != value:
            self._theta_length = value
            self._dirty_geometry = True

    def update_geometry(self):
        super().update_geometry()

        self._vertices = []

        # some helper variables
        radius = self._inner_radius
        radius_step = ((self._outer_radius - self._inner_radius) / self._phi_segments)
        vertex = [0.00, 0.00, 0.00]

        # generate vertices
        for j in range(0, self._phi_segments + 1):
            if j == 0 or j == self._phi_segments:
                if j == 0:
                    r = range(0, self._theta_segments + 1, 1)
                else:
                    r = range(self._theta_segments, -1, -1)

                for i in r:
                    # values are generate from the inside of the ring to the outside
                    segment = self._theta_start + i / self._theta_segments * self._theta_length

                    # vertex
                    vertex[0] = radius * math.cos(segment)
                    vertex[1] = radius * math.sin(segment)
                    self._vertices.extend(vertex)
            else:
                segment_start = self._theta_start
                vertex[0] = radius * math.cos(segment_start)
                vertex[1] = radius * math.sin(segment_start)
                self._vertices.extend(vertex)

                segment_end = self._theta_start + self._theta_length
                vertex[0] = radius * math.cos(segment_end)
                vertex[1] = radius * math.sin(segment_end)
                self._vertices.extend(vertex)

            # increase the radius for next row of vertices
            radius += radius_step

        # Close the shape
        self._vertices.extend([self._vertices[0], self._vertices[1], self._vertices[2]])
