from .. import Object3D
from ..geometry import Cylinder, Box
from ..materials import MeshBasicMaterial
from ...utils import Colors


class ArrowSquareHelper(Object3D):

    def __init__(self, color=(1.00, 1.00, 0.00), length=1, radius=0.025, head_radius=0.05, head_length=0.125):
        super().__init__()

        self._body_geometry = Cylinder()
        self._body = Object3D(geometry=self._body_geometry, material=MeshBasicMaterial())
        self.add_child(self._body)

        self._head_geometry = Box()
        self._head = Object3D(geometry=self._head_geometry, material=MeshBasicMaterial())
        self.add_child(self._head)

        self._color = color
        self._length = length
        self._radius = radius
        self._head_radius = head_radius
        self._head_length = head_length

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, value):
        if self._length != value:
            self._length = value
            self._dirty_attributes = True

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, value):
        if self._radius != value:
            self._radius = value
            self._dirty_attributes = True

    @property
    def head_radius(self):
        return self._head_radius

    @head_radius.setter
    def head_radius(self, value):
        if self._head_radius != value:
            self._head_radius = value
            self._dirty_attributes = True

    @property
    def head_length(self):
        return self._head_length

    @head_length.setter
    def head_length(self, value):
        if self._head_length != value:
            self._head_length = value
            self._dirty_attributes = True

    def update_attributes(self):
        super().update_attributes()

        self._body.material.color = self._color
        self._body_geometry.radius_top = self._radius
        self._body_geometry.radius_bottom = self._radius
        self._body_geometry.height = self._length - self._head_length
        self._body.y = self._body_geometry.height / 2

        self._head.material.color = self._color

        size = self._head_radius * 2
        self._head_geometry.width = size
        self._head_geometry.depth = size
        self._head_geometry.height = self._head_length

        self._head.y = self._body_geometry.height + (self._head_geometry.height / 2)
