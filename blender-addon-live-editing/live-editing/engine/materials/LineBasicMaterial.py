from OpenGL.GL import *
from . import Material
from .. import Program
from ...utils.GLUtils import GL4Float


class LineBasicMaterial(Material):
    def __init__(self, color=(1.0, 1.0, 1.0), opacity=1.0):
        super().__init__(program=Program.get_program('lines'))
        self._color = color
        self._opacity = opacity
        self._color_gl = GL4Float(*self._color, self._opacity)
        self._color_changed = False

    # region Properties

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        if self._color != value:
            self._color = value
            self._color_changed = True

    @property
    def opacity(self):
        return self._opacity

    @opacity.setter
    def opacity(self, value):
        if self._opacity != value:
            self._opacity = value
            self._color_changed = True

    # endregion

    def update(self, time, delta):
        super().update(time, delta)
        if self._color_changed:
            self._color_changed = False
            self._color_gl = GL4Float(*self._color, self._opacity)

    def bind(self, object_3d):
        super().bind(object_3d)

        # Color
        glUniform4fv(self._program.line_color_uniform, 1, self._color_gl)
