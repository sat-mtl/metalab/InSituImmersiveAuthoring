from ...utils import Logger
from ...Constants import DEBUG_ENGINE


class Material:

    def __init__(self, program):
        if DEBUG_ENGINE:
            self._logger = Logger(self)

        self._program = program

    def dispose(self):
        if DEBUG_ENGINE:
            self._logger.log("dispose")

    @property
    def program(self):
        return self._program

    def update(self, time, delta):
        pass

    def bind(self, object_3d):
        if DEBUG_ENGINE:
            self._logger.open("bind")

        object_3d.scene.engine.select_program(self._program)

    def unbind(self, object_3d):
        if DEBUG_ENGINE:
            self._logger.close("unbind")
