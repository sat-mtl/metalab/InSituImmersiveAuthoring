from OpenGL.GL import *
from . import Material
from .. import Program
from ...utils.GLUtils import GL8Float


class MeshBasicMaterial(Material):

    def __init__(self, color=(1.0, 1.0, 1.0), opacity=1.0, diffuse_map=None, flat=False, flip_y=True):
        super().__init__(program=Program.get_program('basic'))

        self._color = color
        self._opacity = opacity
        self._data_changed = True  # We have to init with constructor params on first loop
        self._gl_data = GL8Float()

        self._diffuse_map = diffuse_map
        self._flat = flat
        self._flip_y = flip_y
        if self._diffuse_map is not None:
            self._diffuse_map.slot = 0

    def dispose(self):
        if self._diffuse_map is not None:
            self._diffuse_map.dispose()
            self._diffuse_map = None
        super().dispose()

    # region Properties

    @property
    def color(self):
        return self._color
        
    @color.setter
    def color(self, value):
        if self._color != value:
            self._color = value
            self._data_changed = True

    @property
    def opacity(self):
        return self._opacity

    @opacity.setter
    def opacity(self, value):
        if self._opacity != value:
            self._opacity = value
            self._data_changed = True

    @property
    def flat(self):
        return self._flat

    @flat.setter
    def flat(self, value):
        if self._flat != value:
            self._flat = value
            self._data_changed = True
            
    @property
    def flip_y(self):
        return self._flip_y

    @flip_y.setter
    def flip_y(self, value):
        if self._flip_y != value:
            self._flip_y = value
            self._data_changed = True

    @property
    def diffuse_map(self):
        return self._diffuse_map

    # endregion

    def update(self, time, delta):
        super().update(time, delta)

        if self._data_changed:
            self._data_changed = False

            """
            Combine all values in one vector to save some calls to PyOpenGL
            """
            self._gl_data = GL8Float(
                *self._color, self._opacity,
                1 if self._diffuse_map is not None else 0,
                1 if self._flat else 0,
                1 if self._flip_y else 0
            )

        if self._diffuse_map is not None:
            self._diffuse_map.update(time, delta)

    def bind(self, object_3d):
        super().bind(object_3d)

        # Diffuse Map
        if self._diffuse_map is not None:
            self._diffuse_map.bind(self._program.diffuse_uniform)

        glUniform4fv(self._program.data_uniform, 2, self._gl_data)

    def unbind(self, object_3d):
        super().unbind(object_3d)
        if self._diffuse_map is not None:
            self._diffuse_map.unbind()

