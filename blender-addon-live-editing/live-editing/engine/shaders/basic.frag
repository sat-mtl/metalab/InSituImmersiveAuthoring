#version 330 compatibility

#define COLOR 0
#define DATA 1
#define DATA_DIFFUSE 0
#define DATA_FLAT 1
#define DATA_FLIP_Y 2

uniform vec4 data[2];
uniform sampler2D diffuse;

in vec3 normal;
in vec2 uv;

out vec4 fragColor;

void main()
{
    vec4 color = data[COLOR];
    bool flat_color = data[DATA][DATA_FLAT] == 1;
    bool flip_y = data[DATA][DATA_FLIP_Y] == 1;
    bool use_diffuse = data[DATA][DATA_DIFFUSE] == 1;

    float value = flat_color ? 1.0 : 0.5 + abs(dot(normal, vec3(0.0, 0.0, 1.0))) / 2.0;

    if (use_diffuse) {
        vec4 diffuse_color = texture(diffuse, vec2(uv.x, flip_y ? 1.0 - uv.y : uv.y)) * color;
        fragColor = vec4(diffuse_color.rgb * value, diffuse_color.a);
    } else {
        fragColor = vec4(color.rgb * value, color.a);
    }
}
