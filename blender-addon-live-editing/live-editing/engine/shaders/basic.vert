#version 330 compatibility

uniform mat4 proj_matrix;
uniform mat4 view_matrix;
uniform mat4 obj_matrix;

in vec3 vertex_position;
in vec3 vertex_normal;
in vec2 vertex_uv;

out vec3 normal;
out vec2 uv;

void main()
{
    mat4 mvp = proj_matrix * obj_matrix;

    normal = normalize((transpose(inverse(view_matrix * obj_matrix)) * vec4(vertex_normal, 0.0)).xyz);
    uv = vertex_uv;

    gl_Position = mvp * vec4(vertex_position, 1.0);
}
