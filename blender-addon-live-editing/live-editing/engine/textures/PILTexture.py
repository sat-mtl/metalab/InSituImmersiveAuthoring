from . import Texture


class PILTexture(Texture):

    def __init__(self, image=None):
        super().__init__()
        self.set_image(image)

    def set_image(self, image):
        self._data = image.tobytes() if image is not None else None
        self._width, self._height = image.size if image is not None else (0.0, 0.0)
        self._texture_needs_update = True
