from OpenGL.GL import *
from ...utils import Logger
from ...Constants import DEBUG_ENGINE, DEBUG_GL_TEX


class Texture:

    def __init__(self, data=None, width=0, height=0):
        if DEBUG_ENGINE:
            self._logger = Logger(self)

        self._data = data

        self._last_width = 0
        self._last_height = 0

        self._width = width
        self._height = height

        self._slot = 0
        self._texture_id = -1

        self._texture_needs_update = True

    def dispose(self):
        if DEBUG_ENGINE:
            self._logger.open("dispose")

        self._data = None
        if self._texture_id != -1:
            if DEBUG_ENGINE:
                self._logger.log("removing texture id", self._texture_id)
            if DEBUG_GL_TEX:
                print('\033[1;31mTEX -', self._texture_id, '\033[0m')
            glDeleteTextures(1, [self._texture_id])
            self._texture_id = -1

        if DEBUG_ENGINE:
            self._logger.close()

    # region Properties

    @property
    def slot(self):
        return self._slot

    @slot.setter
    def slot(self, value):
        if self._slot != value:
            self._slot = value
        self._texture_needs_update = True

    # endregion

    def update(self, time, delta):
        if not self._texture_needs_update:
            return

        if DEBUG_ENGINE:
            self._logger.open("update")

        if self._data is not None:
            if DEBUG_ENGINE:
                self._logger.log("updating")

            init = False
            if self._texture_id == -1:
                self._texture_id = glGenTextures(1)
                if DEBUG_GL_TEX:
                    print('\033[1;32mTEX +', self._texture_id, '\033[0m')
                init = True

            if self._last_width != self._width or self._last_height != self._height:
                init = True

            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, self._texture_id)
            texture = (GLubyte * len(self._data))(*self._data)

            if init:
                if DEBUG_ENGINE:
                    self._logger.log("first time, generating")

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
                glTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_RGBA,
                    self._width,
                    self._height,
                    0,
                    GL_RGBA,
                    GL_UNSIGNED_INT_8_8_8_8_REV,
                    texture
                )
            else:
                if DEBUG_ENGINE:
                    self._logger.log("already initialized, updating")

                glTexSubImage2D(
                    GL_TEXTURE_2D,
                    0,
                    0,
                    0,
                    self._width,
                    self._height,
                    GL_RGBA,
                    GL_UNSIGNED_INT_8_8_8_8_REV,
                    texture
                )

            glGenerateMipmap(GL_TEXTURE_2D)
            glBindTexture(GL_TEXTURE_2D, 0)

            self._last_width = self._width
            self._last_height = self._height

        else:
            if DEBUG_ENGINE:
                self._logger.log("clearing")

            if self._texture_id != -1:
                glDeleteTextures(1, [self._texture_id])
                if DEBUG_GL_TEX:
                    print('\033[1;35mTEX *', self._texture_id, '\033[0m')
                self._texture_id = -1

            self._last_width = 0
            self._last_height = 0

        self._texture_needs_update = False

        if DEBUG_ENGINE:
            self._logger.close()

    def bind(self, uniform):
        if DEBUG_ENGINE:
            self._logger.open("bind", "texture_id", self._texture_id)

        if self._texture_id:
            if DEBUG_GL_TEX:
                print('\033[1;30mGEO ', self._texture_id, '\033[0m')
            glActiveTexture(GL_TEXTURE0 + self._slot)
            glBindTexture(GL_TEXTURE_2D, self._texture_id)
            glUniform1i(uniform, self._slot)

    def unbind(self):
        if self._texture_id:
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, 0)

        if DEBUG_ENGINE:
            self._logger.close("unbind", "texture id", self._texture_id)
