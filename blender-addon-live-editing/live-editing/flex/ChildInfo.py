class ChildInfo:
    def __init__(self):
        self.child = None
        self.size = 0.00
        self.preferred = 0.00
        self.flex = 0.00
        self.percent = 0.00
        self.min = 0.00
        self.max = 0.00
        self.width = 0.00
        self.height = 0.00
        self.depth = 0.00
