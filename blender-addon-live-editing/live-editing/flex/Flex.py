import math


class Flex:

    @staticmethod
    def flex_children_proportionally(space_for_children, space_to_distribute, total_percent, child_info_array):
        #  The algorithm iteratively attempts to break down the space that
        #  is consumed by "flexible" containers into ratios that are related
        #  to the percent_width/percent_height of the participating containers.

        num_children = len(child_info_array)
        flex_consumed = 0.00
        done = False
        unused = 0.00

        #  Continue as long as there are some remaining flexible children.
        #  The "done" flag isn't strictly necessary, except that it catches
        #  cases where round-off error causes totalPercent to not exactly
        #  equal zero.
        while not done:
            flex_consumed = 0  # space consumed by flexible components
            done = True  # we are optimistic

            #  We now do something a little tricky so that we can 
            #  support partial filling of the space. If our total
            #  percent < 100% then we can trim off some space.
            #  This unused space can be used to fulfill mins and maxes.
            unused = space_to_distribute - (space_for_children * total_percent)
            if unused > 0:
                space_to_distribute -= unused
            else:
                unused = 0

            # Space for flexible children is the total amount of space
            #  available minus the amount of space consumed by non-flexible
            #  components.Divide that space in proportion to the percent
            #  of the child
            space_per_percent = space_to_distribute / total_percent

            #  Attempt to divide out the space using our percent amounts,
            #  if we hit its limit then that control becomes 'non-flexible'
            #  and we run the whole space to distribute calculation again.
            for i in range(0, num_children):
                child_info = child_info_array[i]

                #  Set its size in proportion to its percent.
                size = child_info.percent * space_per_percent

                #  If our flexibility calc say grow/shrink more than we are
                #  allowed, then we grow/shrink whatever we can, remove
                #  ourselves from the array for the next pass, and start
                #  the loop over again so that the space that we weren't
                #  able to consume / release can be re-used by others.
                if size < child_info.min:
                    min_size = child_info.min
                    child_info.size = min_size

                    #  Move this object to the end of the array
                    #  and decrement the length of the array.
                    #  This is slightly expensive, but we don't expect
                    #  to hit these min/max limits very often.
                    child_info_array[i] = child_info_array[--num_children]
                    child_info_array[num_children] = child_info

                    total_percent -= child_info.percent
                    #  Use unused space first before reducing flexible space.
                    if unused >= min_size:
                        unused -= min_size
                    else:
                        space_to_distribute -= min_size - unused
                        unused = 0
                    done = False
                    break

                elif size > child_info.max:
                    max_size = child_info.max
                    child_info.size = max_size

                    child_info_array[i] = child_info_array[--num_children]
                    child_info_array[num_children] = child_info

                    total_percent -= child_info.percent
                    #  Use unused space first before reducing flexible space.
                    if unused >= max_size:
                        unused -= max_size
                    else:
                        space_to_distribute -= max_size - unused
                        unused = 0
                    done = False
                    break

                else:
                    #  All is well, let's carry on...
                    child_info.size = size
                    flex_consumed += size

        # return max(0, math.floor((space_to_distribute + unused) - flex_consumed))
        return max(0, (space_to_distribute + unused) - flex_consumed)
