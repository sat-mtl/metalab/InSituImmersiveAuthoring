import sys
from . import PriorityQueue
from ..utils import Logger
from ..Constants import DEBUG_LAYOUT, DEBUG_LAYOUT_VISUAL


class LayoutManager:
    def __init__(self):
        if DEBUG_LAYOUT:
            self._logger = Logger(self)
             
        """
        Constructor
        """

        """
        A queue of objects that need to dispatch update_complete events
        when invalidation processing is complete
        """
        self._update_complete_queue = PriorityQueue()

        """
        A queue of objects to be processed during the first phase
        of invalidation processing, when an UIComponent  has
        its validate_properties() method called (which in a UIComponent
        calls commit_properties()).
        Objects are added to this queue by invalidate_properties()
        and removed by validate_properties().
        """
        self._invalidate_properties_queue = PriorityQueue()

        """
        A flag indicating whether there are objects
        in the invalidate_properties_queue.
        It is set true by invalidate_properties()
        and set false by validate_properties().
        """
        self._invalidate_properties_flag = False
        #  flag when in validate_client to check the properties queue again
        self._invalidate_client_properties_flag = False

        """
        A queue of objects to be processed during the second phase
        of invalidation processing, when an UIComponent  has
        its validate_size() method called (which in a UIComponent
        calls measure()).
        Objects are added to this queue by invalidate_size().
        and removed by validate_size().
        """
        self._invalidate_size_queue = PriorityQueue()

        """
        A flag indicating whether there are objects
        in the invalidate_size_queue.
        It is set true by invalidate_size()
        and set false by validate_size().
        """
        self._invalidate_size_flag = False

        #  flag when in validate_client to check the size queue again
        self._invalidate_client_size_flag = False

        """
        A queue of objects to be processed during the third phase
        of invalidation processing, when an UIComponent  has
        its validate_display_list() method called (which in a
        UIComponent calls update_display_list()).
        Objects are added to this queue by invalidate_display_list()
        and removed by validate_display_list().
        """
        self._invalidate_display_list_queue = PriorityQueue()

        """
        A flag indicating whether there are objects
        in the invalidate_display_list_queue.
        It is set true by invalidate_display_list()
        and set false by validate_display_list().
        """
        self._invalidate_display_list_flag = False

        self._listeners_attached = False

        """
        used in validate_client to quickly estimate whether we have to
        search the queues again
        """
        self._target_level = sys.maxsize

        """
        the current object being validated
        it could be wrong if the validating object calls validate_now on something.
        """
        self._current_object = None

        """
        A flag that indicates whether the LayoutManager allows screen updates
        between phases.
        If <code>true</code>, measurement and layout are done in phases, one phase
        per screen update.
        All components have their <code>validateProperties()</code>
        and <code>commitProperties()</code> methods
        called until all their properties are validated.
        The screen will then be updated.
        
        <p>Then all components will have their <code>validateSize()</code>
        and <code>measure()</code>
        methods called until all components have been measured, then the screen
        will be updated again.  </p>
        
        <p>Finally, all components will have their
        <code>validateDisplayList()</code> and
        <code>updateDisplayList()</code> methods called until all components
        have been validated, and the screen will be updated again.
        If in the validation of one phase, an earlier phase gets invalidated,
        the LayoutManager starts over.
        This is more efficient when large numbers of components
        are being created an initialized.  The framework is responsible for setting
        this property.</p>
        
        <p>If <code>false</code>, all three phases are completed before the screen is updated.</p>
        """
        self._use_phased_instantiation = False

    # --------------------------------------------------------------------------
    # 
    #   Methods: Invalidation
    # 
    # --------------------------------------------------------------------------

    def invalidate_properties(self, obj):
        """
        Adds an object to the list of components that want their
        <code>validate_properties()</code> method called.
        A component should call this method when a property changes.
        Typically, a property setter method
        stores a the new value in a temporary variable and calls
        the <code>invalidate_properties()</code> method
        so that its <code>validate_properties()</code>
        and <code>commit_properties()</code> methods are called
        later, when the new value will actually be applied to the component and/or
        its children.  The advantage of this strategy is that often, more than one
        property is changed at a time and the properties may interact with each
        other, or repeat some code as they are applied, or need to be applied in
        a specific order.  This strategy allows the most efficient method of
        applying new property values.

        @param obj The object whose property changed.

        """

        if DEBUG_LAYOUT:
            self._logger.open("invalidate_properties", obj)

        if not self._invalidate_properties_flag:
            if DEBUG_LAYOUT:
                self._logger.log("Invalidated!")

            self._invalidate_properties_flag = True
            if not self._listeners_attached:
                self.attach_listeners()

        if self._target_level <= obj.nest_level:
            self._invalidate_client_properties_flag = True

        self._invalidate_properties_queue.add_object(obj, obj.nest_level)

        if DEBUG_LAYOUT:
            self._logger.close()

    def invalidate_size(self, obj):
        """
        Adds an object to the list of components that want their
        <code>validate_size()</code> method called.
        Called when an object's size changes.

        <p>An object's size can change for two reasons:</p>

        <ol>
            <li>The content of the object changes. For example, the size of a
            button changes when its <code>label</code> is changed.</li>
            <li>A script explicitly changes one of the following properties:
            <code>min_width</code>, <code>min_height</code>,
            <code>explicit_width</code>, <code>explicit_height</code>,
            <code>max_width</code>, or <code>max_height</code>.</li>
        </ol>

        <p>When the first condition occurs, it's necessary to recalculate
        the measurements for the object.
        When the second occurs, it's not necessary to recalculate the
        measurements because the new size of the object is known.
        However, it's necessary to remeasure and relayout the object's
        parent.</p>

        @param obj The object whose size changed.
        """

        if DEBUG_LAYOUT:
            self._logger.open("invalidate_size", obj)

        if not self._invalidate_size_flag:
            if DEBUG_LAYOUT:
                self._logger.log("Invalidated!")

            self._invalidate_size_flag = True
            if not self._listeners_attached:
                self.attach_listeners()

        if self._target_level <= obj.nest_level:
            self._invalidate_client_size_flag = True

        self._invalidate_size_queue.add_object(obj, obj.nest_level)

        if DEBUG_LAYOUT:
            self._logger.close()

    def invalidate_display_list(self, obj):
        """
        Called when a component changes in some way that its layout and/or visuals
        need to be changed.
        In that case, it is necessary to run the component's layout algorithm,
        even if the component's size hasn't changed.  For example, when a new child component
        is added, or a style property changes or the component has been given
        a new size by its parent.

        @param obj The object that changed.
        """

        if DEBUG_LAYOUT:
            self._logger.open("invalidate_display_list", obj)

        if not self._invalidate_display_list_flag:
            if DEBUG_LAYOUT:
                self._logger.log("Invalidated!")

            self._invalidate_display_list_flag = True
            if not self._listeners_attached:
                self.attach_listeners()

        self._invalidate_display_list_queue.add_object(obj, obj.nest_level)

        if DEBUG_LAYOUT:
            self._logger.close()

    # --------------------------------------------------------------------------
    # 
    #   Methods: Commitment, measurement, layout, and drawing
    # 
    # --------------------------------------------------------------------------

    def validate_properties(self):
        """
         Validates all components whose properties have changed and have called
         the <code>invalidate_properties()</code> method.
         It calls the <code>validate_properties()</code> method on those components
         and will call <code>validate_properties()</code> on any other components that are
         invalidated while validating other components.
         """

        if DEBUG_LAYOUT:
            self._logger.open("validate_properties")

        #  Keep traversing the invalidate_properties_queue until we've reached the end.
        #  More elements may get added to the queue while we're in this loop, or a
        #  a recursive call to this function may remove elements from the queue while
        #  we're in this loop.
        obj = self._invalidate_properties_queue.remove_smallest()
        while obj is not None:
            if DEBUG_LAYOUT:
                self._logger.log(obj)

            if obj.nest_level > 0:
                self._current_object = obj
                obj.validate_properties()
                if not obj.update_complete_pending_flag:
                    self._update_complete_queue.add_object(obj, obj.nest_level)
                    obj.update_complete_pending_flag = True

            # Once we start, don't stop.
            obj = self._invalidate_properties_queue.remove_smallest()

        if self._invalidate_properties_queue.is_empty():
            self._invalidate_properties_flag = False

        if DEBUG_LAYOUT:
            self._logger.close()

    def validate_size(self):
        """
        Validates all components whose properties have changed and have called
        the <code>invalidate_size()</code> method.
        It calls the <code>validate_size()</code> method on those components
        and will call the <code>validate_size()</code> method
        on any other components that are
        invalidated while validating other components.
        The </code>validate_size()</code> method  starts with
        the most deeply nested child in the tree of display objects
        """

        if DEBUG_LAYOUT:
            self._logger.open("validate_size")

        obj = self._invalidate_size_queue.remove_largest()
        while obj is not None:
            if DEBUG_LAYOUT:
                self._logger.log('Validating >', obj)

            if obj.nest_level > 0:
                self._current_object = obj
                obj.validate_size()
                if not obj.update_complete_pending_flag:
                    self._update_complete_queue.add_object(obj, obj.nest_level)
                    obj.update_complete_pending_flag = True

            obj = self._invalidate_size_queue.remove_largest()

        if self._invalidate_size_queue.is_empty():
            self._invalidate_size_flag = False

        if DEBUG_LAYOUT:
            self._logger.close()

    def validate_display_list(self):
        """
        Validates all components whose properties have changed and have called
        the <code>invalidate_display_list()</code> method.
        It calls <code>validate_display_list()</code> method on those components
        and will call the <code>validate_display_list()</code> method
        on any other components that are
        invalidated while validating other components.
        The <code>validate_display_list()</code> method starts with
        the least deeply nested child in the tree of display objects
        """

        if DEBUG_LAYOUT:
            self._logger.open("validate_display_list")

        obj = self._invalidate_display_list_queue.remove_smallest()
        while obj is not None:
            if DEBUG_LAYOUT:
                self._logger.log(obj)

            if obj.nest_level > 0:
                self._current_object = obj
                obj.validate_display_list()
                if not obj.update_complete_pending_flag:
                    self._update_complete_queue.add_object(obj, obj.nest_level)
                    obj.update_complete_pending_flag = True

            # Once we start, don't stop.
            obj = self._invalidate_display_list_queue.remove_smallest()

        if self._invalidate_display_list_queue.is_empty():
            self._invalidate_display_list_flag = False

        if DEBUG_LAYOUT:
            self._logger.close()

    def do_phased_instantiation(self):
        if DEBUG_LAYOUT:
            self._logger.open("do_phased_instantiation")

        if self._use_phased_instantiation:
            if DEBUG_LAYOUT:
                self._logger.log("phased")

            if self._invalidate_properties_flag:
                self.validate_properties()
            elif self._invalidate_size_flag:
                self.validate_size()
            elif self._invalidate_display_list_flag:
                self.validate_display_list()
        else:
            if DEBUG_LAYOUT:
                self._logger.log("not phased")

            if self._invalidate_properties_flag:
                self.validate_properties()
            if self._invalidate_size_flag:
                self.validate_size()
            if self._invalidate_display_list_flag:
                self.validate_display_list()

        if self._invalidate_properties_flag or self._invalidate_size_flag or self._invalidate_display_list_flag:
            self.attach_listeners()
        else:
            if DEBUG_LAYOUT:
                self._logger.log("Complete!")

            self._use_phased_instantiation = False
            self._listeners_attached = False

            obj = self._update_complete_queue.remove_largest()
            while obj is not None:
                if DEBUG_LAYOUT:
                    self._logger.log(obj)

                if not obj.initialized:
                    obj.initialized = True
                obj.update_complete()
                obj.update_complete_pending_flag = False
                obj = self._update_complete_queue.remove_largest()

        if DEBUG_LAYOUT:
            self._logger.close()

    def validate_now(self):
        """
        When properties are changed, components generally do not apply those changes immediately.
        Instead the components usually call one of the layout_manager's invalidate methods and
        apply the properties at a later time.  The actual property you set can be read back
        immediately, but if the property affects other properties in the component or its
        children or parents, those other properties may not be immediately updated.  To
        guarantee that the values are updated, you can call the <code>validate_now()</code> method.
        It updates all properties in all components before returning.
        Call this method only when necessary as it is a computationally intensive call.
        """
        if DEBUG_LAYOUT:
            self._logger.open("validate_now")

        if not self._use_phased_instantiation:
            infinite_loop_guard = 0
            while self._listeners_attached and infinite_loop_guard < 100:
                infinite_loop_guard += 1
                self.do_phased_instantiation()

        if DEBUG_LAYOUT:
            self._logger.close()

    def validate_client(self, target, skip_display_list=False):
        """
        When properties are changed, components generally do not apply those changes immediately.
        Instead the components usually call one of the layout_manager's invalidate methods and
        apply the properties at a later time.  The actual property you set can be read back
        immediately, but if the property affects other properties in the component or its
        children or parents, those other properties may not be immediately updated.

        <p>To guarantee that the values are updated,
        you can call the <code>validate_client()</code> method.
        It updates all properties in all components whose nest level is greater than or equal
        to the targets component before returning.
        Call this method only when necessary as it is a computationally intensive call.</p>

        @param targets The component passed in is used to test which components
        should be validated.  All components contained by this component will have their
        <code>validate_properties()</code>, <code>commit_properties()</code>,
        <code>validate_size()</code>, <code>measure()</code>,
        <code>validate_display_list()</code>,
        and <code>update_display_list()</code> methods called.

        @param skip_display_list If <code>true</code>,
        does not call the <code>validate_display_list()</code>
        and <code>update_display_list()</code> methods.
        """

        if DEBUG_LAYOUT:
            self._logger.open("validate_client", self._current_object)

        last_current_object = self._current_object

        done = False
        old_target_level = self._target_level

        #  the theory here is that most things that get validated are deep in the tree
        #  and so there won't be nested calls to validate_client.  However if there is,
        #  we don't want to have a more sophisticated scheme of keeping track
        #  of dirty flags at each level that is being validated, but we definitely
        #  do not want to keep scanning the queues unless we're pretty sure that
        #  something might be dirty so we just say that if something got dirty
        #  during this call at a deeper nesting than the first call to validate_client
        #  then we'll scan the queues.  So we only change targetLevel if we're the
        #  outer call to validate_client and only that call restores it.
        if self._target_level == sys.maxsize:
            self._target_level = target.nest_level

        while not done:
            #  assume we won't find anything
            done = True

            #  Keep traversing the invalidate_properties_queue until we've reached the end.
            #  More elements may get added to the queue while we're in this loop, or a
            #  a recursive call to this function may remove elements from the queue while
            #  we're in this loop.

            # PROPERTIES
            obj = self._invalidate_properties_queue.remove_smallest_child(target)
            while obj is not None:
                if DEBUG_LAYOUT:
                    self._logger.open("validate_properties", obj)

                if obj.nest_level > 0:
                    self._current_object = obj
                    obj.validate_properties()
                    if not obj.update_complete_pending_flag:
                        self._update_complete_queue.add_object(obj, obj.nest_level)
                        obj.update_complete_pending_flag = True

                # Once we start, don't stop.
                obj = self._invalidate_properties_queue.remove_smallest_child(target)

                if DEBUG_LAYOUT:
                    self._logger.close()

            if self._invalidate_properties_queue.is_empty():
                self._invalidate_properties_flag = False
                self._invalidate_client_properties_flag = False

            # SIZE
            obj = self._invalidate_size_queue.remove_largest_child(target)
            while obj is not None:
                if DEBUG_LAYOUT:
                    self._logger.open("validate_size", obj)

                if obj.nest_level > 0:
                    self._current_object = obj
                    obj.validate_size()
                    if not obj.update_complete_pending_flag:
                        self._update_complete_queue.add_object(obj, obj.nest_level)
                        obj.update_complete_pending_flag = True

                if self._invalidate_client_properties_flag:
                    #  did any properties get invalidated while validating size?
                    obj = self._invalidate_properties_queue.remove_smallest_child(target)
                    if obj is not None:
                        #  re-queue it. we'll pull it at the beginning of the loop
                        self._invalidate_properties_queue.add_object(obj, obj.nest_level)
                        done = False
                        break

                obj = self._invalidate_size_queue.remove_largest_child(target)

                if DEBUG_LAYOUT:
                    self._logger.close()

            if self._invalidate_size_queue.is_empty():
                self._invalidate_size_flag = False
                self._invalidate_client_size_flag = False

            # DISPLAY LIST
            if not skip_display_list:
                obj = self._invalidate_display_list_queue.remove_smallest_child(target)
                while obj is not None:
                    if DEBUG_LAYOUT:
                        self._logger.open("validate_display_list", obj)

                    if obj.nest_level:
                        self._current_object = obj
                        obj.validate_display_list()
                        if not obj.update_complete_pending_flag:
                            self._update_complete_queue.add_object(obj, obj.nest_level)
                            obj.update_complete_pending_flag = True

                    if self._invalidate_client_properties_flag:
                        #  did any properties get invalidated while validating size?
                        obj = self._invalidate_properties_queue.remove_smallest_child(target)
                        if obj is not None:
                            #  re-queue it. we'll pull it at the beginning of the loop
                            self._invalidate_properties_queue.add_object(obj, obj.nest_level)
                            done = False
                            break

                    if self._invalidate_client_size_flag:
                        obj = self._invalidate_size_queue.remove_largest_child(target)
                        if obj is not None:
                            #  re-queue it. we'll pull it at the beginning of the loop
                            self._invalidate_size_queue.add_object(obj, obj.nest_level)
                            done = False
                            break

                    # Once we start, don't stop.
                    obj = self._invalidate_display_list_queue.remove_smallest_child(target)

                    if DEBUG_LAYOUT:
                        self._logger.close()

                if self._invalidate_display_list_queue.is_empty():
                    self._invalidate_display_list_flag = False

        if old_target_level == sys.maxsize:
            self._target_level = sys.maxsize
            if not skip_display_list:
                if DEBUG_LAYOUT:
                    self._logger.log("Complete!")

                obj = self._update_complete_queue.remove_largest_child(target)
                while obj is not None:
                    if DEBUG_LAYOUT:
                        self._logger.log(obj)

                    if not obj.initialized:
                        obj.initialized = True
                    obj.update_complete()
                    obj.update_complete_pending_flag = False
                    obj = self._update_complete_queue.remove_largest_child(target)

        self._current_object = last_current_object

    def is_invalid(self):
        """
        Returns <code>true</code> if there are components that need validating
        <code>false</code> if all components have been validated.

        @return Returns <code>true</code> if there are components that need validating
        <code>false</code> if all components have been validated.
        """
        return self._invalidate_properties_flag or self._invalidate_size_flag or self._invalidate_display_list_flag

    def attach_listeners(self):
        if DEBUG_LAYOUT:
            self._logger.log("attach_listeners")

        self._listeners_attached = True

    def do_phased_instantiation_callback(self):
        if DEBUG_LAYOUT:
            self._logger.open("do_phased_instantiation_callback")

        self.do_phased_instantiation()
        self._current_object = None

        if DEBUG_LAYOUT:
            self._logger.close("do_phased_instantiation_callback")

    def update(self):
        if not self._listeners_attached:
            # if DEBUG_LAYOUT:
            #     self._logger.close("Nothing to do")
            return

        if DEBUG_LAYOUT:
            self._logger.open("update")

        # print('##### QUEUES #####')
        # print('prop', self._invalidate_properties_queue)
        # print('size', self._invalidate_size_queue)
        # print('disp', self._invalidate_display_list_queue)
        # print('comp', self._update_complete_queue)
        # print('#################')

        self.do_phased_instantiation_callback()

        if DEBUG_LAYOUT:
            self._logger.close("update")
