from enum import Enum, unique
from ...engine import Object3D
from ...engine.geometry import Plane
from ...engine.materials import MeshBasicMaterial
from ...utils import Colors
from . import Component, Group, Label


@unique
class ButtonState(Enum):
    UP = 0
    OVER = 1
    DOWN = 2


class Button(Group):
    def __init__(
        self,
        label,
        horizontal_label_align='LEFT',
        vertical_label_align='MIDDLE',
        on_click=None,
        toggle=False
    ):
        super().__init__()

        self._interactive = True  # Important!

        self.on_click = on_click

        self._padding_top = 0.125
        self._padding_bottom = 0.125
        self._padding_left = 0.125
        self._padding_right = 0.125

        self._disabled_color = Colors.foreground
        self._up_color = Colors.white
        self._over_color = (0.125, 0.125, 0.125)  # Colors.white
        self._down_color = Colors.black
        self._selected_up_color = (0.125, 0.125, 0.125)  # Colors.white
        self._selected_over_color = (0.125, 0.125, 0.125)  # Colors.white
        self._selected_down_color = (0.125, 0.125, 0.125)  # Colors.white

        self._disabled_background_color = Colors.background
        self._up_background_color = (0.125, 0.125, 0.125)  # Colors.dark
        self._over_background_color = Colors.blue_accent  # Colors.medium
        self._down_background_color = Colors.blue_accent  # Colors.light
        self._selected_up_background_color = Colors.yellow
        self._selected_over_background_color = Colors.yellow
        self._selected_down_background_color = Colors.yellow

        self._toggle = toggle
        self._selected = False
        self._state = ButtonState.UP

        background = Component()
        self.add_element(background)

        self._background_plane = Plane()
        self._background = Object3D(geometry=self._background_plane, material=MeshBasicMaterial())
        self._background.material.flat = True
        background.add_child(self._background)

        self._label = Label(text=label, font="SourceSansPro-Bold.ttf")
        self._label.align = horizontal_label_align
        self._label.vertical_align = vertical_label_align
        self.add_element(self._label)

    # region Dimensions

    @property
    def padding_top(self):
        return self._padding_top
        
    @padding_top.setter
    def padding_top(self, value):
        if self._padding_top != value:
            self._padding_top = value
            self.invalidate_properties()
            self.invalidate_display_list()

    @property
    def padding_bottom(self):
        return self._padding_bottom

    @padding_bottom.setter
    def padding_bottom(self, value):
        if self._padding_bottom != value:
            self._padding_bottom = value
            self.invalidate_properties()
            self.invalidate_display_list()

    @property
    def padding_left(self):
        return self._padding_left

    @padding_left.setter
    def padding_left(self, value):
        if self._padding_left != value:
            self._padding_left = value
            self.invalidate_properties()
            self.invalidate_display_list()

    @property
    def padding_right(self):
        return self._padding_right

    @padding_right.setter
    def padding_right(self, value):
        if self._padding_right != value:
            self._padding_right = value
            self.invalidate_properties()
            self.invalidate_display_list()
            
    # endregion

    # region Behavior
    
    @property
    def toggle(self):
        return self._toggle

    @toggle.setter
    def toggle(self, value):
        if self._toggle != value:
            self._toggle = value
            self.invalidate_properties()
            
    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, value):
        if self._selected != value:
            self._selected = value
            self.invalidate_properties()

    def on_cursor_released(self):
        super().on_cursor_released()
        if not self._enabled:
            return
        if self._toggle:
            self.selected = not self._selected
        if self.on_click is not None and callable(self.on_click):
            self.on_click(self)

    # endregion

    # region Label

    @property
    def label(self):
        return self._label.text

    @label.setter
    def label(self, value):
        if self._label.text != value:
            self._label.text = value
            # self.invalidate_parent_size_and_display_list()
            # self.invalidate_size()
            # self.invalidate_display_list()
    
    @property
    def horizontal_label_align(self):
        return self._label.align

    @horizontal_label_align.setter
    def horizontal_label_align(self, value):
        if self._label.align != value:
            self._label.align = value

    @property
    def vertical_label_align(self):
        return self._label.vertical_align

    @vertical_label_align.setter
    def vertical_label_align(self, value):
        if self._label.vertical_align != value:
            self._label.vertical_align = value

    # endregion

    def commit_properties(self):
        super().commit_properties()

        # State
        if self._cursor_over:
            self._state = ButtonState.OVER
        elif self._cursor_out:
            self._state = ButtonState.UP

        if not self._enabled:
            self._background.material.color = self._disabled_background_color
        elif self._state is ButtonState.UP:
            self._background.material.color = self._selected_up_background_color if self._selected else self._up_background_color
        elif self._state is ButtonState.OVER:
            self._background.material.color = self._selected_over_background_color if self._selected else self._over_background_color
        elif self._state is ButtonState.DOWN:
            self._background.material.color = self._selected_down_background_color if self._selected else self._down_background_color

        # Label
        if not self._enabled:
            self._label.color = self._disabled_color
        elif self._state is ButtonState.UP:
            self._label.color = self._selected_up_color if self._selected else self._up_color
        elif self._state is ButtonState.OVER:
            self._label.color = self._selected_over_color if self._selected else self._over_color
        elif self._state is ButtonState.DOWN:
            self._label.color = self._selected_down_color if self._selected else self._down_color

        # Padding
        self._label.left = self._padding_left
        self._label.right = self._padding_right
        self._label.top = self._padding_top
        self._label.bottom = self._padding_bottom

    def update_display_list(self, width, height, depth):
        super().update_display_list(width, height, depth)

        self._background.x = width / 2
        self._background.y = height / 2
        self._background_plane.width = width
        self._background_plane.height = height
