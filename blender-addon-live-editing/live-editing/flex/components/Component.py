import weakref
import random
from ...engine import Object3D
from ...engine.geometry import Plane
from ...Constants import DEBUG_LAYOUT, DEBUG_LAYOUT_VISUAL
from ...utils import Logger

DEFAULT_MAX_WIDTH = 10000
DEFAULT_MAX_HEIGHT = 10000
DEFAULT_MAX_DEPTH = 10000


class Component(Object3D):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if DEBUG_LAYOUT:
            self._logger = Logger(self)

        self._top = None
        self._bottom = None
        self._left = None
        self._right = None
        self._front = None
        self._back = None

        self._horizontal_center = None
        self._vertical_center = None
        self._depth_center = None

        self._width = 0
        self._height = 0
        self._depth = 0

        self._percent_width = None
        self._percent_height = None
        self._percent_depth = None

        self._explicit_width = None
        self._explicit_height = None
        self._explicit_depth = None

        self._explicit_min_width = None
        self._explicit_max_width = None
        self._explicit_min_height = None
        self._explicit_max_height = None
        self._explicit_min_depth = None
        self._explicit_max_depth = None

        self._measured_width = None
        self._measured_height = None
        self._measured_depth = None
        self._measured_min_width = None
        self._measured_min_height = None
        self._measured_min_depth = None
        self._measured_max_width = None
        self._measured_max_height = None
        self._measured_max_depth = None

        self._old_width = None
        self._old_height = None
        self._old_depth = None
        self._old_min_width = None
        self._old_min_height = None
        self._old_min_depth = None
        self._old_explicit_width = None
        self._old_explicit_height = None
        self._old_explicit_depth = None

        self._visible = False  # Will be reset to _explicit_visible in initialize()
        self._explicit_visible = True
        self._bypass_render = True

        self._initialized = False
        self._owner = None
        self._nest_level = 0
        self._enabled = True
        self._include_in_layout = True

        self._invalidate_properties_flag = False
        self._invalidate_size_flag = False
        self._invalidate_display_list_flag = False
        self.update_complete_pending_flag = False

        if DEBUG_LAYOUT_VISUAL:
            if DEBUG_LAYOUT:
                self._logger.open("adding debug object")
            self._debug_object = Object3D(geometry=Plane(), material=MeshBasicMaterial())
            self._debug_object.z = -0.0001
            self._debug_object.material.color = (random.random(), random.random(), random.random())
            self.add_child(self._debug_object)
            if DEBUG_LAYOUT:
                self._logger.close()

    def __str__(self):
        return self.__class__.__name__ + " (init: " + str(self.initialized) + ", level: " + str(self.nest_level) + ", visible: " + str(self.visible) + ")"

    # region Lifecycle

    @property
    def initialized(self):
        return self._initialized

    @initialized.setter
    def initialized(self, value):
        self._initialized = value
        if self._initialized:
            self.set_visible(self._explicit_visible)

    def initialize(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: initialize")

        if self._initialized:
            if DEBUG_LAYOUT:
                self._logger.close("already initialized")

            return

        self.create_children()
        self.children_created()
        self.initialization_complete()

        if DEBUG_LAYOUT:
            self._logger.close()

    def create_children(self):
        pass

    def children_created(self):
        if DEBUG_LAYOUT:
            self._logger.open('Component: children_created')

        self.invalidate_properties()
        self.invalidate_size()
        self.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    def initialization_complete(self):
        pass

    def update_callbacks(self):
        if self._invalidate_display_list_flag:
            self.layout_manager.invalidate_display_list(self)

        if self._invalidate_size_flag:
            self.layout_manager.invalidate_size(self)

        if self._invalidate_properties_flag:
            self.layout_manager.invalidate_properties(self)

    # endregion

    # region Relationships

    def parent_changed(self, p):
        if p is None:
            self._parent = None
            self.nest_level = 0
        elif isinstance(p, Component):
            self._parent = weakref.ref(p)
        else:
            # XXX: What is this? It doesn't answer the case of p not being a Component
            self._parent = weakref.ref(p.parent)

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, value):
        if self._owner != value:
            self._owner = value

    @property
    def root(self):
        return self.parent.root if self.parent is not None and isinstance(self.parent, Component) else None

    @property
    def layout_manager(self):
        return self.root.layout_manager if self.root is not None else None

    @property
    def nest_level(self):
        return self._nest_level

    @nest_level.setter
    def nest_level(self, value):
        #  If my parent hasn't been attached to the display list, then its nest_level
        #  will be zero.  If it tries to set my nest_level to 1, ignore it.  We'll
        #  update nest levels again after the parent is added to the display list.
        if value == 1:
            return

        # Also punt if the new value for nest_level is the same as my current value.
        #  TODO: (aharui) add early exit if nest_level isn't changing
        if value > 1 and self._nest_level != value:
            self._nest_level = value
            self.update_callbacks()
            value += 1
        elif value == 0:
            self._nest_level = value = 0
        else:
            value += 1

        for i in range(0, self.num_children):
            ui = self.get_child_at(i)
            if isinstance(ui, Component):
                ui.nest_level = value

    # endregion

    # region Children

    def add_child(self, child):
        if DEBUG_LAYOUT:
            self._logger.open("Component: add_child", child)

        former_parent = child.parent

        if former_parent is not None:
            former_parent.remove_child(child)

        # Do anything that needs to be done before the child is added.
        #  When adding a child to UIComponent, this will set the child's
        #  virtual parent, its nest_level, its document, etc.
        #  When adding a child to a Container, the override will also
        #  invalidate the container, adjust its content/chrome partitions,
        #  etc.
        self.adding_child(child)

        #  Call a low-level player method in DisplayObjectContainer which
        #  actually attaches the child to this component.
        #  The player dispatches an "added" event from the child just after
        #  it is attached, so all "added" handlers execute during this call.
        #  UIComponent registers an addedHandler() in its constructor,
        #  which makes it runs before any other "added" handlers except
        #  capture-phase ones it sets up the child's styles.
        super()._add_child(child)

        #  Do anything that needs to be done after the child is added
        #  and after all "added" handlers have executed.
        #  This is where
        self.child_added(child)

        if DEBUG_LAYOUT:
            self._logger.close()

    def add_child_at(self, child, index):
        if DEBUG_LAYOUT:
            self._logger.open("Component: add_child_at", child, index)

        former_parent = child.parent
        if former_parent is not None:
            if DEBUG_LAYOUT:
                self._logger.log("child had a former parent", former_parent)
            former_parent.remove_child(child)
        self.adding_child(child)
        super()._add_child_at(child, index)
        self.child_added(child)

        if DEBUG_LAYOUT:
            self._logger.close()

    def remove_child(self, child):
        if DEBUG_LAYOUT:
            self._logger.open("Component: remove_child", child)

        self.removing_child(child)
        super().remove_child(child)
        self.child_removed(child)

        if DEBUG_LAYOUT:
            self._logger.close()

    def remove_child_at(self, index):
        if DEBUG_LAYOUT:
            self._logger.open("Component: remove_child_at", index)

        child = self.get_child_at(index)
        self.removing_child(child)
        super().remove_child(child)
        self.child_removed(child)

        if DEBUG_LAYOUT:
            self._logger.close()

    def adding_child(self, child):
        if DEBUG_LAYOUT:
            self._logger.open("Component: adding_child", child)

        if isinstance(child, Component):
            child.parent_changed(self)
            child.nest_level = self.nest_level + 1

        if DEBUG_LAYOUT:
            self._logger.close()

    def child_added(self, child):
        if DEBUG_LAYOUT:
            self._logger.open("Component: child_added", child)

        if isinstance(child, Component):
            if not child.initialized:
                child.initialize()

        # Offset a little bit so that we have some layer depth
        index = self._children.index(child)
        if DEBUG_LAYOUT:
            self._logger.log("setting offset matrix:", "index:", index)

        child._offset_matrix.translation.z = index * 0.001
        child._dirty_matrix = True

        if DEBUG_LAYOUT:
            self._logger.close()

    def removing_child(self, child):
        pass

    def child_removed(self, child):
        if DEBUG_LAYOUT:
            self._logger.open("Component: child_removed", child)

        if isinstance(child, Component):
            child.parent_changed(None)

        if DEBUG_LAYOUT:
            self._logger.close()

    # endregion

    # region Behavior & Visibility

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        if self._enabled != value:
            self._enabled = value
            self.invalidate_display_list()

    @property
    def visible(self):
        return self._explicit_visible

    @Object3D.visible.setter
    def visible(self, value):
        self.set_visible(value)

    def set_visible(self, value):
        self._explicit_visible = value

        if not self.initialized:
            return

        if self._visible != value:
            self._visible = value
            self._dirty_attributes = True

    @property
    def include_in_layout(self):
        return self._include_in_layout

    @include_in_layout.setter
    def include_in_layout(self, value):
        if self._include_in_layout != value:
            self._include_in_layout = value
            # Force, because we usually ignore this coming from non-included Components
            self.invalidate_parent_size_and_display_list(True)

    def set_hover(self, hovered, location, down):
        hover_changed = super().set_hover(hovered, location, down)
        if hover_changed:
            self.invalidate_properties()

    # endregion

    # region Position

    @Object3D.x.setter
    def x(self, value):
        if super().x != value:
            Object3D.x.fset(self, value)
            self.invalidate_properties()
            if self.parent is not None and isinstance(self.parent, Component):
                self.parent.child_xyz_changed()

    @Object3D.y.setter
    def y(self, value):
        if super().y != value:
            Object3D.y.fset(self, value)
            self.invalidate_properties()
            if self.parent is not None and isinstance(self.parent, Component):
                self.parent.child_xyz_changed()

    @Object3D.z.setter
    def z(self, value):
        if super().z != value:
            Object3D.z.fset(self, value)
            self.invalidate_properties()
            if self.parent is not None and isinstance(self.parent, Component):
                self.parent.child_xyz_changed()

    def move(self, x=None, y=None, z=None):
        if x is not None and x != self.x:
            Object3D.x.fset(self, x)

        if y is not None and y != self.y:
            Object3D.y.fset(self, y)

        if z is not None and z != self.z:
            Object3D.z.fset(self, z)

    def child_xyz_changed(self):
        pass

    # endregion

    # region Constraints

    @property
    def top(self):
        return self._top

    @top.setter
    def top(self, value):
        if self._top != value:
            self._top = value
            self.invalidate_parent_size_and_display_list()

    @property
    def bottom(self):
        return self._bottom

    @bottom.setter
    def bottom(self, value):
        if self._bottom != value:
            self._bottom = value
            self.invalidate_parent_size_and_display_list()

    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, value):
        if self._left != value:
            self._left = value
            self.invalidate_parent_size_and_display_list()

    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, value):
        if self._right != value:
            self._right = value
            self.invalidate_parent_size_and_display_list()

    @property
    def front(self):
        return self._front

    @front.setter
    def front(self, value):
        if self._front != value:
            self._front = value
            self.invalidate_parent_size_and_display_list()

    @property
    def back(self):
        return self._back

    @back.setter
    def back(self, value):
        if self._back != value:
            self._back = value
            self.invalidate_parent_size_and_display_list()

    @property
    def horizontal_center(self):
        return self._horizontal_center

    @horizontal_center.setter
    def horizontal_center(self, value):
        if self._horizontal_center != value:
            self._horizontal_center = value
            self.invalidate_parent_size_and_display_list()

    @property
    def vertical_center(self):
        return self._vertical_center

    @vertical_center.setter
    def vertical_center(self, value):
        if self._vertical_center != value:
            self._vertical_center = value
            self.invalidate_parent_size_and_display_list()

    @property
    def depth_center(self):
        return self._depth_center

    @depth_center.setter
    def depth_center(self, value):
        if self._depth_center != value:
            self._depth_center = value
            self.invalidate_parent_size_and_display_list()

    #endregion

    # region Size

    """
    WIDTH
    """

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        if self._explicit_width != value:
            self.explicit_width = value
            self.invalidate_size()

        if self._width != value:
            self._width = value
            self.invalidate_properties()
            self.invalidate_display_list()
            self.invalidate_parent_size_and_display_list()

    @property
    def explicit_width(self):
        return self._explicit_width

    @explicit_width.setter
    def explicit_width(self, value):
        if self._explicit_width != value:
            if value is not None:
                self._percent_width = None
            self._explicit_width = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def percent_width(self):
        return self._percent_width

    @percent_width.setter
    def percent_width(self, value):
        if self._percent_width != value:
            if value is not None:
                self._explicit_width = None
            self._percent_width = value
            self.invalidate_parent_size_and_display_list()

    @property
    def measured_min_width(self):
        return self._measured_min_width

    @property
    def min_width(self):
        if self._explicit_min_width is not None:
            return self._explicit_min_width
        return self._measured_min_width

    @min_width.setter
    def min_width(self, value):
        if self._explicit_min_width != value:
            self.explicit_min_width = value

    @property
    def explicit_min_width(self):
        return self._explicit_min_width

    @explicit_min_width.setter
    def explicit_min_width(self, value):
        if self._explicit_min_width != value:
            self._explicit_min_width = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def max_width(self):
        return self._explicit_max_width if self._explicit_max_width is not None else DEFAULT_MAX_WIDTH

    @max_width.setter
    def max_width(self, value):
        if self._explicit_max_width != value:
            self.explicit_max_width = value

    @property
    def explicit_max_width(self):
        return self._explicit_max_width

    @explicit_max_width.setter
    def explicit_max_width(self, value):
        if self._explicit_max_width != value:
            self._explicit_max_width = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def measured_width(self):
        return self._measured_width

    @measured_width.setter
    def measured_width(self, value):
        if self._measured_width != value:
            self._measured_width = value

    @property
    def explicit_or_measured_width(self):
        return self._explicit_width if self._explicit_width is not None else self._measured_width

    def get_max_bounds_width(self):
        return self._explicit_max_width if self._explicit_max_width is not None else DEFAULT_MAX_WIDTH

    def get_min_bounds_width(self):
        min_width = None
        if self._explicit_min_width is not None:
            min_width = self._explicit_min_width
        else:
            min_width = self._measured_min_width if self._measured_min_width is not None else 0
            if self._explicit_max_width is not None:
                min_width = min(min_width, self._explicit_max_width)
        return min_width

    """
    HEIGHT
    """

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        if self._explicit_height != value:
            self.explicit_height = value
            self.invalidate_size()

        if self._height != value:
            self._height = value
            self.invalidate_properties()
            self.invalidate_display_list()
            self.invalidate_parent_size_and_display_list()

    @property
    def explicit_height(self):
        return self._explicit_height

    @explicit_height.setter
    def explicit_height(self, value):
        if self._explicit_height != value:
            if value is not None:
                self._percent_height = None
            self._explicit_height = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def percent_height(self):
        return self._percent_height

    @percent_height.setter
    def percent_height(self, value):
        if self._percent_height != value:
            if value is not None:
                self._explicit_height = None
            self._percent_height = value
            self.invalidate_parent_size_and_display_list()

    @property
    def measured_min_height(self):
        return self._measured_min_height

    @property
    def min_height(self):
        if self._explicit_min_height is not None:
            return self._explicit_min_height
        return self._measured_min_height

    @min_height.setter
    def min_height(self, value):
        if self._explicit_min_height != value:
            self.explicit_min_height = value

    @property
    def explicit_min_height(self):
        return self._explicit_min_height

    @explicit_min_height.setter
    def explicit_min_height(self, value):
        if self._explicit_min_height != value:
            self._explicit_min_height = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def max_height(self):
        return self._explicit_max_height if self._explicit_max_height is not None else DEFAULT_MAX_HEIGHT

    @max_height.setter
    def max_height(self, value):
        if self._explicit_max_height != value:
            self.explicit_max_height = value

    @property
    def explicit_max_height(self):
        return self._explicit_max_height

    @explicit_max_height.setter
    def explicit_max_height(self, value):
        if self._explicit_max_height != value:
            self._explicit_max_height = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def measured_height(self):
        return self._measured_height

    @measured_height.setter
    def measured_height(self, value):
        if self._measured_height != value:
            self._measured_height = value

    @property
    def explicit_or_measured_height(self):
        return self._explicit_height if self._explicit_height is not None else self._measured_height

    def get_max_bounds_height(self):
        return self._explicit_max_height if self._explicit_max_height is not None else DEFAULT_MAX_HEIGHT

    def get_min_bounds_height(self):
        min_height = None
        if self._explicit_min_height is not None:
            min_height = self._explicit_min_height
        else:
            min_height = self._measured_min_height if self._measured_min_height is not None else 0
            if self._explicit_max_height is not None:
                min_height = min(min_height, self._explicit_max_height)
        return min_height

    """
    DEPTH
    """

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        if self._explicit_depth != value:
            self.explicit_depth = value
            self.invalidate_size()

        if self._depth != value:
            self._depth = value
            self.invalidate_properties()
            self.invalidate_display_list()
            self.invalidate_parent_size_and_display_list()

    @property
    def explicit_depth(self):
        return self._explicit_depth

    @explicit_depth.setter
    def explicit_depth(self, value):
        if self._explicit_depth != value:
            if value is not None:
                self._percent_depth = None
            self._explicit_depth = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def percent_depth(self):
        return self._percent_depth

    @percent_depth.setter
    def percent_depth(self, value):
        if self._percent_depth != value:
            if value is not None:
                self._explicit_depth = None
            self._percent_depth = value
            self.invalidate_parent_size_and_display_list()

    @property
    def measured_min_depth(self):
        return self._measured_min_depth

    @property
    def min_depth(self):
        if self._explicit_min_depth is not None:
            return self._explicit_min_depth
        return self._measured_min_depth

    @min_depth.setter
    def min_depth(self, value):
        if self._explicit_min_depth != value:
            self.explicit_min_depth = value

    @property
    def explicit_min_depth(self):
        return self._explicit_min_depth

    @explicit_min_depth.setter
    def explicit_min_depth(self, value):
        if self._explicit_min_depth != value:
            self._explicit_min_depth = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def max_depth(self):
        return self._explicit_max_depth if self._explicit_max_depth is not None else DEFAULT_MAX_DEPTH

    @max_depth.setter
    def max_depth(self, value):
        if self._explicit_max_depth != value:
            self.explicit_max_depth = value

    @property
    def explicit_max_depth(self):
        return self._explicit_max_depth

    @explicit_max_depth.setter
    def explicit_max_depth(self, value):
        if self._explicit_max_depth != value:
            self._explicit_max_depth = value
            self.invalidate_size()
            self.invalidate_parent_size_and_display_list()

    @property
    def measured_depth(self):
        return self._measured_depth

    @measured_depth.setter
    def measured_depth(self, value):
        if self._measured_depth != value:
            self._measured_depth = value

    @property
    def explicit_or_measured_depth(self):
        return self._explicit_depth if self._explicit_depth is not None else self._measured_depth

    def get_max_bounds_depth(self):
        return self._explicit_max_depth if self._explicit_max_depth is not None else DEFAULT_MAX_DEPTH

    def get_min_bounds_depth(self):
        min_depth = None
        if self._explicit_min_depth is not None:
            min_depth = self._explicit_min_depth
        else:
            min_depth = self._measured_min_depth if self._measured_min_depth is not None else 0
            if self._explicit_max_depth is not None:
                min_depth = min(min_depth, self._explicit_max_depth)
        return min_depth

    """
    SIZE
    """

    def set_actual_size(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("Component: set_actual_size", width, height, depth)

        changed = False

        if self._width != width:
            self._width = width
            changed = True

        if self._height != height:
            self._height = height
            changed = True

        if self._depth != depth:
            self._depth = depth
            changed = True

        if changed:
            self.invalidate_display_list()
            self.dispatch_resize_event()

        if DEBUG_LAYOUT:
            self._logger.close()

    def set_layout_bounds_size(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("Component: set_layout_bounds_size", width, height, depth)

        if width is None:
            if DEBUG_LAYOUT:
                self._logger.log("width is None using explicit_or_measured_width:", self.explicit_or_measured_width)
            width = self.explicit_or_measured_width

        if height is None:
            if DEBUG_LAYOUT:
                self._logger.log("height is None using explicit_or_measured_height:", self.explicit_or_measured_height)
            height = self.explicit_or_measured_height

        if depth is None:
            if DEBUG_LAYOUT:
                self._logger.log("depth is None using explicit_or_measured_depth:", self.explicit_or_measured_depth)
            depth = self.explicit_or_measured_depth

        self.set_actual_size(width, height, depth)

        if DEBUG_LAYOUT:
            self._logger.close()

    # endregion

    # region Invalidation

    def invalidate_properties(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: invalidate_properties")

        if not self._invalidate_properties_flag:
            if DEBUG_LAYOUT:
                self._logger.log("setting flag")

            self._invalidate_properties_flag = True
            if self.nest_level > 0 and self.layout_manager is not None:
                if DEBUG_LAYOUT:
                    self._logger.log("calling manager")

                self.layout_manager.invalidate_properties(self)

            elif DEBUG_LAYOUT:
                if self.nest_level < 0:
                    self._logger.log("nest_level is 0")
                else:
                    self._logger.log("no layout manager")

        elif DEBUG_LAYOUT:
            self._logger.log('already invalid')

        if DEBUG_LAYOUT:
            self._logger.close()

    def invalidate_size(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: invalidate_size")

        if not self._invalidate_size_flag:
            if DEBUG_LAYOUT:
                self._logger.log("setting flag")

            self._invalidate_size_flag = True
            if self.nest_level > 0 and self.layout_manager is not None:
                if DEBUG_LAYOUT:
                    self._logger.log("calling manager")

                self.layout_manager.invalidate_size(self)

            elif DEBUG_LAYOUT:
                if self.nest_level < 0:
                    self._logger.log("nest_level is 0")
                else:
                    self._logger.log("no layout manager")

        elif DEBUG_LAYOUT:
            self._logger.log('already invalid')

        if DEBUG_LAYOUT:
            self._logger.close()

    def invalidate_parent_size_and_display_list(self, force=False):
        if DEBUG_LAYOUT:
            self._logger.open("Component: invalidate_parent_size_and_display_list")

        if ( not force and not self.include_in_layout) or self.parent is None or not isinstance(self.parent, Component):
            if DEBUG_LAYOUT:
                if not self.include_in_layout:
                    self._logger.close("not included in layout")
                elif self.parent is None:
                    self._logger.close("not parent")
                else:
                    self._logger.close("parent is not a component")
            return

        self.parent.invalidate_size()
        self.parent.invalidate_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    def invalidate_display_list(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: invalidate_display_list")

        if not self._invalidate_display_list_flag:
            if DEBUG_LAYOUT:
                self._logger.log("setting flag")

            self._invalidate_display_list_flag = True
            if self.nest_level > 0 and self.layout_manager is not None:
                if DEBUG_LAYOUT:
                    self._logger.log("calling manager")

                self.layout_manager.invalidate_display_list(self)

            elif DEBUG_LAYOUT:
                if self.nest_level < 0:
                    self._logger.log("nest_level is 0")
                else:
                    self._logger.log("no layout manager")

        elif DEBUG_LAYOUT:
            self._logger.log('already invalid')

        if DEBUG_LAYOUT:
            self._logger.close()

    def validate_now(self):
        if self.layout_manager is not None:
            self.layout_manager.validate_client(self)

    def validate_properties(self):
        if self._invalidate_properties_flag:
            self.commit_properties()
            self._invalidate_properties_flag = False

    def commit_properties(self):
        if self.width != self._old_width or self.height != self._old_height or self.depth != self._old_depth:
            self.dispatch_resize_event()

    def validate_size(self, recursive=False):
        if DEBUG_LAYOUT:
            self._logger.open('Component: validate_size', 'recursive=' + str(recursive))

        if recursive:
            for i in range(0, self.num_children):
                child = self.get_child_at(i)
                if isinstance(child, Component):
                    child.validate_size(True)

        if self._invalidate_size_flag:
            if DEBUG_LAYOUT:
                self._logger.log("Validating...")

            size_changing = self.measure_sizes()
            if size_changing and self.include_in_layout:
                if DEBUG_LAYOUT:
                    self._logger.log("Size changed, invalidating parent size and display list")

                # TODO(egeorgie): we don't need this invalidate_display_list() here
                # because we'll call it if the parent sets new actual size?
                self.invalidate_display_list()
                self.invalidate_parent_size_and_display_list()

        if DEBUG_LAYOUT:
            self._logger.close()

    def validate_display_list(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: validate_display_list")

        if self._invalidate_display_list_flag:
            # XXX: Check if we are top level, this used to be a check if parent was root in flash
            if self.nest_level == 1:
                if DEBUG_LAYOUT:
                    self._logger.log("We are top-level, set actual size", self.explicit_or_measured_width, self.explicit_or_measured_height, self.explicit_or_measured_depth)

                self.set_actual_size(
                    self.explicit_or_measured_width,
                    self.explicit_or_measured_height,
                    self.explicit_or_measured_depth
                )

            if DEBUG_LAYOUT:
                self._logger.open("update_display_list", self.width, self.height, self.depth)

            self.update_display_list(self.width, self.height, self.depth)
            self._invalidate_display_list_flag = False

            if DEBUG_LAYOUT:
                self._logger.close()

        if DEBUG_LAYOUT:
            self._logger.close()

    def update_display_list(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open("Component: update_display_list", width, height, depth)

        if DEBUG_LAYOUT_VISUAL:
            if DEBUG_LAYOUT:
                self._logger.log("resizing debug shape")
            self._debug_object.x = width / 2
            self._debug_object.y = height / 2
            self._debug_object.z = depth / 2
            self._debug_object.geometry.width = width
            self._debug_object.geometry.height = height
            self._debug_object.geometry.depth = depth

        if DEBUG_LAYOUT:
            self._logger.close()

    def update_complete(self):
        pass

    # endregion

    # region Measurements

    def can_skip_measurement(self):
        return self.explicit_width is not None and self.explicit_height is not None and self.explicit_depth is not None

    def measure_sizes(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: measure_sizes")

        changed = False

        if not self._invalidate_size_flag:
            if DEBUG_LAYOUT:
                self._logger.close("valid!")

            return changed

        if self.can_skip_measurement():
            if DEBUG_LAYOUT:
                self._logger.log("can skip measurements")

            self._invalidate_size_flag = False
            self._measured_min_width = 0
            self._measured_min_height = 0
            self._measured_min_depth = 0
        else:
            self.measure()

            self._invalidate_size_flag = False

            if self.explicit_min_width is not None and self._measured_width < self.explicit_min_width:
                self._measured_width = self.explicit_min_width

            if self.explicit_max_width is not None and self._measured_width > self.explicit_max_width:
                self._measured_width = self.explicit_max_width

            if self.explicit_min_height is not None and self._measured_height < self.explicit_min_height:
                self._measured_height = self.explicit_min_height

            if self.explicit_max_height is not None and self._measured_height > self.explicit_max_height:
                self._measured_height = self.explicit_max_height

            if self.explicit_min_depth is not None and self._measured_depth < self.explicit_min_depth:
                self._measured_depth = self.explicit_min_depth

            if self.explicit_max_depth is not None and self._measured_depth > self.explicit_max_depth:
                self._measured_depth = self.explicit_max_depth

        if self._old_min_width is None or self._old_min_height is None or self._old_min_depth is None:
            self._old_min_width = self.explicit_min_width if self.explicit_min_width is not None else self.measured_min_width
            self._old_min_height = self.explicit_min_height if self.explicit_min_height is not None else self.measured_min_height
            self._old_min_depth = self.explicit_min_depth if self.explicit_min_depth is not None else self.measured_min_depth
            self._old_explicit_width = self.explicit_width if self.explicit_width is not None else self.measured_width
            self._old_explicit_height = self.explicit_height if self.explicit_height is not None else self.measured_height
            self._old_explicit_depth = self.explicit_depth if self.explicit_depth is not None else self.measured_depth
            changed = True

        else:
            new_value = self.explicit_min_width if self.explicit_min_width is not None else self.measured_min_width
            if new_value != self._old_min_width:
                self._old_min_width = new_value
                changed = True

            new_value = self.explicit_min_height if self.explicit_min_height is not None else self.measured_min_height
            if new_value != self._old_min_height:
                self._old_min_height = new_value
                changed = True

            new_value = self.explicit_min_depth if self.explicit_min_depth is not None else self.measured_min_depth
            if new_value != self._old_min_depth:
                self._old_min_depth = new_value
                changed = True

            new_value = self.explicit_width if self.explicit_width is not None else self.measured_width
            if new_value != self._old_explicit_width:
                self._old_explicit_width = new_value
                changed = True

            new_value = self.explicit_height if self.explicit_height is not None else self.measured_height
            if new_value != self._old_explicit_height:
                self._old_explicit_height = new_value
                changed = True

            new_value = self.explicit_depth if self.explicit_depth is not None else self.measured_depth
            if new_value != self._old_explicit_depth:
                self._old_explicit_depth = new_value
                changed = True

        if DEBUG_LAYOUT:
            self._logger.close("changed: " + str(changed))

        return changed

    def measure(self):
        if DEBUG_LAYOUT:
            self._logger.open("Component: measure")

        self._measured_min_width = 0.00
        self._measured_min_height = 0.00
        self._measured_min_depth = 0.00
        self._measured_width = 0.00
        self._measured_height = 0.00
        self._measured_depth = 0.00

        if DEBUG_LAYOUT:
            self._logger.close()

    # endregion

    # region Helpers

    def dispatch_resize_event(self):
        # if ( hasEventListener( ResizeEvent.RESIZE ) ) {
        #     var resizeEvent : ResizeEvent = new ResizeEvent(ResizeEvent.RESIZE)
        #     resizeEvent.old_width = self._old_width
        #     resizeEvent.old_height = self._old_height
        #     resizeEvent.old_depth = self._old_depth
        #     dispatchEvent( resizeEvent )
        # }

        self._old_width = self.width
        self._old_height = self.height
        self._old_depth = self.depth

    # endregion
