from . import Component
from ...Constants import DEBUG_LAYOUT
from ...engine import Object3D, TextField


class Label(Component):

    def __init__(self, text="Label", font=None, size=.30, color=(1.0, 1.0, 1.0), shadow=None, align='LEFT'):
        super().__init__()

        self._text_field = TextField(
            text=text,
            font=font if font is not None else "SourceSansPro-Light.ttf",
            size=size,
            color=color,
            shadow=shadow
        )
        self._text_field.material.flat = True
        self._text_field.material.flip_y = False

        self._padding_top = 0.0
        self._padding_bottom = 0.0
        self._padding_left = 0.0
        self._padding_right = 0.0

        self._ratio = 0.00
        self._text_width = 0.0
        self._text_height = 0.0
        self._text_width_scaled = None
        self._text_height_scaled = None

        self._text_offset = (0.00, 0.00)
        self._shadow_offset = 0.00

        self._width_constraint = None
        self._align = align
        self._vertical_align = 'TOP'

    def create_children(self):
        super().create_children()
        self.add_child(self._text_field)

    # region TextField

    @property
    def material(self):
        return self._text_field.material

    @Object3D.material.setter
    def material(self, value):
        self._text_field.material = self._material

    @property
    def text(self):
        return self._text_field.text

    @text.setter
    def text(self, value):
        value = str(value)
        if self._text_field.text != value:
            self._text_field.text = value
            self.invalidate_properties()
            self.invalidate_size()
            self.invalidate_display_list()

    @property
    def font(self):
        return self._text_field.font

    @font.setter
    def font(self, value):
        if self._text_field.font != value:
            self._text_field.font = value
            self.invalidate_properties()

    @property
    def size(self):
        return self._text_field.size

    @size.setter
    def size(self, value):
        if self._text_field.size != value:
            self._text_field.size = value
            self.invalidate_properties()

    @property
    def color(self):
        return self._text_field.color

    @color.setter
    def color(self, value):
        if self._text_field.color != value:
            self._text_field.color = value
            self.invalidate_properties()

    @property
    def shadow(self):
        return self._text_field.shadow

    @shadow.setter
    def shadow(self, value):
        if self._text_field.shadow != value:
            self._text_field.shadow = value
            self.invalidate_properties()

    # endregion

    # region Alignment

    @property
    def align(self):
        return self._align

    @align.setter
    def align(self, value):
        if self._align != value:
            self._align = value
            self.invalidate_properties()

    @property
    def vertical_align(self):
        return self._vertical_align

    @vertical_align.setter
    def vertical_align(self, value):
        if self._vertical_align != value:
            self._vertical_align = value
            self.invalidate_properties()

    # endregion

    # region Dimensions

    @property
    def padding_top(self):
        return self._padding_top

    @padding_top.setter
    def padding_top(self, value):
        if self._padding_top != value:
            self._padding_top = value
            self.invalidate_properties()
            self.invalidate_display_list()

    @property
    def padding_bottom(self):
        return self._padding_bottom

    @padding_bottom.setter
    def padding_bottom(self, value):
        if self._padding_bottom != value:
            self._padding_bottom = value
            self.invalidate_properties()
            self.invalidate_display_list()

    @property
    def padding_left(self):
        return self._padding_left

    @padding_left.setter
    def padding_left(self, value):
        if self._padding_left != value:
            self._padding_left = value
            self.invalidate_properties()
            self.invalidate_display_list()

    @property
    def padding_right(self):
        return self._padding_right

    @padding_right.setter
    def padding_right(self, value):
        if self._padding_right != value:
            self._padding_right = value
            self.invalidate_properties()
            self.invalidate_display_list()

    # endregion

    def resize_text_field(self, width, height):
        if DEBUG_LAYOUT:
            self._logger.open('Label: resize_text_field', width, height)

        # self._text_field.geometry.width = self._text_width_scaled  # width - (self._padding_left + self._padding_right)
        # self._text_field.geometry.height = self._text_height_scaled  # height - (self._padding_top + self._padding_bottom)

        # if width is None and height is None:
        #     self._text_field.width = self._text_width / self._resolution_CHANGE_ME
        #     self._text_field.height = self._text_height / self._resolution_CHANGE_ME
        # elif width is not None:
        #     self._text_field.width = width - (self._padding_left + self._padding_right)
        #     self._text_field.height = self._text_field.width * self._ratio
        # elif height is not None:
        #     self._text_field.height = height - (self._padding_top + self._padding_bottom)
        #     self._text_field.width = self._text_field.height / self._ratio

        if DEBUG_LAYOUT:
            self._logger.close()

    def measure(self):
        if DEBUG_LAYOUT:
            self._logger.open('Label: measure')

        super().measure()

        if self._text_field is None:
            self.measured_width = self._padding_left + self._padding_right
            self.measured_height = self._padding_top + self._padding_bottom
        else:
            self.measured_width = self._text_field.text_width + self._padding_left + self._padding_right
            self.measured_height = self._text_field.text_height + self._padding_top + self._padding_bottom

        if DEBUG_LAYOUT:
            self._logger.close(self.measured_width, self.measured_height)

    def set_layout_bounds_size(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open('Label: set_layout_bounds_size', width, height, depth)

        super().set_layout_bounds_size(width, height, depth)

        self.invalidate_size()

        if DEBUG_LAYOUT:
            self._logger.close()

    def update_display_list(self, width, height, depth):
        if DEBUG_LAYOUT:
            self._logger.open('Label: update display_list', width, height, depth)

        super().update_display_list(width, height, depth)

        self.resize_text_field(width, height)

        if self._align == 'CENTER':
            self._text_field.x = (self._text_field.geometry.width / 2) + ((width - self._text_field.geometry.width ) * 0.5 )
        elif self._align == 'RIGHT':
            self._text_field.x = width - self._padding_right - (self._text_field.geometry.width / 2)
        else:  # if self._align == 'LEFT':
            self._text_field.x = self._padding_left + (self._text_field.geometry.width / 2)

        if self._vertical_align == 'MIDDLE':
            self._text_field.y = (self._text_field.geometry.height / 2) + ((height - self._text_field.geometry.height ) * 0.5 )
        elif self._vertical_align == 'BOTTOM':
            self._text_field.y = height - self._padding_bottom - (self._text_field.geometry.height / 2)
        else:  # if self._vertical_align == 'TOP'
            self._text_field.y = self._padding_top + (self._text_field.geometry.height / 2)

        if DEBUG_LAYOUT:
            self._logger.close()

    def update_complete(self):
        super().update_complete()
        # self._width_constraint = None

    def validate_properties(self):
        if DEBUG_LAYOUT:
            self._logger.open("Label: validate_properties")

        super().validate_properties()

        if self._text_field.text_width <= 0 or self._text_field.text_height <= 0:
            if DEBUG_LAYOUT:
                self._logger.log("size is zero in one or more dimension(s), will not render an image")

            self._bypass_render = True
        else:
            self._bypass_render = False

        if DEBUG_LAYOUT:
            self._logger.close()
