import math
from ...engine import Object3D
from ...engine.geometry import Plane
from ...engine.materials import MeshBasicMaterial
from ...utils import Colors
from . import Component, VGroup, HGroup, Label


class Slider(VGroup):

    def __init__(self, label="Slider", min=0.00, max=1.00, value=0.50, on_change=None):
        super().__init__(gap=0.0625, horizontal_align='JUSTIFY')

        self._interactive = True  # Important!

        self._on_change = on_change

        self._track_cursor = False
        self._last_trackpad_value = 0.00

        self._min = min
        self._max = max
        self._value = value

        labels = HGroup()
        self.add_element(labels)

        self._label = Label(text=label, size=.20)
        self._label.percent_width = 1.00
        labels.add_element(self._label)

        self._value_label = Label(text="0.00", size=.20)
        labels.add_element(self._value_label)

        self._slider = Component()
        self._slider.percent_height = 1.00
        self._slider.min_height = 0.25
        self.add_element(self._slider)

        self._track = Object3D(geometry=Plane(), material=MeshBasicMaterial())
        self._track.material.flat = True
        self._track.material.color = (0.125, 0.125, 0.125)
        self._slider.add_child(self._track)

        self._range = Object3D(geometry=Plane(), material=MeshBasicMaterial())
        self._range.material.flat = True
        self._range.material.color = Colors.dark
        self._slider.add_child(self._range)

    # region Values

    @property
    def min(self):
        return self._min
    
    @min.setter
    def min(self, value):
        if self._min != value:
            self._min = value
            self._value = max(self._min, self._value)
            self._dirty_attributes = True

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, value):
        if self._max != value:
            self._max = value
            self._value = min(self._max, self._value)
            self._dirty_attributes = True

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, value):
        self._label = value
        self._dirty_attributes = True

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        if self._value != value:
            self._value = min(self._max, max(self._min, value))
            # Layout manager is too slow for such fast changing values
            # And also, this does not contribute to any change in position or size,
            # so just use the engine's invalidation
            self._dirty_attributes = True

    def _set_value(self, value):
        """
        Internal value setter that also calls the callback to notify the change
        :param value: 
        :return: 
        """
        self.value = value
        if self._on_change is not None and callable(self._on_change):
            self._on_change(self, self._value)

    # endregion

    def on_cursor_pressed(self):
        self._track_cursor = True

    def on_cursor_released(self):
        self._track_cursor = False

    def update_input(self):
        # FIXME: This fails when cursor is outside any interactive object, since we don't get a position when ray casting
        # if self._track_cursor:
        #     percent_value = (self._cursor_location.x + (self._width / 2)) / self._width
        #     self.value = (max(0.00, min(1.00, percent_value)) * (self._max - self._min)) + self._min

        if self._cursor_over:
            controller = self.engine.editor.controller2
            if controller.trackpad_button.touched:
                self._last_trackpad_value = controller.trackpad_x
            elif controller.trackpad_button.touching:
                controller_value = controller.trackpad_x
                multiplier = controller.trackpad_y + 1.0
                delta = (controller_value - self._last_trackpad_value) * multiplier
                self._set_value(max(self._min, min(self._max, self.value + delta)))
                self._last_trackpad_value = controller_value

        super().update_input()

    def on_cursor_entered(self):
        super().on_cursor_entered()
        self._track.material.color = (0.25, 0.25, 0.25)
        self._range.material.color = Colors.blue_accent

    def on_cursor_exited(self):
        super().on_cursor_entered()
        self._track.material.color = (0.125, 0.125, 0.125)
        self._range.material.color = Colors.dark

    def on_cursor_over(self):
        super().on_cursor_over()

        if self.engine.editor.controller1.trigger_button.down:
            percent_value = self._cursor_location.x / self._width
            self._set_value((max(0.00, min(1.00, percent_value)) * (self._max - self._min)) + self._min)

    def _update_slider(self):
        percent_value = ((self._value - self._min) / (self._max - self._min))

        self._value_label.text = str(round(self.value, 3))
        self._range.scale_x = percent_value  # Use scale as to not regenerate a new geometry on every value change
        self._range.x = (percent_value * self._range.geometry.width) / 2


    def update_attributes(self):
        super().update_attributes()
        self._update_slider()

    def update_display_list(self, width, height, depth):
        super().update_display_list(width, height, depth)

        # Layout Stuff
        self._range.geometry.width = self._slider.width
        self._range.geometry.height = self._slider.height
        self._range.y = self._slider.height / 2

        self._track.geometry.width = self._slider.width
        self._track.geometry.height = self._slider.height
        self._track.x = self._slider.width / 2
        self._track.y = self._slider.height / 2

        # Range stuff
        self._update_slider()
