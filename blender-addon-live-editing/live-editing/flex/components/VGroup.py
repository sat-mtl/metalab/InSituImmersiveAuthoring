from ..layouts import VerticalLayout
from . import DirectionalGroupBase


class VGroup(DirectionalGroupBase):
    def __init__(self, gap=0, horizontal_align='LEFT'):
        super().__init__()
        self.layout = VerticalLayout(gap, horizontal_align)

    @property
    def vertical_layout(self):
        return self.layout
