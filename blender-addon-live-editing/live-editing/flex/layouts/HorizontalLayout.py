import math
from ...utils import MathUtils
from .. import Flex, SizesAndLimit
from ...Constants import DEBUG_LAYOUT
from . import DirectionalLayoutBase, LayoutElementChildInfo


class HorizontalLayout(DirectionalLayoutBase):

    @staticmethod
    def calculate_percent_height(layout_element, height):
        return MathUtils.clamp(
            min(round(layout_element.percent_height * height), height),
            layout_element.get_min_bounds_height(),
            layout_element.get_max_bounds_height()
        )

    @staticmethod
    def size_layout_element(layout_element, height, vertical_align, restricted_height, width, variable_column_width, column_width):
        new_height = None

        if vertical_align == 'JUSTIFY':
            new_height = restricted_height
        else:
            if layout_element.percent_height is not None:
                new_height = HorizontalLayout.calculate_percent_height(layout_element, height)

        if variable_column_width:
            layout_element.set_layout_bounds_size(width, new_height, None)
        else:
            layout_element.set_layout_bounds_size(column_width, new_height, None)

    def __init__(self, gap=0, vertical_align='TOP'):
        super().__init__(gap, 'LEFT', vertical_align)
        self._column_width = None
        self._variable_column_width = True

    #   ######   #######  ##       ##     ## ##     ## ##    ##  ######
    #  ##    ## ##     ## ##       ##     ## ###   ### ###   ## ##    ##
    #  ##       ##     ## ##       ##     ## #### #### ####  ## ##
    #  ##       ##     ## ##       ##     ## ## ### ## ## ## ##  ######
    #  ##       ##     ## ##       ##     ## ##     ## ##  ####       ##
    #  ##    ## ##     ## ##       ##     ## ##     ## ##   ### ##    ##
    #   ######   #######  ########  #######  ##     ## ##    ##  ######

    @property
    def column_width(self):
        return self._column_width if self._column_width is not None else 0

    @column_width.setter
    def column_width(self, value):
        if self._column_width != value:
            self._column_width = value
        self.invalidate_target_size_and_display_list()

    @property
    def variable_column_width(self):
        return self._variable_column_width

    @variable_column_width.setter
    def variable_column_width(self, value):
        if self._variable_column_width != value:
            self._variable_column_width = value
            self.invalidate_target_size_and_display_list()

    #  ##     ## ########    ###     ######  ##     ## ########  ########
    #  ###   ### ##         ## ##   ##    ## ##     ## ##     ## ##
    #  #### #### ##        ##   ##  ##       ##     ## ##     ## ##
    #  ## ### ## ######   ##     ##  ######  ##     ## ########  ######
    #  ##     ## ##       #########       ## ##     ## ##   ##   ##
    #  ##     ## ##       ##     ## ##    ## ##     ## ##    ##  ##
    #  ##     ## ######## ##     ##  ######   #######  ##     ## ########

    def measure_real(self, layout_target):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: measure_real", layout_target)

        size = SizesAndLimit()
        justify = self._vertical_align == 'JUSTIFY'
        num_elements = layout_target.num_elements
        num_elements_in_layout = num_elements

        preferred_width = 0.00
        preferred_height = 0.00
        min_width = 0.00
        min_height = 0.00

        fixed_column_width = None if self._variable_column_width else self._column_width

        for i in range(0, num_elements):
            element = layout_target.get_element_at(i)

            if DEBUG_LAYOUT:
                self._logger.open("element", element)

            if element is None or not element.include_in_layout:
                if DEBUG_LAYOUT:
                    if element is None:
                        self._logger.close("no element")
                    elif not element.include_in_layout:
                        self._logger.close("element not included in layout")

                num_elements_in_layout -= 1
                continue

            self.get_element_width(element, fixed_column_width, size)
            preferred_width += size.preferred_size
            min_width += size.min_size

            self.get_element_height(element, justify, size)
            preferred_height = max(preferred_height, size.preferred_size)
            min_height = max(min_height, size.min_size)

            if DEBUG_LAYOUT:
                self._logger.close()

        # Add gaps
        if num_elements_in_layout > 1:
            gap = self._gap * (num_elements_in_layout - 1)
            preferred_width += gap
            min_width += gap

        h_padding = self._padding_left + self._padding_right
        v_padding = self._padding_top + self._padding_bottom

        layout_target.measured_width = preferred_width + h_padding
        layout_target.measured_height = preferred_height + v_padding
        layout_target._measured_min_width = min_width + h_padding
        layout_target._measured_min_height = min_height + v_padding

        if DEBUG_LAYOUT:
            self._logger.close(
                str(layout_target.measured_width) + "/" + str(layout_target._measured_min_width),
                str(layout_target.measured_height) + "/" + str(layout_target._measured_min_height)
            )

    #  ######## ##       ######## ##     ## ######## ##    ## ########  ######
    #  ##       ##       ##       ###   ### ##       ###   ##    ##    ##    ##
    #  ##       ##       ##       #### #### ##       ####  ##    ##    ##
    #  ######   ##       ######   ## ### ## ######   ## ## ##    ##     ######
    #  ##       ##       ##       ##     ## ##       ##  ####    ##          ##
    #  ##       ##       ##       ##     ## ##       ##   ###    ##    ##    ##
    #  ######## ######## ######## ##     ## ######## ##    ##    ##     ######

    def get_element_width(self, element, fixed_column_width, result):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: get_element_width", element, fixed_column_width, result)

        # element_preferred_width = math.ceil(element.explicit_or_measured_width) if fixed_column_width is None else fixed_column_width
        element_preferred_width = element.explicit_or_measured_width if fixed_column_width is None else fixed_column_width
        flexible_width = element.percent_width is not None
        # element_min_width = math.ceil(element.get_min_bounds_width()) if flexible_width else element_preferred_width
        element_min_width = element.get_min_bounds_width() if flexible_width else element_preferred_width
        result.preferred_size = element_preferred_width
        result.min_size = element_min_width

        if DEBUG_LAYOUT:
            self._logger.close(result)

    def get_element_height(self, element, justify, result):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: get_element_height", element, justify, result)

        # element_preferred_height = math.ceil(element.explicit_or_measured_height)
        element_preferred_height = element.explicit_or_measured_height
        flexible_height = element.percent_height is not None or justify
        # element_min_height = math.ceil(element.get_min_bounds_height()) if flexible_height else element_preferred_height
        element_min_height = element.get_min_bounds_height() if flexible_height else element_preferred_height
        result.preferred_size = element_preferred_height
        result.min_size = element_min_height

        if DEBUG_LAYOUT:
            self._logger.close(result)

    #  ##     ##    ###    ##       #### ########     ###    ######## ####  #######  ##    ##
    #  ##     ##   ## ##   ##        ##  ##     ##   ## ##      ##     ##  ##     ## ###   ##
    #  ##     ##  ##   ##  ##        ##  ##     ##  ##   ##     ##     ##  ##     ## ####  ##
    #  ##     ## ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ## ## ##
    #   ##   ##  ######### ##        ##  ##     ## #########    ##     ##  ##     ## ##  ####
    #    ## ##   ##     ## ##        ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
    #     ###    ##     ## ######## #### ########  ##     ##    ##    ####  #######  ##    ##

    def update_display_list_real(self):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: update_display_list_real")

        layout_target = self.target
        target_width = max(0, layout_target.width - self._padding_left - self._padding_right)
        target_height = max(0, layout_target.height - self._padding_top - self._padding_bottom)

        if DEBUG_LAYOUT:
            self._logger.log(
                "layout_target:", layout_target,
                "target_width", target_width,
                "target_height:", target_height
            )

        #layout_element = None
        count = layout_target.num_elements

        container_height = target_height
        # if self._clipAndEnableScrolling and ( self._vertical_align == 'MIDDLE' or self._vertical_align == 'BOTTOM' ):
        #     for i in range(0, count):
        #         layout_element = layout_target.get_element_at( i )
        #         if layout_element is None or not layout_element.include_in_layout:
        #             continue
        #
        #         layout_elementHeight = None
        #         if layout_element.percent_height is not None:
        #             layout_elementHeight = self.calculate_percent_height( layout_element, targetHeight )
        #         else:
        #             layout_elementHeight = layout_element.explicit_or_measured_height
        #
        #         containerHeight = max( containerHeight, math.ceil( layout_elementHeight ) )

        excess_width = self.distribute_width(target_width, target_height, container_height)

        if DEBUG_LAYOUT:
            self._logger.log(
                "container_height:", container_height,
                "excess_width:", excess_width
            )

        v_align = 0
        if self.vertical_align == 'MIDDLE':
            v_align = .5
        elif self._vertical_align == 'BOTTOM':
            v_align = 1

        # As the layout_elements are positioned, we'll count how many rows
        # fall within the layout_target's scrollRect
        # visible_columns = 0
        # min_visible_x = layout_target.horizontalScrollPosition
        # max_visible_x = min_visible_x + target_width

        # Finally, position the layout_elements and find the first/last
        # visible indices, the content size, and the number of
        # visible elements.
        x = self._padding_left
        y0 = self._padding_top
        max_x = self._padding_left
        max_y = self._padding_top
        # first_col_in_view = -1
        # last_col_in_view = -1

        # Take vertical_align into account
        if excess_width > 0:  # or not self._clipAndEnableScrolling:
            h_align = self._horizontal_align
            if h_align == 'CENTER':
                # x = self.padding_left + round(excess_width / 2)
                x = self.padding_left + (excess_width / 2)
            elif h_align == 'RIGHT':
                x = self.padding_left + excess_width

        for index in range(0, count):
            layout_element = layout_target.get_element_at(index)
            if layout_element is None or not layout_element.include_in_layout:
                continue

            # Set the layout element's position
            # dx = math.ceil(layout_element.width)  # Was getPreferredBoundsWidth
            dx = layout_element.width  # Was getPreferredBoundsWidth
            # dy = math.ceil(layout_element.height)  # Was getPreferredBoundsHeight
            dy = layout_element.height  # Was getPreferredBoundsHeight

            y = y0 + (container_height - dy) * v_align
            # In case we have vertical_align.MIDDLE we have to round
            # if v_align == 0.5:
            #     y = round(y)
            layout_element.move(x, y)  # Was setLayoutBoundsPosition

            # Update maxX,Y, first,lastVisibleIndex, and y
            max_x = max(max_x, x + dx)
            max_y = max(max_y, y + dy)
            # if not self._clipAndEnableScrolling or ( ( x < max_visible_x ) and ( ( x + dx ) > min_visible_x ) ) or ( ( dx <= 0 ) and ( ( x == max_visible_x ) or (x == min_visible_x ) ) ) :
            #     visible_columns += 1
            #     if first_col_in_view == -1:
            #         first_col_in_view = last_col_in_view = index
            #     else:
            #         last_col_in_view = index
            x += dx + self.gap

        # setColumnCount(visible_columns)
        # setIndexInView(first_col_in_view, last_col_in_view)

        # Make sure that if the content spans partially over a pixel to the right/bottom,
        # the content size includes the whole pixel.
        # layout_target.set_content_size(math.ceil(max_x + self.padding_right), math.ceil(max_y + self.padding_bottom))
        layout_target.set_content_size(max_x + self.padding_right, max_y + self.padding_bottom)

        if DEBUG_LAYOUT:
            self._logger.close()

    def distribute_width(self, width, height, restricted_height):
        if DEBUG_LAYOUT:
            self._logger.open("VerticalLayout: distribute_width", width, height, restricted_height)

        space_to_distribute = width
        total_percent_width = 0
        child_info_array = []

        # column_width can be expensive to compute
        # cw = 0 if self._variable_column_width else math.ceil(self.column_width)
        cw = 0 if self._variable_column_width else self.column_width
        count = self.target.num_elements
        total_count = count  # number of elements to use in gap calculation

        # If the child is flexible, store information about it in the
        # childInfoArray. For non-flexible children, just set the child's
        # width and height immediately.
        for index in range(0, count):
            layout_element = self.target.get_element_at(index)
            if layout_element is None or not layout_element.include_in_layout:
                total_count -= 1
                continue

            if layout_element.percent_width is not None and self._variable_column_width:
                total_percent_width += layout_element.percent_width
                child_info = LayoutElementChildInfo()
                child_info.layout_element = layout_element
                child_info.percent = layout_element.percent_width
                child_info.min = layout_element.get_min_bounds_width()
                child_info.max = layout_element.get_max_bounds_width()
                child_info_array.append(child_info)
            else:
                self.size_layout_element(layout_element, height, self._vertical_align, restricted_height, None, self.variable_column_width, cw)
                # space_to_distribute -= math.ceil(layout_element.width)  # Was getLayoutBoundsWidth
                space_to_distribute -= layout_element.width  # Was getLayoutBoundsWidth

        if total_count > 1:
            space_to_distribute -= (total_count - 1) * self.gap

        # Distribute the extra space among the flexible children
        if total_percent_width > 0:
            Flex.flex_children_proportionally(width, space_to_distribute, total_percent_width, child_info_array)
            round_off = 0
            for child_info in child_info_array:
                # Make sure the calculated percentages are rounded to pixel boundaries
                # XXX: Probably not anymore since we don't round
                # child_size = round(child_info.size + round_off)
                child_size = child_info.size + round_off
                round_off += child_info.size - child_size
                self.size_layout_element(child_info.layout_element, height, self._vertical_align, restricted_height, child_size, self._variable_column_width, cw)
                space_to_distribute -= child_size

        if DEBUG_LAYOUT:
            self._logger.close()

        return space_to_distribute
