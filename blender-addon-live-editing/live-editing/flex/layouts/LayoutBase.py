from ..utils import Box
from ...Constants import DEBUG_LAYOUT
from ...utils import Logger

class LayoutBase:
    def __init__(self):
        if DEBUG_LAYOUT:
            self._logger = Logger(self)

        self._target = None

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, value):
        if self._target != value:
            self._target = value

    def get_element_bounds(self, index):
        if DEBUG_LAYOUT:
            self._logger.open("LayoutBase: get_element_bounds", index)

        if self._target is None:
            if DEBUG_LAYOUT:
                self._logger.close("no target")
            return None

        n = self._target.num_elements
        if index < 0 or index >= n:
            if DEBUG_LAYOUT:
                self._logger.close("index outside bounds")
            return None

        elt = self._target.get_element_at(index)
        if elt is None or not elt.include_in_layout:
            if DEBUG_LAYOUT:
                if elt is None:
                    self._logger.close("no element")
                elif not elt.include_in_layout:
                    self._logger.close("element not included in layout")
            return None

        elt_x = elt.x  # elt.getLayoutBoundsX()
        elt_y = elt.y  # elt.getLayoutBoundsY()
        elt_z = elt.z
        elt_w = elt.width  # elt.getLayoutBoundsWidth()
        elt_h = elt.height  # elt.getLayoutBoundsHeight()
        elt_d = elt.depth

        if DEBUG_LAYOUT:
            self._logger.close()

        return Box(elt_x, elt_y, elt_z, elt_w, elt_h, elt_d)

    def element_added(self, index):
        pass

    def element_removed(self, index):
        pass

    def measure(self):
        pass

    def update_display_list(self, width, height, depth):
        pass
