import os
import bpy
from operator import itemgetter
import multiprocessing
from multiprocessing import Pool

from hachoir.metadata import extractMetadata
from hachoir.parser import createParser
import hashlib
import json

from .GenericAsset import GenericAsset
from ...utils import BlenderUtils


supported_metadata = ('title', 'creation_date', 'mime_type', 'width', 'height')
save_folder = os.path.join(os.path.expanduser("~"), '.blender-assets')


class AssetIndexer:
    def __init__(self, additional_metadata_keys=list()):
        self._files = list()
        self._unsorted_assets = list()
        self._sorted_assets = dict()
        self._custom_objects_files = list()
        self._custom_objects = list()
        self._blend_files = list()
        self._blend_elements = list(dict())
        self._path = ''
        self.supported_metadata = list(supported_metadata)
        self.supported_metadata.extend(additional_metadata_keys)
        if not os.path.exists(save_folder):
            os.mkdir(save_folder)

    def get_files(self, path):
        if not os.path.exists(path) or not os.path.isdir(path):
            print('The provided path {} is not a valid directory'.format(path))
            return

        # First clear the internal files list
        self._files.clear()
        self._custom_objects_files.clear()
        self._blend_files.clear()

        for root, dirs, files in os.walk(path, followlinks=True):
            for filename in files:
                new_file = os.path.join(root, filename)
                if os.path.isfile(new_file):
                    if filename.endswith('.json'):
                        self._custom_objects_files.append(new_file)
                    elif filename.endswith('.blend'):
                        self._blend_files.append(new_file)
                    else:
                        self._files.append(new_file)
        self._path = path

    @property
    def custom_objects(self):
        return self._custom_objects

    def parse_files(self):
        if not self._path:
            print('No valid path was provided, parsing cancelled.')
            return

        # Clear the internal metadata list
        self._unsorted_assets.clear()

        self._deserialize_metadata()

        # Clear metadata from files that disappeared since last serialization
        self._unsorted_assets = [m for m in self._unsorted_assets if m.path in self._files]

        # Filter new files or files that changed
        files_to_parse = list()
        for f in self._files:
            to_parse = True
            for m in self._unsorted_assets:
                if m.path == f and not m.has_changed:
                    to_parse = False
            if to_parse:
                files_to_parse.append(f)

        try:
            pool = Pool(multiprocessing.cpu_count())
            for f, metadata in pool.map(self._extract_metadata, files_to_parse):
                if not f or not metadata:
                    continue
                self._unsorted_assets.append(self._store_metadata(f, metadata))
        except:
            print('Unable to start processes to extract metadata')

        self._serialize_metadata()
        self._parse_custom_objects_files()
        self._parse_blend_files()

    def _parse_custom_objects_files(self):
        """
        Parses all the json files to check if they are custom objects descriptions
        Needs to be called after get_files() has fetched all the files from the folder.
        """
        self._custom_objects.clear()
        for f in self._custom_objects_files:
            if f.endswith('.json'):
                with open(f, 'r') as json_file:
                    content = json.load(json_file, encoding='utf-8')
                    content_type = content.get('description')
                    if not content_type or content_type != 'custom_objects_metadata':
                        continue
                    objects = content.get('objects')
                    for o in objects:
                        # We need to have the full path in case there are custom objects in sub-folders
                        relative_path = o.get('path')
                        if not relative_path:
                            continue
                        full_path = [p for p in self._files if p.endswith(os.sep + relative_path)]
                        if len(full_path):
                            o['path'] = full_path[0]
                            self._custom_objects.append(o)

    def _parse_blend_files(self):
        self._blend_elements.clear()
        for blend in self._blend_files:
            # print list of materials with users, if present.
            with bpy.data.libraries.load(blend) as (data_from, data_to):
                def parse_element(suffix, element_name):
                    element_path = os.path.join(blend, suffix, element_name)
                    element_struct = dict()
                    element_struct['path'] = element_path
                    element_struct['element_name'] = element_name
                    element_struct['type'] = suffix.lower()
                    element_struct['from_blend'] = True
                    self._blend_elements.append(element_struct)

                if data_from.materials:
                    for material in data_from.materials:
                        parse_element('Material', material)
                        data_to.materials.append(material)
                if data_from.textures:
                    for texture in data_from.textures:
                        parse_element('Texture', texture)
                        data_to.textures.append(texture)
                if data_from.objects:
                    for o in data_from.objects:
                        parse_element('Object', o)
                        data_to.objects.append(o)

        # Filter unwanted objects and their children (recursively)
        to_remove = list()
        for obj in self.objects:
            current_object = bpy.data.objects.get(obj['element_name'])
            if not current_object:
                continue
            if current_object.type == 'CAMERA' or current_object.type == 'LAMP' or current_object.type == 'ARMATURE':
                for o in BlenderUtils.get_children(current_object):
                    to_remove.append(o.name)
                to_remove.append(current_object.name)
            else:
                if current_object.name not in bpy.context.scene.objects:
                    bpy.context.scene.objects.link(current_object)
        for obj in [obj_to_remove for obj_to_remove in self.objects if obj_to_remove['element_name'] in to_remove]:
                self._blend_elements.remove(obj)

    @property
    def materials(self):
        return [elem for elem in self._blend_elements if elem['type'] == 'material']

    @property
    def textures(self):
        return [elem for elem in self._blend_elements if elem['type'] == 'texture']

    @property
    def objects(self):
        return [elem for elem in self._blend_elements if elem['type'] == 'object']

    def _sort_metadata(self):
        self._sorted_assets = dict()

        for m in self.supported_metadata:
            assets = self.sort_assets_by_key(m, self._unsorted_assets)
            if assets:
                self._sorted_assets[m] = assets

    def _get_asset_by_index(self, idx):
        ret = None
        if idx < len(self._unsorted_assets):
            ret = self._unsorted_assets[idx]
        return ret

    @property
    def assets(self):
        return self._unsorted_assets

    def filter_assets_by_key(self, key, value=None, pred=(lambda x, y: x == y)):
        """
        If no value is specified returns all the assets containing the metadata key
        If a value is specified, also checks that the value 'matches' the one from the asset.
        If the predicate pred is specified, use it to define what 'matches' means.
        """
        return [a for a in self._unsorted_assets if a.get_value(key) and (not value or pred(a.get_value(key), value))]

    def _serialize_metadata(self):
        serialized_path = os.path.join(save_folder, self._transform_path(self._path, '-'))

        # We clear the old file if there is one
        if os.path.exists(serialized_path):
            os.remove(serialized_path)

        # Store all the metadata in a serializable dictionary
        serial_dict = dict()
        for m in self._unsorted_assets:
            m.serialize(serial_dict)
        with open(serialized_path, 'w') as save_file:
            save_file.write(json.dumps(serial_dict, sort_keys=True, indent=2, separators=(',', ': ')))

    def _deserialize_metadata(self):
        serialized_path = os.path.join(save_folder, self._transform_path(self._path, '-'))

        if not os.path.exists(serialized_path) or not os.path.isfile(serialized_path):
            return

        with open(serialized_path, 'r') as save_file:
            deserialized_json = json.load(save_file, encoding='utf-8')

        for p, keys in deserialized_json.items():
            self._unsorted_assets.append(GenericAsset(keys, p))

    def _store_metadata(self, path, metadata):
        keys = dict()
        for m in self.supported_metadata:
            if metadata.has(m):
                keys[m] = str(metadata.get(m))
        with open(path, 'rb') as f:
            keys['hash'] = hashlib.md5(f.read()).hexdigest()
        metadata = GenericAsset(keys, path)
        return metadata

    @staticmethod
    def _extract_metadata(path):
        try:
            parser = createParser(path)
        except:
            parser = None
        if not parser:
            return None, None

        with parser:
            try:
                metadata = extractMetadata(parser)
            except Exception as metadata_error:
                metadata = None
                print('Failed to extract metadata from {}: {}'.format(path, metadata_error))
            if not metadata:
                return None, None

        return path, metadata

    @staticmethod
    def sort_assets_by_key(key, assets, value_getter=(lambda x, y: x.get_value(y)), compare=itemgetter(1)):
        metadata_assets = dict()
        for asset in assets:
            value = value_getter(asset, key)
            if value:
                metadata_assets[assets.index(asset)] = value
        # Creates and returns a list of index sorted by the value of a metadata
        return [meta[0] for meta in sorted(metadata_assets.items(), key=compare)]

    @staticmethod
    def _transform_path(path, new_sep):
        return path[1:].replace(os.sep, new_sep)

