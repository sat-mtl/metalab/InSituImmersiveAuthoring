import hashlib


class GenericAsset:
    def __init__(self, keys, path):
        self._metadata = keys
        self._path = path

    def serialize(self, serial_dict):
        serial_dict[self._path] = self._metadata

    @property
    def has_changed(self):
        with open(self._path, 'rb') as f:
            return self._metadata['hash'] != hashlib.md5(f.read()).hexdigest()

    @property
    def path(self):
        return self._path

    @property
    def metadata(self):
        return self._metadata

    def get_value(self, key):
        return self._metadata.get(key)
