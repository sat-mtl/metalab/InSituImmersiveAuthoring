import bpy
from bpy.types import Node, NodeLink
from .LiveEditingBaseNode import LiveEditingBaseNode


class AutomaticInOutTypeNode(LiveEditingBaseNode):
    """Live Editing Base node"""
    bl_idname = 'AutomaticInOutTypeNode'
    bl_label = 'Automatic In Out Type'

    def init(self, context):
        super().init(context)
        self.inputs.new('NodeSocketVirtual', "Input")
        self.outputs.new('NodeSocketVirtual', "Output")

    def update(self):
        super().update()

        input = self.inputs["Input"]
        output = self.outputs["Output"]

        link = input.links[0] if len(input.links) > 0 else None
        if input.is_linked and link is not None and link.is_valid:
            if link.from_socket.bl_idname != output.bl_idname:
                prev_links = output.links
                self.outputs.remove(output)
                output = self.outputs.new(link.from_socket.bl_idname, "Output")

                """
                for link in prev_links:
                    self.insert_link(NodeLink(
                        from_node=self,
                        from_socket=output,
                        valid=True,
                        to_node=link.to_node,
                        to_socket=link.to_socket
                    ))
                """

        elif output.bl_idname != "NodeSocketVirtual":
            self.outputs.remove(output)
            self.outputs.new('NodeSocketVirtual', "Output")
