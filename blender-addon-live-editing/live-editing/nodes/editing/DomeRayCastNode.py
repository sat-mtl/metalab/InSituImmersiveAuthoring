import bpy
import math
from mathutils import Vector, Quaternion
from ..LiveEditingBaseNode import LiveEditingBaseNode


class DomeRayCastNode(LiveEditingBaseNode):
    """Dome Ray Cast Node"""
    bl_idname = 'DomeRayCastNode'
    bl_label = 'Dome Ray Cast'
    bl_icon = 'SNAP_NORMAL'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Dome Location")
        self.inputs.new('NodeSocketFloat', "Dome Radius")
        self.inputs.new('NodeSocketVectorXYZ', "Location")
        self.inputs.new('NodeSocketQuaternion', "Rotation")

        self.outputs.new('NodeSocketBool', "Result")
        self.outputs.new('NodeSocketVectorXYZ', "Dome Intersection")
        self.outputs.new('NodeSocketQuaternion', "Ray Orientation")
        self.outputs.new('NodeSocketString', "Object")
        self.outputs.new('NodeSocketVectorXYZ', "Location")
        self.outputs.new('NodeSocketVectorXYZ', "Normal")

    def solveQuadratic(self, a, b, c):
        discr = b * b - 4 * a * c
        if discr < 0:
            return False, 0, 0
        elif discr == 0:
            x0 = x1 = - 0.5 * b / a
        else:
            q = -0.5 * (b + math.sqrt(discr)) if b > 0 else -0.5 * (b - math.sqrt(discr))
            x0 = q / a
            x1 = c / q

        if x0 > x1:
            x0, x1 = x1, x0

        return True, x0, x1

    def run(self):
        domeLocation = self.getInputValue("Dome Location")
        domeRadius = self.getInputValue("Dome Radius")
        rayLocation = self.getInputValue("Location")
        rayRotation = self.getInputValue("Rotation")

        rayVector = Vector((0.0, 1.0, 0.0))
        rayVector.rotate(rayRotation)

        # Intersect ray with dome
        rayDomeVec = rayLocation - domeLocation
        a = rayVector.dot(rayVector)
        b = 2 * rayVector.dot(rayDomeVec)
        c = rayDomeVec.dot(rayDomeVec) - (domeRadius * domeRadius)
        result, t0, t1 = self.solveQuadratic(a, b, c)
        if not result:
            return self.nothing(True)

        if t0 < 0:
            # if t0 is negative, let's use t1 instead
            t0 = t1
        if t0 < 0:
            # both t0 and t1 are negative
            return self.nothing(True)

        # Find intersection point with dome
        rayVector.length = t0
        domeRayLocation = rayLocation + rayVector
        self.outputs["Dome Intersection"].default_value = domeRayLocation

        # Find vector/rotation pointing from dome center to that intersection point
        domeLoc = domeRayLocation.normalized()
        dirVector = Vector((0.0, 1.0, 0.0))
        axis = dirVector.cross(domeLoc)
        angle = math.acos(dirVector.dot(domeLoc))
        rayRotation = Quaternion(axis, angle)
        self.outputs["Ray Orientation"].default_value = rayRotation

        rayVector = Vector((0.0, 1.0, 0.0))
        rayVector.rotate(rayRotation)

        """result = None
        location = None
        object = None
        normal = None
        if True:
            distance = math.inf

            for obj in bpy.context.scene.objects:
                print(obj.live_editing.lock)
                if obj.type != 'MESH' or not obj.is_visible(bpy.context.scene):
                    continue

                res, loc, norm, index = obj.ray_cast(domeLocation - obj.location, rayVector)
                if not res:
                    continue

                v = obj.location - domeLocation
                len = math.fabs(v.length)
                if len > distance:
                    continue

                object = obj
                location = loc
                normal = norm
                distance = len

            result = object is not None

        else:"""
        result, location, normal, index, object, matrix = bpy.context.scene.ray_cast(domeLocation, rayVector)

        if not result:
            return self.nothing(False)

        self.outputs["Result"].default_value = result
        self.outputs["Object"].default_value = object.name
        self.outputs["Location"].default_value = location
        self.outputs["Normal"].default_value = normal

    def nothing(self, resetDomeValues):
        if resetDomeValues:
            self.outputs["Dome Intersection"].default_value = (0.00, 0.00, 0.00)
            self.outputs["Ray Orientation"].default_value = Quaternion((1.00, 0.00, 0.00, 0.00))

        self.outputs["Result"].default_value = False
        self.outputs["Object"].default_value = ""
        self.outputs["Location"].default_value = (0.00, 0.00, 0.00)
        self.outputs["Normal"].default_value = (0.00, 0.00, 0.00)
