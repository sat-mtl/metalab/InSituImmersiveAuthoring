import bpy
from bpy.props import FloatVectorProperty, BoolProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class ObjectGrabberNode(LiveEditingBaseNode):
    """Object Grabber Node"""
    bl_idname = 'ObjectGrabberNode'
    bl_label = 'Object Grabber'
    bl_icon = 'HAND'

    didNotGrab = BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    isGrabbing = BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    offsetVector = FloatVectorProperty(
        size=3,
        subtype='XYZ',
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    offsetRotation = FloatVectorProperty(
        size=4,
        subtype='QUATERNION',
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Grab")
        self.inputs.new('NodeSocketString', "Object")
        self.inputs.new('NodeSocketVectorXYZ', "Grab Axis Location")
        self.inputs.new('NodeSocketQuaternion', "Grab Axis Rotation")

        self.outputs.new('NodeSocketBool', "Grabbed")
        self.outputs.new('NodeSocketString', "Object")
        self.outputs.new('NodeSocketVectorXYZ', "Location")
        self.outputs.new('NodeSocketQuaternion', "Rotation")

    def run(self):
        # The earliest we can return is with the grabbed or not boolean
        grabbed = self.getInputValue("Grab")
        if not grabbed:
            if self.didNotGrab:
                # Reset failed grab flag
                self.didNotGrab = False

            if self.isGrabbing:
                # Released a grabbed object
                self.outputs["Object"].default_value = ""
                self.outputs["Grabbed"].default_value = False
                self.outputs["Location"].default_value = (0.00, 0.00, 0.00)
                self.outputs["Rotation"].default_value = (1.00, 0.00, 0.00, 0.00)
                self.isGrabbing = False
            return
        elif self.didNotGrab:
            # We can only grab on "rising-edge", holding grab should not continue to try grabbing
            return

        # Capture object only on grab so that we don't lose it when moving
        # This is done first in order to be able to return early
        # and stop running parent nodes as soon as possible
        obj = None
        if not self.isGrabbing:
            objectName = self.getInputValue("Object")
            if not objectName:
                self.didNotGrab = True
                return

            obj = bpy.data.objects.get(objectName)
            if not obj:
                self.didNotGrab = True
                return

            # Set object on first grab
            self.outputs["Grabbed"].default_value = True
            self.outputs["Object"].default_value = objectName

        # We can now grab the other input values since we are now in a grab operation
        grabAxisLocation = self.getInputValue("Grab Axis Location")
        grabAxisRotation = self.getInputValue("Grab Axis Rotation")

        # Save offsets after reading the values from the inputs
        # This is done is a second step in order to be able to return early
        # when there is no object to grab
        if not self.isGrabbing:
            self.offsetVector = obj.matrix_world.translation - grabAxisLocation
            self.offsetVector.rotate(grabAxisRotation.inverted())
            self.offsetRotation = obj.matrix_world.to_quaternion()
            self.offsetRotation.rotate(grabAxisRotation.inverted())

        # We can now save our grabbing state
        self.isGrabbing = True

        location = self.offsetVector.copy()
        location.rotate(grabAxisRotation)
        location += grabAxisLocation
        self.outputs["Location"].default_value = location

        rotation = self.offsetRotation.copy()
        rotation.rotate(grabAxisRotation)
        self.outputs["Rotation"].default_value = rotation
