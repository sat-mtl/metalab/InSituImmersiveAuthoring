import bpy
from ..LiveEditingBaseNode import LiveEditingBaseNode


class LocalToWorldNode(LiveEditingBaseNode):
    """Local to world coordinates Node"""
    bl_idname = 'LocalToWorldNode'
    bl_label = 'Local to World'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Object")
        self.inputs.new('NodeSocketVectorXYZ', "Local Coordinates")

        self.outputs.new('NodeSocketVectorXYZ', "World Coordinates")

    def run(self):
        objectName = self.getInputValue("Object")
        if not objectName:
            self.reset()
            return

        obj = bpy.data.objects.get(objectName)
        if not obj:
            self.reset()
            return

        self.outputs["World Coordinates"].default_value = obj.matrix_world * self.getInputValue("Local Coordinates")

    def reset(self):
        self.outputs["World Coordinates"].default_value = (0.0, 0.0, 0.0)
