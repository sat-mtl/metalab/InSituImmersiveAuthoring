import bpy
from ..LiveEditingBaseNode import LiveEditingBaseNode


class WorldToLocalNode(LiveEditingBaseNode):
    """World to local coordinates Node"""
    bl_idname = 'WorldToLocalNode'
    bl_label = 'World to Local'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketString', "Object")
        self.inputs.new('NodeSocketVectorXYZ', "World Coordinates")

        self.outputs.new('NodeSocketVectorXYZ', "Local Coordinates")

    def run(self):
        objectName = self.getInputValue("Object")
        if not objectName:
            self.reset()
            return

        obj = bpy.data.objects.get(objectName)
        if not obj:
            self.reset()
            return

        self.outputs["Local Coordinates"].default_value = obj.matrix_world.inverted() * self.getInputValue("World Coordinates")

    def reset(self):
        self.outputs["Local Coordinates"].default_value = (0.0, 0.0, 0.0)
