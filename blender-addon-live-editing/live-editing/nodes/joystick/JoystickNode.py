import bpy
import sdl2
from bpy.props import IntProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode

# Initialize SDL2 joysticks
sdl2.SDL_Init(sdl2.SDL_INIT_JOYSTICK)

class JoystickNode(LiveEditingBaseNode):
    """Joystick Node"""
    bl_idname = 'JoystickNode'
    bl_label = 'JoystickNode'
    bl_icon = 'GAME'

    idProperty = IntProperty(
        name="Id",
        description="Joystick ID",
        default=0, min=0, max=15
    )

    deadZoneProperty = IntProperty(
        name="Dead zone",
        description="Joystick dead zone",
        default=0, min=0, max=32768
    )

    joysticks = {}

    def draw_buttons(self, context, layout):
        layout.prop(self, "idProperty")
        layout.prop(self, "deadZoneProperty")

    def preRun(self):
        super().preRun()

        sdl2.SDL_PumpEvents()

        joyId = self.idProperty
        joystick = JoystickNode.joysticks.get(joyId)
        if joystick is None:
            joystick = sdl2.SDL_JoystickOpen(joyId)
            JoystickNode.joysticks[joyId] = joystick

        numAxes = sdl2.SDL_JoystickNumAxes(joystick)
        numButtons = sdl2.SDL_JoystickNumButtons(joystick)
        numHats = sdl2.SDL_JoystickNumHats(joystick)
        numBalls = sdl2.SDL_JoystickNumBalls(joystick)

        if numAxes + numButtons != len(self.outputs):
            self.outputs.clear()

        for axis in range(numAxes):
            name = "Axis_" + str(axis)
            socket = self.outputs.get(name)
            if socket is None:
                socket = self.outputs.new('NodeSocketInt', name)
            value = sdl2.SDL_JoystickGetAxis(joystick, axis)
            if abs(value) > self.deadZoneProperty:
                socket.default_value = value
            else:
                socket.default_value = 0

        for ball in range(numBalls):
            name = "Ball" + str(ball)
            socket = self.outputs.get(name)
            if socket is None:
                socket = self.outputs.new('NodeSocketInt', name)
            socket.default_value = sdl2.SDL_JoystickGetBall(joystick, ball)

        for button in range(numButtons):
            name = "Button_" + str(button)
            socket = self.outputs.get(name)
            if socket is None:
                socket = self.outputs.new('NodeSocketBool', name)
            socket.default_value = sdl2.SDL_JoystickGetButton(joystick, button)

        for hat in range(numHats):
            name = "Hat_" + str(hat)
            socket = self.outputs.get(name)
            if socket is None:
                socket = self.outputs.new('NodeSocketInt', name)
            socket.default_value = sdl2.SDL_JoystickGetHat(joystick, hat)
