hasJoystick = False
try:
    import sdl2
    hasJoystick = True
except ImportError:
    print("Unable to find SDL2 module, joystick nodes won't be available")

if hasJoystick is True:
    from .JoystickNode import JoystickNode
