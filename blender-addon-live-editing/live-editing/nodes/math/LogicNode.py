from bpy.props import EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode

class LogicNode(LiveEditingBaseNode):
    """Logic Node"""
    bl_idname = 'LogicNode'
    bl_label = 'Logic'
    bl_icon = 'LAMP_DATA'

    op = EnumProperty(
        name="Operation",
        description="Logic Operation",
        default="AND",
        items=[
            ("AND", "AND", "A AND B", 1),
            ("OR",  "OR",  "A OR B",  2),
            ("NOT", "NOT", "NOT A", 3),
            ("NAND", "NAND", "A NAND B", 4),
            ("NOR", "NOR", "A NOR B", 5),
            ("XOR", "XOR", "A XOR B", 6),
            ("XNOR", "XNOR", "A XNOR B", 7)
        ]
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Input A")
        self.inputs.new('NodeSocketBool', "Input B")

        self.outputs.new('NodeSocketBool', "Output")

    def draw_buttons(self, context, layout):
        layout.prop(self, "op")

    def run(self):
        a = self.getInputValue("Input A")
        b = self.getInputValue("Input B")

        out = False

        if self.op == "AND":
            out = a and b
        elif self.op == "OR":
            out = a or b
        elif self.op == "NOT":
            out = not a
        elif self.op == "NAND":
            out = not (a and b)
        elif self.op == "NOR":
            out = not(a or b)
        elif self.op == "XOR":
            out = a != b
        elif self.op == "XNOR":
            out = a == b

        self.outputs["Output"].default_value = out
