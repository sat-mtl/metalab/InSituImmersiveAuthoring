from mathutils import Quaternion
from bpy.props import EnumProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class QuaternionOperationNode(LiveEditingBaseNode):
    """Quaternion Operation Node"""
    bl_idname = 'QuaternionOperationNode'
    bl_label = 'Quaternion Operation'
    bl_icon = 'PLUS'

    op = EnumProperty(
        name="Operation",
        description="Quaternion Operation",
        default="ADD",
        items=[
            ("ADD", "A + B", "A plus B", 1),
            ("SUB", "A - B", "A minus B", 2),
            ("MUL", "A * B", "A multiplied by B", 3),
            ("CROSS", "Cross", "Cross product of A and B", 4),
            ("DOT", "Dot", "Dot product of A and B", 5),
            ("NORM", "Normalize", "Normalize A", 6),
            ("INVERT", "Invert", "Invert A", 7),
            ("ROTATE", "Rotate", "Rotate A by B", 8),
            ("ROTATION_DIFFERENCE", "Rotation Difference", "Difference of rotation between A and B", 9)
        ]
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketQuaternion', "Quaternion A")
        self.inputs.new('NodeSocketQuaternion', "Quaternion B")

        self.outputs.new('NodeSocketQuaternion', "Quaternion")

    def draw_buttons(self, context, layout):
        layout.prop(self, "op")

    def run(self):
        out = None

        # A-Only Operations
        a = self.getInputValue("Quaternion A")

        if self.op == "NORM":
            out = a.normalized()
        elif self.op == "INVERT":
            out = a.inverted()
        else:
            # A - B Operations
            b = self.getInputValue("Quaternion B")

            if self.op == "ADD":
                out = a + b
            elif self.op == "SUB":
                out = a - b
            elif self.op == "MUL":
                out = a * b
            elif self.op == "CROSS":
                out = a.cross(b)
            elif self.op == "DOT":
                out = a.dot(b)
            elif self.op == "ROTATE":
                out = a.copy().rotate(b)
            elif self.op == "ROTATION_DIFFERENCE":
                out = a.rotation_difference(b)

        self.outputs["Quaternion"].default_value = out if out is not None else Quaternion((1.0, 0.0, 0.0, 0.0))
