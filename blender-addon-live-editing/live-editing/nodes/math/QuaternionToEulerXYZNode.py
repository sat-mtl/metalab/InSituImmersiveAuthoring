from mathutils import Euler
from ..LiveEditingBaseNode import LiveEditingBaseNode


class QuaternionToEulerXYZNode(LiveEditingBaseNode):
    """Quaternion To Euler XYZ Node"""
    bl_idname = 'QuaternionToEulerXYZNode'
    bl_label = 'Quaternion To Euler XYZ'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketQuaternion', "Quaternion")

        self.outputs.new('NodeSocketVectorEuler', "Euler XYZ")

    def run(self):
        self.outputs["Euler XYZ"].default_value = self.getInputValue("Quaternion").to_euler('XYZ')
