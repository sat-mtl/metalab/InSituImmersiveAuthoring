from ..LiveEditingBaseNode import LiveEditingBaseNode


class SetVectorMagnitudeNode(LiveEditingBaseNode):
    """Set Vector Magnitude Node"""
    bl_idname = 'SetVectorMagnitudeNode'
    bl_label = 'Set Vector Magnitude'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketVectorXYZ', "Vector")
        magnitude = self.inputs.new('NodeSocketFloat', "Magnitude")
        magnitude.default_value = 1.00

        self.outputs.new('NodeSocketVectorXYZ', "Vector")

    def run(self):
        vector = self.getInputValue("Vector").copy()
        vector.length = self.getInputValue("Magnitude")
        self.outputs["Vector"].default_value = vector
