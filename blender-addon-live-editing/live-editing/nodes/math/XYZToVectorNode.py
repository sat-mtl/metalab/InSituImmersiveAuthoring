from ..LiveEditingBaseNode import LiveEditingBaseNode


class XYZToVectorNode(LiveEditingBaseNode):
    """XYZ to Vector Node"""
    bl_idname = 'XYZToVectorNode'
    bl_label = 'XYZ to Vector'
    bl_icon = 'EDIT'

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketFloat', "X")
        self.inputs.new('NodeSocketFloat', "Y")
        self.inputs.new('NodeSocketFloat', "Z")

        self.outputs.new('NodeSocketVectorXYZ', "Vector")

    def run(self):
        vector = self.outputs["Vector"].default_value
        vector[0] = self.getInputValue("X")
        vector[1] = self.getInputValue("Y")
        vector[2] = self.getInputValue("Z")
