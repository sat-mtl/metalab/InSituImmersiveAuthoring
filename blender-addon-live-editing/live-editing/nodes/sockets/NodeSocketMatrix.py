from bpy.types import NodeSocket
from bpy.props import FloatVectorProperty

class NodeSocketMatrix(NodeSocket):
    bl_idname = "NodeSocketMatrix"
    bl_label = "Matrix Node Socket"

    default_value = FloatVectorProperty(
        name="Matrix",
        subtype='MATRIX',
        size=16
    )

    def draw(self, context, layout, node, text):
        layout.label(text)

    def draw_color(self, context, node):
        return 1.0, 0.0, 0.25, 1.00
