from bpy.types import NodeSocket
from bpy.props import FloatVectorProperty

class NodeSocketQuaternion(NodeSocket):
    bl_idname = "NodeSocketQuaternion"
    bl_label = "Quaternion Node Socket"

    default_value = FloatVectorProperty(
        name="Quaternion",
        subtype='QUATERNION',
        size=4
    )

    def draw(self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label(text)
        else:
            layout.prop(self, "default_value", "", expand=False)

    def draw_color(self, context, node):
        return 1.0, 0.5, 0.0, 1.0
