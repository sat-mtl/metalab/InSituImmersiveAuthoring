from mathutils import Vector
from bpy.props import BoolProperty
from ..LiveEditingBaseNode import LiveEditingBaseNode


class CaptureVectorNode(LiveEditingBaseNode):
    """Capture Vector Node"""
    bl_idname = 'CaptureVectorNode'
    bl_label = 'Capture Vector'
    bl_icon = 'EDIT'

    captured = BoolProperty(
        options={'HIDDEN', 'SKIP_SAVE'}
    )

    def init(self, context):
        super().init(context)

        self.inputs.new('NodeSocketBool', "Momentary")
        self.inputs.new('NodeSocketBool', "Capture")
        self.inputs.new('NodeSocketVectorXYZ', "Vector")

        self.outputs.new('NodeSocketBool', "Captured")
        self.outputs.new('NodeSocketVectorXYZ', "Vector")

    def run(self):
        capture = self.getInputValue("Capture")

        if capture and not self.captured:
            self.outputs["Vector"].default_value = self.getInputValue("Vector").copy()
            self.outputs["Captured"].default_value = True
            self.captured = True

        elif not capture and self.captured:
            if self.getInputValue("Momentary"):
                self.outputs["Vector"].default_value = Vector()
                self.outputs["Captured"].default_value = False
            self.captured = False