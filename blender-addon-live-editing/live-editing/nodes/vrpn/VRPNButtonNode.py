import vrpn
import bpy
from bpy.props import StringProperty, BoolProperty
from .VRPNNode import VRPNNode


class VRPNButtonNode(VRPNNode):
    """VRPN Button Node"""
    bl_idname = 'VRPNButtonNode'
    bl_label = 'VRPN Button'
    bl_icon = 'GAME'

    """hostProperty = StringProperty(
        name="Host",
        description="VRPN server address (and port, if necessary)",
        default="localhost"
    )"""

    deviceNameProperty = StringProperty(
        name="Device",
        description="VRPN device name",
        default=""
    )

    connectedProperty = BoolProperty(
        name="VRPN device connected",
        description="VRPN device connected",
        default=False,
        options={'HIDDEN'}
    )

    """
    Static variable used to determine which node is currently being updated in the VRPN callback
    This is because instances are *always* changing in blender so we can't keep node references that
    will eventually be destroyed and segfault blender if we try to access it.
    """
    buttonNodes = {}
    buttons = {}

    def _keepNodeReference(self):
        """ Update node we are calling mainloop for as a static variable,
        required because we can't keep references to nodes as they seem
        to vanish and be replaced anytime blender feels like it """
        VRPNButtonNode.buttonNodes[self.name] = self

    def treeUpdate(self, tree):
        super().treeUpdate(tree)
        self._keepNodeReference()

    def connect(self):
        super().connect()
        host = bpy.context.user_preferences.addons[__package__.split('.')[0]].preferences.vrpn.host  # self.hostProperty
        print("Connecting VRPNButton to host", host)
        button = vrpn.receiver.Button(self.deviceNameProperty + "@" + host)
        button.register_change_handler(self.name, VRPNButtonNode.onButtonPosition)
        VRPNButtonNode.buttons[self.name] = button
        self._keepNodeReference()
        self.connectedProperty = True

    def disconnect(self):
        button = VRPNButtonNode.buttons.get(self.name)
        if button is not None:
            try:
                button.unregister_change_handler(self.name, VRPNButtonNode.onButtonPosition)
            except:
                pass
            del VRPNButtonNode.buttons[self.name]
            del button
        else:
            print("No button to disconnect!")

        self.connectedProperty = False

    def preRun(self):
        super().preRun()
        button = VRPNButtonNode.buttons.get(self.name)
        if button is not None:
            button.mainloop()
        elif self.connectedProperty:
            self.connect()

    def onButtonPosition(userdata, data):
        node = VRPNButtonNode.buttonNodes.get(userdata)
        if node is None:
            print('NoneType Node in VRPNButton callback')
            return

        # Button Press
        name = "Button Pressed " + str(data["button"])
        socket = node.outputs.get(name)
        if socket is None:
            socket = node.outputs.new('NodeSocketBool', name)
        socket.default_value = data["state"] is 1

        # Button Touch
        name = "Button Touched " + str(data["button"])
        socket = node.outputs.get(name)
        if socket is None:
            socket = node.outputs.new('NodeSocketBool', name)
        socket.default_value = data["state"] >= 1
