import bpy
from bpy.types import Operator
from bpy.props import FloatVectorProperty
from mathutils import Vector, Euler
from math import pi


class AddLampOperator(Operator):
    """Add a lamp (3D model and Blender lamp)"""
    bl_idname = "live_editing.lamp_add"             
    bl_label = "Add a lamp and its 3D model"

    location = FloatVectorProperty(
            name="location",
            description="Location of the newly created lamp",
            default=((0.0, 0.0, 0.0))
        )

    rotation = FloatVectorProperty(
            name="rotation",
            description="Rotation of the newly created lamp",
            default=((0.0, 0.0, 0.0))
        )

    def execute(self, context):
        overrider = context.copy()
        for window in context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D' and area.spaces[0].camera == context.scene.camera:
                    for region in area.regions:
                        if region.type == 'WINDOW':
                            overrider['screen'] = screen
                            overrider['scene'] = context.scene
                            overrider['window'] = window
                            overrider['screen'] = screen
                            overrider['area'] = area
                            overrider['region'] = region
                            overrider['active_object'] = None
                            overrider['edit_object'] = None
                            break

        # Add the lamp
        bpy.ops.object.lamp_add(overrider, type='SPOT', location=Vector((0.0, 0.0, 0.0)), rotation=Euler((pi/2.0, 0.0, 0.0), 'XYZ'))
        lamp = context.selected_objects[0]
        lamp.data.shadow_method = 'RAY_SHADOW'

        # Add the lamp object
        bpy.ops.object.add(type='MESH', location=Vector((0.0, 0.0, 0.0)), rotation=Euler((0.0, 0.0, 0.0), 'XYZ'))
        lamp_object = context.selected_objects[0]
        lamp_object.live_editing.non_editable = True
        lamp_object.data = bpy.data.meshes['Model_Lamp']
        lamp_object.name = "Lamp_Helper"
        bpy.ops.object.material_slot_add()
        lamp_object.material_slots[0].material = bpy.data.materials['Mat_Helper_Objects']
        bpy.context.scene.update()

        # Parent the lamp to the object
        lamp.parent = lamp_object

        # Set the position / orientation
        lamp_object.location = self.location
        lamp_object.rotation_euler = Euler(self.rotation, 'XYZ')

        return {'FINISHED'}
