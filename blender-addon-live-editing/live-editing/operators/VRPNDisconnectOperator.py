import bpy
from bpy.types import Operator
from .. import LiveEditor

class VRPNDisconnectOperator(Operator):
    """Connect VRPN"""
    bl_idname = "vrpn.disconnect"
    bl_label = "Disconnect"

    def execute(self, context):
        LiveEditor.instance.disconnect()
        return {'FINISHED'}
