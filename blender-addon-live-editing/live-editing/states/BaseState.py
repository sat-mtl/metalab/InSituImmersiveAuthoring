from transitions.extensions.nesting import NestedState as State
from ..engine import Object3D
from ..utils import Logger
from ..Constants import DEBUG_STATES


class BaseState(State):

    def __init__(self, *args, verb=None, machine=None, trigger=None, **kwargs):
        super().__init__(*args, **kwargs)

        if DEBUG_STATES:
            self._logger = Logger(self)

        self._machine = machine

        # Original name given to the state (before the machine prepends nested state's parent's name)
        self._original_name = kwargs['name']
        # Label/Verb describing the state
        self._verb = verb if verb is not None else self.name
        # Custom trigger for the state
        self._trigger = trigger if trigger is not None else self.verb

        self._cancel_nested_run = True

    @property
    def machine(self):
        return self._machine

    @property
    def original_name(self):
        return self._original_name

    @property
    def verb(self):
        return self._verb

    @property
    def trigger(self):
        return self._trigger

    # region State

    def should_enter(self, event):
        """
        State method that can be used as a transition condition
        Note that you'll still have to specify it in the transition, this is only
        provided as a helper for custom scenarios
        :return: 
        """
        return True

    def enter(self, event):
        super().enter(event)
        if DEBUG_STATES:
            self._logger.log('Enter', self.name)

        self._cancel_nested_run = False

    def exit(self, event):
        super().exit(event)
        if DEBUG_STATES:
            self._logger.log("Exit", self.name)

        self._cancel_nested_run = True

    def always_run_before(self):
        """
        Always run (before the run() method), even when showing a modal
        :return: 
        """

        pass

    def run(self):
        """
        Run the state
        Only executed when not showing a modal
        :return:
        """

        pass

    def always_run_after(self):
        """
        Always run (after the run() method), even when showing a modal
        :return: 
        """

        pass

    def run_nested(self, modal):
        """
        Run the nested state by first running its parent then self
        :return:
        """

        if DEBUG_STATES:
            self._logger.open("Run nested")

        if self.parent is not None:
            self.parent.run_nested(modal)

        if self._cancel_nested_run:
            if DEBUG_STATES:
                self._logger.close("canceled")
            return

        if DEBUG_STATES:
            self._logger.open("always run (before)")

        self.always_run_before()

        if DEBUG_STATES:
            self._logger.close()

        if not modal:
            if DEBUG_STATES:
                self._logger.open("run")

            self.run()

            if DEBUG_STATES:
                self._logger.close()

        if DEBUG_STATES:
            self._logger.open("always run (after)")

        self.always_run_after()

        if DEBUG_STATES:
            self._logger.close()

    def execute(self, modal=False):
        self.run_nested(modal)

    # endregion
