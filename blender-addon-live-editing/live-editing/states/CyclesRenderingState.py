import bpy
import math
from . import BaseEditorState
from mathutils import Euler
from ..components import Cursor
from ..components.menus import EditorRadialMenu


class RenderingMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        def _exit(target):
            self._state.go_back()
            self.close()
        self.add_item("Exit", _exit)

    def on_opening(self):
        super().on_opening()

    def _move(self, matrix_world):
        self.editor.pod.matrix_world = matrix_world


class CyclesRenderingState(BaseEditorState):
    def __init__(self, *args, on_exit, **kwargs):
        super().__init__(
            name='cycles_rendering',
            verb='cycles_render',
            recallable=True,
            **kwargs,
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'grab': "Recall last state"
            },
            'secondary': {
            }
        }

        self._on_exit = on_exit

        # Cursor
        self._cursor = None

        # Menu
        self._menu = RenderingMenu

        # Variables
        self._previous_camera = None
        self._previous_render_engine = None
        self._previous_viewport_shade = None
        self._previous_camera_type = None
        self._has_immersive_view = False

    def enter(self, event):
        super().enter(event)

        # Cursor
        self._cursor = Cursor(selection_chevron=True)
        self._cursor.minimal = True
        self.editor.cursor_manager.cursor = self._cursor

        # Check whether an immersive window is opened
        self._has_immersive_view = False
        for wm in bpy.data.window_managers:
            for window in wm.windows:
                if window.screen.immersive_viewport and window.screen.immersive_viewport.enabled and window.screen.immersive_viewport.active:
                    self._has_immersive_view = True

        # Switch immersive views to Cycles
        for wm in bpy.data.window_managers:
            for window in wm.windows:
                # If immersive viewport is enabled, we disable it temporarily
                # and we set the camera to fisheye
                if self._has_immersive_view:
                    if window.screen.immersive_viewport.enabled and window.screen.immersive_viewport.active:
                        bpy.ops.immersive.enable({
                            'window_manager': wm,
                            'window': window,
                            'screen': window.screen,
                            'area': window.screen.areas[0],
                            'region': None,
                            'scene': window.screen.scene,
                            'blend_data': bpy.context.blend_data,
                            'user_preferences': bpy.context.user_preferences
                        }, 'INVOKE_DEFAULT')

                        if self._has_immersive_view:
                            anchor_object_name = bpy.context.user_preferences.addons["immersive-viewport"].preferences.anchorObject
                            anchor_object = bpy.data.objects.get(anchor_object_name)
                            if anchor_object and anchor_object.type == 'CAMERA':
                                # We copy the pod camera because it is otherwise fixed on the z rotation axis,
                                # preventing to render with the proper angle
                                render_pod_camera = anchor_object.copy()
                                render_pod_camera.name = 'Render Camera'
                                render_pod_camera.data = anchor_object.data.copy()
                                self._previous_camera = window.screen.scene.camera.name
                                bpy.context.scene.objects.link(render_pod_camera)
                                window.screen.scene.camera = render_pod_camera

                        camera = window.screen.scene.camera
                        self._previous_camera_type = camera.data.type
                        camera.data.type = 'PANO'
                        camera.data.cycles.panorama_type = 'FISHEYE_EQUIDISTANT'
                        camera.data.cycles.fisheye_fov = bpy.context.user_preferences.addons['immersive-viewport'].preferences.fieldOfView / 180.0 * math.pi

                        # The camera has to be rotate to match the immersive view from the immersive viewport addon
                        camera.matrix_world = camera.matrix_world * Euler((math.pi / 2.0, - math.pi / 4.0, 0.0)).to_matrix().to_4x4()

                        self._previous_render_engine = window.screen.scene.render.engine
                        window.screen.scene.render.engine = 'CYCLES'
                        for area in window.screen.areas:
                            if area.type != 'VIEW_3D':
                                continue
                            for space in area.spaces:
                                if space.type != 'VIEW_3D':
                                    continue
                                self._previous_viewport_shade = space.viewport_shade
                                space.viewport_shade = 'RENDERED'

                # There is no immersive window: switch all view_3d to Cycles
                else:
                    self._previous_render_engine = window.screen.scene.render.engine
                    window.screen.scene.render.engine = 'CYCLES'
                    for area in window.screen.areas:
                        if area.type != 'VIEW_3D':
                            continue
                        for space in area.spaces:
                            if space.type != 'VIEW_3D':
                                continue
                            self._previous_viewport_shade = space.viewport_shade
                            space.viewport_shade = 'RENDERED'

    def exit(self, event):
        super().exit(event)
        self._cursor.dispose()
        self._cursor = None

        # Switch immersive views back to the previous engine
        for wm in bpy.data.window_managers:
            for window in wm.windows:
                if self._has_immersive_view:
                    if window.screen.immersive_viewport.enabled and window.screen.immersive_viewport.active:
                        window.screen.scene.render.engine = self._previous_render_engine
                        for area in window.screen.areas:
                            if area.type != 'VIEW_3D':
                                continue
                            for space in area.spaces:
                                if space.type != 'VIEW_3D':
                                    continue
                                space.viewport_shade = self._previous_viewport_shade

                        bpy.ops.immersive.enable({
                            'window_manager': wm,
                            'window': window,
                            'screen': window.screen,
                            'area': window.screen.areas[0],
                            'region': None,
                            'scene': window.screen.scene,
                            'blend_data': bpy.context.blend_data,
                            'user_preferences': bpy.context.user_preferences
                        }, 'INVOKE_DEFAULT')

                        if self._previous_camera:
                            if bpy.data.objects.get(self._previous_camera):
                                camera = window.screen.scene.camera
                                window.screen.scene.camera = bpy.data.objects[self._previous_camera]
                                bpy.data.objects.remove(camera, True)
                            self._previous_camera = ""

                else:
                    window.screen.scene.render.engine = self._previous_render_engine
                    for area in window.screen.areas:
                        if area.type != 'VIEW_3D':
                            continue
                        for space in area.spaces:
                            if space.type != 'VIEW_3D':
                                continue
                            space.viewport_shade = self._previous_viewport_shade

    def go_back(self):
        self._on_exit()

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

    def run(self):
        super().run()

        # Trigger state change
        if self.editor.controller2.menu_button.pressed:
            self._on_exit()
            return
