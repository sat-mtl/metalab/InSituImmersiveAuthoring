import bpy
import math
from mathutils import Matrix
from . import BaseEditorState
from ..components import Cursor
from ..components.menus import EditorRadialMenu


CAMERA_TO_POD_ROTATION_OFFSET = Matrix.Rotation(-math.pi/2, 4, 'X')
POD_TO_CAMERA_ROTATION_OFFSET = Matrix.Rotation(math.pi/2, 4, 'X')


class PresentationMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__()

        def _exit(target):
            self._state._on_exit()
            self.close()
        self.add_item("Exit", _exit)

        def _play(target):
            bpy.ops.screen.animation_play()
            self.close()
        self.add_item("Play", _play)

        def _jump_next_keyframe(target):
            current_frame = bpy.context.scene.frame_current
            next_keyframe = None
            next_keyframe_distance = math.inf
            for marker in bpy.context.scene.timeline_markers:
                if marker.frame > current_frame:
                    dist = abs(current_frame - marker.frame)
                    if dist < next_keyframe_distance:
                        next_keyframe = marker.frame
                        next_keyframe_distance = dist
            if next_keyframe is not None:
                bpy.context.scene.frame_current = next_keyframe
            self.close()
        self._next_keyframe = self.add_item("Next Keyframe", _jump_next_keyframe)

        def _jump_end(target):
            bpy.context.scene.frame_current = bpy.context.scene.frame_end
            self.close()
        self.add_item("Goto End", _jump_end)

        def _jump_start(target):
            bpy.context.scene.frame_current = bpy.context.scene.frame_start
            self.close()
        self.add_item("Goto Start", _jump_start)

        def _jump_previous_keyframe(target):
            current_frame = bpy.context.scene.frame_current
            next_keyframe = None
            next_keyframe_distance = math.inf
            for marker in bpy.context.scene.timeline_markers:
                if marker.frame < current_frame:
                    dist = abs(current_frame - marker.frame)
                    if dist < next_keyframe_distance:
                        next_keyframe = marker.frame
                        next_keyframe_distance = dist
            if next_keyframe is not None:
                bpy.context.scene.frame_current = next_keyframe
            self.close()
        self._previous_keyframe = self.add_item("Prev Keyframe", _jump_previous_keyframe)

        def _reverse(target):
            bpy.ops.screen.animation_play(reverse=True)
            self.close()
        self.add_item("Reverse", _reverse)

    def on_opening(self):
        super().on_opening()

    def update_menu(self):
        super().update_menu()


class PresentationState(BaseEditorState):

    def __init__(self, *args, on_exit, **kwargs):
        super().__init__(
            name='presenting',
            verb='present',
            recallable=True,
            **kwargs
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'grab': "Recall last state"
            },
            'secondary': {
                'trigger': "Play/Pause",
                'trackpad': "Move Playhead"
            }
        }

        self._on_exit = on_exit

        # Control parameters
        self.trackpad_epsilon = 0.01  # trackpad update not used if smaller than this

        # Variables
        self.previous_cont2_trackpad_x = 0.0
        self.previous_cont2_trackpad_y = 0.0
        self._last_frame = -1

        # UI
        self._cursor = None

        # Menu
        self._menu = PresentationMenu

    def enter(self, event):
        super().enter(event)

        # Cursor
        self._cursor = Cursor(selection_chevron=False)
        self._cursor.minimal = True
        self.editor.cursor_manager.cursor = self._cursor

    def exit(self, event):
        super().exit(event)

        self._cursor.dispose()
        self._cursor = None

    def always_run_before(self):
        super().always_run_before()

        # Pod follows camera if we are in presentation mode
        if self.editor.selected_object is None:
            self.editor.pod.matrix_world = self.editor.camera.matrix_world * CAMERA_TO_POD_ROTATION_OFFSET

        current_frame = bpy.context.scene.frame_current
        if bpy.context.screen.is_animation_playing and current_frame != self._last_frame:
            # Invert the perceived direction if the number of skipped frames is greater than 100
            # It probably means that the playhead just looped around
            looped = abs(current_frame - self._last_frame) > 100
            forward = current_frame > self._last_frame if not looped else self._last_frame > current_frame

            next_keyframe = None
            next_keyframe_distance = math.inf
            for marker in bpy.context.scene.timeline_markers:
                if marker.frame == current_frame:
                    next_keyframe = marker.frame
                    break

                if (forward and marker.frame > self._last_frame) or (not forward and marker.frame < self._last_frame):
                    dist = abs(current_frame - marker.frame)
                    if dist < next_keyframe_distance:
                        next_keyframe = marker.frame
                        next_keyframe_distance = dist

            if next_keyframe is not None \
               and (
                          (forward and (current_frame >= next_keyframe or (looped and current_frame <= next_keyframe)))
                   or (not forward and (current_frame <= next_keyframe or (looped and current_frame >= next_keyframe)))
               ):
                # We are on a marker, stop playback
                bpy.ops.screen.animation_play()
                bpy.context.scene.frame_current = next_keyframe

        self._last_frame = current_frame

    def run(self):
        super().run()

        # Control

        cont1 = self.editor.controller1
        cont2 = self.editor.controller2

        # Control animation
        if cont2.trackpad_button.touched:
            # Initialize
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            self.previous_cont2_trackpad_y = cont2.trackpad_y

        elif cont2.trackpad_button.touching:
            # Update
            delta = cont2.trackpad_x - self.previous_cont2_trackpad_x
            if abs(delta) >= self.trackpad_epsilon:
                multiplier = (cont2.trackpad_y + 1.0) * 50.0
                bpy.context.scene.frame_current += round(delta * multiplier)
                self.previous_cont2_trackpad_x = cont2.trackpad_x

        # Play/pause
        if cont2.trigger_button.pressed:
            bpy.ops.screen.animation_play()
        elif cont1.trigger_button.pressed:
            bpy.ops.screen.animation_play(reverse=True)

        # Update paths
        # bpy.ops.object.paths_update()
