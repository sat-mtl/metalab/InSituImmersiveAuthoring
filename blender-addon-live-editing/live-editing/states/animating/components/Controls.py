from ....components import Panel
from ....flex.components import HGroup, Button


class Controls(Panel):

    def __init__(self):
        super().__init__()

        self._container = HGroup(vertical_align='JUSTIFY', gap=0.0625)
        self._container.left = 0.5
        self._container.right = 0.5
        self._container.top = 0.5
        self._container.bottom = 0.5
        self.add_element(self._container)

        self.play = Button(label="Play")
        self._container.add_element(self.play)

        self.pause = Button(label="Pause")
        self._container.add_element(self.pause)
