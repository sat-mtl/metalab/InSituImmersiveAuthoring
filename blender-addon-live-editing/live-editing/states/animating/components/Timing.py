from ....components import Panel
from ....flex.components import HGroup, Label


class Timing(Panel):

    def __init__(self):
        super().__init__()

        self._container = HGroup(vertical_align='JUSTIFY')
        self._container.left = 0.5
        self._container.right = 0.5
        self._container.top = 0.5
        self._container.bottom = 0.5
        self.add_element(self._container)

        self.frame = Label(size=.50)
        self.frame.vertical_align = 'MIDDLE'
        self._container.add_element(self.frame)
