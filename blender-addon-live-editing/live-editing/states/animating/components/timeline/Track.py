import math
from .track import Keyframes
from .....engine import Object3D
from .....engine.geometry import Plane
from .....engine.materials import MeshBasicMaterial
from .....flex.components import Component, Label, HGroup
from .....utils import Colors


class Track(HGroup):

    def __init__(self, label):
        super().__init__(vertical_align='MIDDLE', gap=0.25)

        self._label = label

        self._start = 0
        self._end = 1
        self._frame = 0
        self._zoom = 0.5

        self._keyframes = []
        self._keyframes_changed = False

        self._track = None
        self._background = None
        self._needle = None
        self._label_component = None
        self._keyframes_container = None
        # self._keyframe_components = []

        self._range_changed = False
        self._zoomed_range_max_kf = 0
        self._half_range = 0

    def create_children(self):
        super().create_children()

        self._label_component = Label(text=self._label, size=.20)
        self._label_component.width = 1
        self.add_element(self._label_component)

        self._track = Component()
        self._track.height = 0.25
        self._track.percent_width = 1.00
        self.add_element(self._track)

        self._background = Object3D(geometry=Plane(), material=MeshBasicMaterial())
        self._background.material.flat = True
        self._background.material.color = (0.125, 0.125, 0.125)
        self._track.add_child(self._background)

        self._needle = Object3D(geometry=Plane(width=0.025), material=MeshBasicMaterial())
        self._needle.material.flat = True
        self._needle.material.color = Colors.blue_accent
        self._track.add_child(self._needle)

        self._keyframes_container = Keyframes()
        self._keyframes_container.percent_width = 1.00
        self._keyframes_container.percent_height = 1.00
        self._track.add_child(self._keyframes_container)

    # region Properties

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        if self._start != value:
            self._start = value
            self._range_changed = True
            self._dirty_attributes = True

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if self._end != value:
            self._end = value
            self._range_changed = True
            self._dirty_attributes = True

    @property
    def frame(self):
        return self._frame

    @frame.setter
    def frame(self, value):
        if self._frame != value:
            self._frame = value
            self._dirty_attributes = True
            
    @property
    def zoom(self):
        return self._zoom
    
    @zoom.setter
    def zoom(self, value):
        if self._zoom != value:
            self._zoom = value
            self._range_changed = True
            self._dirty_attributes = True

    @property
    def keyframes(self):
        return self._keyframes

    @keyframes.setter
    def keyframes(self, value):
        if self._keyframes != value:
            self._keyframes = value
            self._keyframes_changed = True
            self._dirty_attributes = True

    # endregion

    def update_attributes(self):
        super().update_attributes()

        start = self._start
        end = self._end
        width = self._track.width

        if width == 0:
            return

        # Range
        if self._range_changed:
            self._range_changed = False
            min_resolution = (end - start) / width
            max_resolution = 5  # frames per "meter"
            resolution = min_resolution + ((math.log10(1 + self._zoom * 9)) * (max_resolution - min_resolution))
            self._zoomed_range_max_kf = math.ceil(width * resolution)
            self._half_range = round(self._zoomed_range_max_kf / 2)

        kf_count = self._zoomed_range_max_kf
        half_kf_count = self._half_range
        frame = self._frame
        half_height = self._track.height / 2

        if frame <= half_kf_count:
            start_kf = max(start, (frame - half_kf_count))
            end_kf = start_kf + kf_count
        else:
            end_kf = min((frame + half_kf_count), end)
            start_kf = end_kf - kf_count

        self._needle.x = ((frame - start_kf) / kf_count) * width
        self._needle.visible = start_kf <= frame <= end_kf

        count = 0
        for keyframe in self._keyframes:
            k = keyframe[0]
            if k < start_kf or k > end_kf:
                continue
            obj = self._keyframes_container.get(count, track=self, keyframe=keyframe)
            obj.x = ((k - start_kf) / kf_count) * width
            obj.y = half_height
            count += 1

        self._keyframes_container.clean(count)

    def update_display_list(self, width, height, depth):
        super().update_display_list(width, height, depth)

        self._background.geometry.width = self._track.width
        self._background.geometry.height = self._track.height
        self._background.x = self._track.width / 2
        self._background.y = self._track.height / 2

        self._needle.geometry.height = self._track.height
        self._needle.y = self._track.height / 2

