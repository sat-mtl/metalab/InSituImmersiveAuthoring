import math
from OpenGL.GL import *
from ......engine import Object3D
from ......engine.geometry import Plane
from ......engine.materials import MeshBasicMaterial
from ......utils import Colors
from ......utils.GLUtils import GL8Float


class Keyframe(Object3D):

    last_active = None

    def __init__(self, *args, track, keyframe, **kwargs):
        super().__init__(*args, **kwargs)
        self.rotation_z = math.pi / 4
        self._track = track
        self._keyframe_data = keyframe

    def init_instance(self, *args, track, keyframe, **kwargs):
        """
        Fake init for the object pool
        :param track: 
        :param keyframe:
        :return: 
        """
        self._track = track
        self._keyframe_data = keyframe

    def pre_draw(self):
        # Dirty Cheat Trigger Warning!
        # Since we share the same material for every instance, we have to select the color before drawing
        # or else they will all be the same color ;) We manually call opengl methods here to save processing power
        # when rendering thousands of keyframes on the screen.
        active = self._track.frame == self._keyframe_data[0]
        if active != Keyframe.last_active:
            glUniform4fv(self.self_or_inherited_material.program.data_uniform, 2, GL8Float(*Colors.orange if active else Colors.dark, 1.00 if active else 0.85, 0, 1, 0))
            Keyframe.last_active = active
        super().pre_draw()
