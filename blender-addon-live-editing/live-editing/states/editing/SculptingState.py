import bpy
import math
from time import time
from mathutils import Vector
from .. import BaseEditorState
from ...utils import BrushUtils
from ...components import Cursor
from ...engine import Object3D
from ...engine.geometry.shapes import Circle
from ...engine.materials import LineBasicMaterial


class SculptingState(BaseEditorState):
    def __init__(self, machine, parent=None):
        super().__init__(
            machine=machine,
            parent=parent,
            name='sculpting',
            verb='sculpt'
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'trigger': "Sculpt",
                'grab': "Recall last state"
            },
            'secondary': {
                'trigger': "Select Brush",
                'trackpad': "(x) Radius, (y) Detail Size, (click) Sculpt Mode",
                'menu': "Deselect/Exit"
            }
        }

        # Control parameters
        self.trackpad_epsilon = 0.01  # trackpad update not used if smaller than this

        # Variables
        self.start_time = None
        self.previous_cont2_trackpad_x = 0.0
        self.previous_cont2_trackpad_y = 0.0
        self.sculpt_mode = 'INVERT'
        self.previous_material = None

        self.sculpt_brushes = ['SculptDraw', 'Smooth']
        self.sculpt_brush_index = 0

        self._start_time = None

        # UI
        self._cursor = None
        self._cursor_helper = None
        self._surface_cursor = None
        self._surface_cursor_brush_size_mesh = None
        self._surface_cursor_detail_size_mesh = None

    def should_enter(self, event):
        should = super().should_enter(event)
        can = not self.editor.selected_object.live_editing.non_editable
        return should and can

    def enter(self, event):
        super().enter(event)

        # Here, we have to be sure that an object is selected
        self.editor.selected_object.select = True
        bpy.context.scene.objects.active = self.editor.selected_object

        # Cursor
        self._cursor = Cursor(
            label=self.editor.selected_object.name,
            detail="Sculpt",
            color=(0.15, 0.65, 0.95),
            selection_chevron=True
        )
        self.editor.cursor_manager.cursor = self._cursor

        self._surface_cursor = Object3D()
        self._surface_cursor.visible = False

        self._surface_cursor_brush_size_mesh = Object3D(
            geometry=Circle(
                segments=64,
                radius=0.30,
                line_width=2
            ),
            material=LineBasicMaterial(color=(1.0, 0.34, 0.13))
        )
        self._surface_cursor_brush_size_mesh.y = 0.001
        self._surface_cursor_brush_size_mesh.rotation_x = -math.pi / 2
        self._surface_cursor.add_child(self._surface_cursor_brush_size_mesh)

        self._surface_cursor_detail_size_mesh = Object3D(
            geometry=Circle(
                segments=64,
                radius=0.30,
                line_width=1
            ),
            material=LineBasicMaterial(color=(0.34, 1.0, 0.13))
        )
        self._surface_cursor_detail_size_mesh.y = 0.001
        self._surface_cursor_detail_size_mesh.rotation_x = -math.pi / 2
        self._surface_cursor.add_child(self._surface_cursor_detail_size_mesh)

        self.editor.cursor_layer.add_child(self._surface_cursor)

        # TOOL
        bpy.ops.object.mode_set(mode='SCULPT')

        bpy.context.tool_settings.sculpt.use_symmetry_x = False
        bpy.context.tool_settings.sculpt.use_symmetry_y = False
        bpy.context.tool_settings.sculpt.use_symmetry_z = False

        # Override the brush size with the one saved for the state
        bpy.context.scene.tool_settings.unified_paint_settings.size = bpy.context.scene.state_settings.sculpt_brush_size

    def exit(self, event):
        super().exit(event)

        bpy.ops.object.mode_set(mode='OBJECT')

        self._cursor.dispose()
        self._cursor = None

        self.editor.cursor_layer.remove_child(self._surface_cursor)
        self._surface_cursor.dispose()
        self._surface_cursor = None
        self._surface_cursor_brush_size_mesh = None
        self._surface_cursor_detail_size_mesh = None

    def on_menu_opened(self):
        super().on_menu_opened()
        self._cursor.visible = True
        self._surface_cursor.visible = False

    def on_menu_closed(self):
        super().on_menu_closed()

    def run(self):
        super().run()

        selected_object = self.editor.selected_object
        cont1 = self.editor.controller1
        cont2 = self.editor.controller2

        overrider = bpy.context.copy()
        w = 0
        for window in bpy.context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    if area.width > w:
                        w = area.width
                        for region in area.regions:
                            if region.type == 'WINDOW':
                                overrider['window'] = window
                                overrider['screen'] = screen
                                overrider['area'] = area
                                overrider['region'] = region
                                overrider['object'] = selected_object
                                overrider['active_object'] = selected_object
                                overrider['selected_objects'] = [selected_object]
                                overrider['sculpt_object'] = selected_object
                                break
            # Priority is given to the immersive viewport
            if window.screen.immersive_viewport.enabled:
                break

        brush_location = self.editor.picker.location
        stroke = [
            {
                "name": "first",
                "mouse": (-1.0, -1.0),
                "pen_flip": True,
                "is_start": True,
                "location": brush_location,
                "pressure": 1.0,
                "time": 0.0,
                "size": 1.0
            }]

        #
        # Sculpt options
        #
        if cont2.trackpad_button.touched:
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            self.previous_cont2_trackpad_y = cont2.trackpad_y
        elif cont2.trackpad_button.touching:
            # Set radius
            delta = cont2.trackpad_x - self.previous_cont2_trackpad_x
            if abs(delta) < self.trackpad_epsilon:
                delta = 0.0
            self.previous_cont2_trackpad_x = cont2.trackpad_x
            bpy.context.scene.state_settings.sculpt_brush_size += round(delta * 10)
            bpy.context.scene.state_settings.sculpt_brush_size = max(1, bpy.context.scene.state_settings.sculpt_brush_size)
            bpy.context.scene.tool_settings.unified_paint_settings.size = bpy.context.scene.state_settings.sculpt_brush_size

            # Set detail size
            delta = cont2.trackpad_y - self.previous_cont2_trackpad_y
            if abs(delta) < self.trackpad_epsilon:
                delta = 0.0
            self.previous_cont2_trackpad_y = cont2.trackpad_y
            bpy.context.scene.tool_settings.sculpt.detail_size += round(delta * 10)
            bpy.context.scene.tool_settings.sculpt.detail_size = max(1, bpy.context.scene.tool_settings.sculpt.detail_size)
            # Detail size can't be bigger than brush size
            bpy.context.scene.tool_settings.sculpt.detail_size = min(bpy.context.scene.tool_settings.unified_paint_settings.size, bpy.context.scene.tool_settings.sculpt.detail_size)

        # Add or remove matter
        if cont2.trackpad_button.pressed:
            if self.sculpt_mode == 'NORMAL':
                self.sculpt_mode = 'INVERT'
            else:
                self.sculpt_mode = 'NORMAL'

        # Select brush
        if cont2.trigger_button.pressed:
            self.sculpt_brush_index = (self.sculpt_brush_index + 1) % len(self.sculpt_brushes)
            bpy.context.scene.tool_settings.sculpt.brush = bpy.data.brushes[
                self.sculpt_brushes[self.sculpt_brush_index]]

        # Cursor Update
        if self.editor.picker.object is not None:
            self._cursor.visible = False
            self._surface_cursor_brush_size_mesh.geometry.radius = BrushUtils.brush_radius(
                bpy.context.scene.tool_settings.unified_paint_settings.size,
                (self.editor.picker.location_matrix.to_translation() - self.editor.pod.matrix_world.to_translation()).length
            )
            self._surface_cursor_detail_size_mesh.geometry.radius = BrushUtils.brush_radius(
                bpy.context.scene.tool_settings.sculpt.detail_size,
                (self.editor.picker.location_matrix.to_translation() - self.editor.pod.matrix_world.to_translation()).length
            )
            self._surface_cursor.matrix_world = self.editor.picker.location_matrix.copy()
            self._surface_cursor.visible = True
        else:
            self._cursor.visible = True
            self._surface_cursor.visible = False

        self._cursor.detail = "Add matter" if self.sculpt_mode == 'INVERT' else "Remove matter"
        self._cursor.detail += " - " + self.sculpt_brushes[self.sculpt_brush_index]
        self._cursor.detail += " - Detail/Brush Size: " + str(
            round(bpy.context.scene.tool_settings.sculpt.detail_size, 2)) + "/" + str(
            round(bpy.context.scene.tool_settings.unified_paint_settings.size, 2))

        #
        # Not hovering the object: return
        #
        if not self.editor.picker.object or self.editor.picker.object.name != selected_object.name:
            return

        #
        # Do sculpt
        #
        if cont1.trigger_button.pressed and self.editor.picker.object.name == selected_object.name:
            self.start_time = time()
            if not selected_object.use_dynamic_topology_sculpting:
                bpy.ops.sculpt.dynamic_topology_toggle(overrider)
            bpy.ops.sculpt.brush_stroke(overrider, stroke=stroke, mode=self.sculpt_mode, ignore_background_click=False)

            # We override the object's material, as some materials don't react well with dyntopo
            if len(selected_object.material_slots.items()) > 0:
                self.previous_material = selected_object.material_slots[0].material
                selected_object.material_slots[0].material = bpy.data.materials['Mat_Blank']
        elif self.start_time is not None and cont1.trigger_button.down and self.editor.picker.object.name == selected_object.name:
            current_time = time() - self.start_time
            stroke[0]['is_start'] = False
            stroke[0]['time'] = current_time
            bpy.ops.sculpt.brush_stroke(overrider, stroke=stroke, mode=self.sculpt_mode, ignore_background_click=False)
        elif self.start_time is not None and cont1.trigger_button.released and self.editor.picker.object.name == selected_object.name:
            if selected_object.use_dynamic_topology_sculpting:
                bpy.ops.sculpt.optimize(overrider)
                bpy.ops.sculpt.dynamic_topology_toggle(overrider)
            self._start_time = None

            if self.previous_material is not None:
                selected_object.material_slots[0].material = self.previous_material
                self.previous_material = None
