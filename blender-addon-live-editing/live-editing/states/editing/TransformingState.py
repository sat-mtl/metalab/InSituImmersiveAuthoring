import bpy
import sys
import math
from mathutils import Vector, Euler, Quaternion, Matrix
from mathutils.geometry import intersect_line_line, intersect_line_plane
from OpenGL.GL import *
from .. import BaseEditorState
from ...components import Cursor
from ...engine import Object3D
from ...engine.helpers import TranslationHelper, RotationHelper, ScaleHelper, AxisHelper
from ...utils.KeyframeUtils import euler_filter


class TranslationData:
    def __init__(self):
        self.active = False
        self.scaled_axis = Vector()
        self.scale = Matrix.Identity(4)
        self.location_offset = Vector()
        self.rotation_offset = Quaternion((1.00, 0.00, 0.00, 0.00))
        self.last_controller_distance = 0.00
        self.last_controller_rotation = Quaternion((1.00, 0.00, 0.00, 0.00))
        self.distance_multiplier = 1.00

        self.object_locked_basis_matrix = Matrix.Identity(4)
        self.picking_vector_locked_ortho = Vector()


class RotationData:
    def __init__(self):
        self.rotating_with_controller = False
        self.rotating_with_trackpad = False
        self.last_x = 0.00
        self.last_y = 0.00

    @property
    def active(self):
        return self.rotating_with_controller or self.rotating_with_trackpad


class ScaleData:
    def __init__(self):
        self.active = False
        self.last_controller_distance = 0.00
        self.scale = 1.00


class TransformingState(BaseEditorState):

    def __init__(self, machine, parent=None):
        super().__init__(
            machine=machine,
            parent=parent,
            name='transforming',
            verb='transform'
        )

        self._help_data = {
            'primary': {
                'menu': "Open/Close Menu",
                'trigger': "Move Object",
                'trackpad': "Rotate Object",
                'grab': "Recall last state"
            },
            'secondary': {
                'trigger': "(Both triggers) Move Backwards/Forwards|Scale",
                'grab': "Rotate object with controller",
                'menu': "Deselect/Exit"
            }
        }

        self.translation = TranslationData()
        self.rotation = RotationData()
        self.scale = ScaleData()

        # UI
        self._cursor = None

        # 3D Helpers
        self._axis_helper = None
        self._transformation_helper = None
        self._translation_helper = None
        self._rotation_helper = None
        self._scale_helper = None

    def enter(self, event):
        super().enter(event)

        self.translation = TranslationData()
        self.rotation = RotationData()
        self.scale = ScaleData()

        self._axis_helper = AxisHelper()
        self.editor.engine.scene.add_child(self._axis_helper)

        self._transformation_helper = Object3D()
        self._translation_helper = TranslationHelper()
        self._translation_helper.visible = False
        self._transformation_helper.add_child(self._translation_helper)

        self._rotation_helper = RotationHelper()
        self._rotation_helper.visible = False
        self._transformation_helper.add_child(self._rotation_helper)

        self._scale_helper = ScaleHelper()
        self._scale_helper.visible = False
        self._transformation_helper.add_child(self._scale_helper)

        self.editor.engine.scene.add_child(self._transformation_helper)

        # Main Cursor
        self._cursor = Cursor(
            label="Transform",
            detail=self.editor.selected_object.name,
            color=(0.15, 0.65, 0.95),
            selection_chevron=True
        )
        self.editor.cursor_manager.cursor = self._cursor

    def exit(self, event):
        super().exit(event)

        self.editor.engine.scene.remove_child(self._axis_helper)
        self._axis_helper.dispose()

        self.editor.engine.scene.remove_child(self._transformation_helper)
        self._transformation_helper.dispose()

        self._transformation_helper = None
        self._translation_helper = None
        self._rotation_helper = None
        self._scale_helper = None

        self._cursor.dispose()
        self._cursor = None

    def on_menu_opened(self):
        super().on_menu_opened()

    def on_menu_closed(self):
        super().on_menu_closed()

    def run(self):
        super().run()

        """
        Exit if picking outside of the surface
        """
        if self.editor.picker.ray_vector.length == 0.0:
            return

        """
        Get info about locked axis
        """

        if self.editor.show_axis_ref == 'WORLD':
            locked_axis_local = False
        else:
            locked_axis_local = True

        locked_axis_count = 0
        locked_axis_x = False
        locked_axis_y = False
        locked_axis_z = False

        if self.editor.lock_x_axis:
            locked_axis_count += 1
            locked_axis_x = True
        if self.editor.lock_y_axis:
            locked_axis_count += 1
            locked_axis_y = True
        if self.editor.lock_z_axis:
            locked_axis_count += 1
            locked_axis_z = True

        """
        TRANSLATION
        """

        # region Translation

        # Translation with 1 or 2 axes locked
        if locked_axis_count != 0:
            if self.editor.controller1.trigger_button.pressed and self.editor.picker.object:
                self.translation.active = True
                self.translation.object_world_basis = self.editor.selected_object.matrix_world

            elif self.editor.controller1.trigger_button.released:
                self.translation.active = False

            # Default translation matrix, equals to no translation
            mx = self.editor.selected_object.matrix_world.copy()

            if self.translation.active:
                picking_ray = self.editor.picker.ray_vector_normalized

                if locked_axis_count == 1:
                    pod_matrix = self.editor.pod.matrix_world

                    if locked_axis_x is True:
                        locked_axis = Vector((1.0, 0.0, 0.0))
                    elif locked_axis_y is True:
                        locked_axis = Vector((0.0, 1.0, 0.0))
                    elif locked_axis_z is True:
                        locked_axis = Vector((0.0, 0.0, 1.0))

                    if locked_axis_local is True:
                        locked_axis = self.translation.object_world_basis.to_quaternion().to_matrix() * locked_axis

                    line_first_point = pod_matrix.translation + Vector((0.0, 0.0, 0.0)) # Camera position in its own basis
                    line_second_point = pod_matrix.translation + picking_ray
                    plane_point = self.translation.object_world_basis.translation
                    plane_direction = locked_axis

                    intersection = intersect_line_plane(
                            line_first_point,
                            line_second_point,
                            plane_point,
                            plane_direction
                    )

                    if intersection is not None:
                        mx.translation = intersection

                elif locked_axis_count == 2:
                    pod_matrix = self.editor.pod.matrix_world

                    if locked_axis_x is False:
                        free_axis = Vector((1.0, 0.0, 0.0))
                    elif locked_axis_y is False:
                        free_axis = Vector((0.0, 1.0, 0.0))
                    elif locked_axis_z is False:
                        free_axis = Vector((0.0, 0.0, 1.0))

                    if locked_axis_local is True:
                        free_axis = self.translation.object_world_basis.to_quaternion().to_matrix() * free_axis

                    first_line_first_point = pod_matrix.translation + Vector((0.0, 0.0, 0.0)) # Camera position in its own basis
                    first_line_second_point = pod_matrix.translation + picking_ray
                    second_line_first_point = self.translation.object_world_basis.translation
                    second_line_second_point = self.translation.object_world_basis.translation + free_axis

                    first_intersection, second_intersection = intersect_line_line(
                            first_line_first_point,
                            first_line_second_point,
                            second_line_first_point,
                            second_line_second_point
                    )
                    mx.translation = second_intersection

                mx.translation.x = max(-1000.0, min(1000.0, mx.translation.x))
                mx.translation.y = max(-1000.0, min(1000.0, mx.translation.y))
                mx.translation.z = max(-1000.0, min(1000.0, mx.translation.z))


        # Translation without axis locked
        else:
            if self.editor.controller1.trigger_button.pressed and self.editor.picker.object:
                self.translation.active = True
                self.translation.location_offset = (self.editor.selected_object.matrix_world * self.editor.picker.location) - self.editor.camera_matrix.translation
                self.translation.location_offset.rotate(self.editor.picker.ray_orientation.inverted())
                self.translation.rotation_offset = self.editor.selected_object.matrix_world.to_quaternion()
                self.translation.rotation_offset.rotate(self.editor.controller1.rotation.inverted())
                self.translation.last_controller_rotation = self.editor.controller1.rotation.copy()

                scale_values = self.editor.selected_object.matrix_world.to_scale()
                self.translation.scale[0][0] = scale_values[0]
                self.translation.scale[1][1] = scale_values[1]
                self.translation.scale[2][2] = scale_values[2]
                self.translation.scaled_axis = self.translation.scale * self.editor.picker.location

                self.translation.distance_multiplier = 1.00

            elif self.editor.controller1.trigger_button.released:
                self.translation.active = False

            if self.translation.active:
                location = self.translation.location_offset.copy()
                location.rotate(self.editor.picker.ray_orientation)

                # Push/Pull
                if self.editor.controller2.trigger_button.pressed:
                    self.translation.last_controller_distance = (self.editor.controller1.location - self.editor.controller2.location).length
                elif self.editor.controller2.trigger_button.down:
                    controller_distance = (self.editor.controller1.location - self.editor.controller2.location).length
                    controller_distance_delta = controller_distance - self.translation.last_controller_distance
                    self.translation.last_controller_distance = controller_distance

                    object_distance = (self.editor.selected_object.matrix_world.translation - self.editor.camera_matrix.translation).length
                    self.translation.distance_multiplier += math.log(object_distance + 1) * controller_distance_delta
                    self.translation.distance_multiplier = max(sys.float_info.epsilon, self.translation.distance_multiplier)

                location.length *= self.translation.distance_multiplier
                location += self.editor.camera_matrix.translation - self.translation.scaled_axis

                rotation_delta = self.editor.controller1.rotation * self.translation.last_controller_rotation.inverted()
                self.translation.last_controller_rotation = self.editor.controller1.rotation.copy()
                rotation = self.editor.selected_object.matrix_world.to_quaternion()
                rotation.rotate(rotation_delta)
                rotation = rotation.to_matrix().to_4x4()
                rotation = Matrix.Translation(self.translation.scaled_axis) * rotation * Matrix.Translation(-self.translation.scaled_axis)

                mx = Matrix.Translation(location) * rotation * self.translation.scale
            else:
                # Since translation is an "absolute" matrix, keep the current matrix in case there is no translation
                mx = self.editor.selected_object.matrix_world.copy()

        # endregion

        """
        ROTATION
        """

        # region Rotation

        # Controller rotation trigger
        if self.editor.controller2.grab_button.pressed:
            self.rotation.rotating_with_controller = True
            self.rotation.last_controller_rotation = self.editor.controller2.rotation.copy()
        elif self.editor.controller2.grab_button.released:
            self.rotation.rotating_with_controller = False

        # Trackpad rotation trigger
        if self.editor.controller1.trackpad_button.touched:
            self.rotation.rotating_with_trackpad = True
            self.rotation.last_x = self.editor.controller1.trackpad_x
            self.rotation.last_y = self.editor.controller1.trackpad_y
        elif self.editor.controller1.trackpad_button.untouched:
            self.rotation.rotating_with_trackpad = False

        # Return early if no rotation operation
        if self.rotation.active:
            # Controller/Base rotation
            if self.rotation.rotating_with_controller:
                rotation = self.editor.controller2.rotation.rotation_difference(self.rotation.last_controller_rotation)
                self.rotation.last_controller_rotation = self.editor.controller2.rotation.copy()
                rotation = Quaternion((rotation[0], rotation[1], -rotation[2], rotation[3]))
            else:
                rotation = Quaternion((1.0, 0.0, 0.0, 0.0))

            rotation_matrix = rotation.to_matrix().to_4x4()

            # Trackpad rotation
            if self.rotation.rotating_with_trackpad:
                delta_x = self.editor.controller1.trackpad_x - self.rotation.last_x
                delta_y = self.editor.controller1.trackpad_y - self.rotation.last_y
                self.rotation.last_x = self.editor.controller1.trackpad_x
                self.rotation.last_y = self.editor.controller1.trackpad_y

                if locked_axis_count == 0:
                    trackpad_rotation = Euler((-delta_y, 0.0, delta_x))
                elif locked_axis_count == 1:
                    if locked_axis_x is True:
                        trackpad_rotation = Euler((0.0, delta_y, delta_x))
                    elif locked_axis_y is True:
                        trackpad_rotation = Euler((-delta_y, 0.0, delta_x))
                    elif locked_axis_z is True:
                        trackpad_rotation = Euler((-delta_y, delta_x, 0.0))
                elif locked_axis_count == 2:
                    if locked_axis_x is False:
                        trackpad_rotation = Euler((delta_x, 0.0, 0.0))
                    elif locked_axis_y is False:
                        trackpad_rotation = Euler((0.0, delta_x, 0.0))
                    elif locked_axis_z is False:
                        trackpad_rotation = Euler((0.0, 0.0, delta_x))
                else:
                    trackpad_rotation = Euler((0.0, 0.0, 0.0))

                trackpad_rotation = trackpad_rotation.to_matrix().to_4x4()
                rotation_matrix *= trackpad_rotation

            if locked_axis_count != 0 and locked_axis_local is True: # Rotation in local basis
                rotation_basis = self.editor.selected_object.matrix_world.to_quaternion().to_matrix().to_4x4()
            elif locked_axis_count != 0: # Rotation in world basis
                rotation_basis = Matrix.Identity(4)
            else: # Rotation in camera basis
                rotation_basis = self.editor.picker.ray_orientation.to_matrix().to_4x4()

            translation = Matrix.Translation(mx.translation)

            mx = \
                translation * \
                rotation_basis * \
                rotation_matrix * \
                rotation_basis.inverted() * \
                translation.inverted() * \
                mx

        # endregion

        """
        SCALE
        """

        # region Scale

        if not self.editor.controller1.trigger_button.down:
            if self.editor.controller2.trigger_button.pressed:
                self.scale.active = True
                self.scale.last_controller_distance = (self.editor.controller1.location - self.editor.controller2.location).length
            elif self.editor.controller2.trigger_button.released:
                self.scale.active = False

        if self.scale.active:
            controller_distance = (self.editor.controller1.location - self.editor.controller2.location).length
            controller_distance_delta = controller_distance - self.scale.last_controller_distance
            self.scale.last_controller_distance = controller_distance
            scale_delta = 1 + controller_distance_delta

            if locked_axis_count == 0:
                mx = mx * Matrix.Scale(scale_delta, 4)
            elif locked_axis_count > 0:
                axes = []
                if locked_axis_x is False:
                    axes.append(Vector((1.0, 0.0, 0.0)))
                if locked_axis_y is False:
                    axes.append(Vector((0.0, 1.0, 0.0)))
                if locked_axis_x is False:
                    axes.append(Vector((0.0, 0.0, 1.0)))

                scale_matrix = Matrix.Identity(4)
                for axis in axes:
                    scale_matrix = Matrix.Scale(scale_delta, 4, axis) * scale_matrix

                mx = mx * scale_matrix

        # endregion


        """
        FINAL APPLICATION
        """

        # region Transformation Application & Keyframing

        if self.editor.selected_object.matrix_world != mx:
            record_translation = False
            record_rotation = False
            record_scale = False
            if bpy.context.scene.tool_settings.use_keyframe_insert_auto:
                # Only record keyframes when actively modifying the property and that property changed
                # Otherwise an already animated object would constantly trigger new keyframes
                # Also, rotation can also be triggered from translation
                # because of the way we move the objects in immersive space
                # TODO: This might not make the most fluid animations ever, we'll need a way to transition between edits
                record_translation = self.translation.active and self.editor.selected_object.matrix_world.to_translation() != mx.to_translation()
                record_rotation = (self.translation.active or self.rotation.active) and self.editor.selected_object.matrix_world.to_quaternion() != mx.to_quaternion()
                record_scale = self.scale.active and self.editor.selected_object.matrix_world.to_scale() != mx.to_scale()

            # Now that we are finished comparing current and next values, apply it to the object
            self.editor.selected_object.matrix_world = mx

            # Only after applying the transformation can we insert the keyframes based on our previous comparison
            if bpy.context.scene.tool_settings.use_keyframe_insert_auto:
                """
                Even though we use the built-in method for keyframe capture, we are not 
                using operators to move objects around, so we still have to "manually" create them here
                """
                if record_translation:
                    self.editor.selected_object.keyframe_insert(data_path='location')
                if record_rotation:
                    self.editor.selected_object.keyframe_insert(data_path='rotation_euler')
                    # Filter euler rotation keyframes
                    euler_filter(self.editor.selected_object)
                if record_scale:
                    self.editor.selected_object.keyframe_insert(data_path='scale')

        # endregion

    def always_run_after(self):
        super().always_run_after()

        """
        UI HELPERS
        """

        if self.editor.show_axis:
            self._axis_helper.visible = True
            self._axis_helper.location = self.editor.selected_object.matrix_world.translation.copy()
            if self.editor.show_axis_ref == 'WORLD':
                self._axis_helper.rotation = Euler()
            elif self.editor.show_axis_ref == 'LOCAL':
                self._axis_helper.rotation = self.editor.selected_object.matrix_world.to_euler()
        else:
            self._axis_helper.visible = False
        self._transformation_helper.matrix_world = Matrix.Translation(self.editor.selected_object.matrix_world.translation) * self.editor.selected_object.matrix_world.to_quaternion().to_matrix().to_4x4()

        # 3D Helpers
        # Scale changes continually and we don't care about precision here, so round it off
        # Otherwise it regenerates the helper's geometry on every frame
        scale = round(max(*self.editor.selected_object.matrix_world.to_scale().xyz), 3)
        dimension = scale * max(map((lambda p: Vector(p).length), self.editor.selected_object.bound_box))

        self._axis_helper.inner = dimension
        self._translation_helper.dimension = dimension
        self._rotation_helper.dimension = dimension
        self._scale_helper.dimension = dimension

        self._translation_helper.visible = self.translation.active
        self._rotation_helper.visible = self.rotation.active
        self._scale_helper.visible = self.scale.active
