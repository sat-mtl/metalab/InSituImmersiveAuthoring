class BlenderUtils:
    @staticmethod
    def get_children(obj):
        """
        Recursively get all children
        :param obj: Object for which we want the children
        """
        children = []
        for child in obj.children:
            children.append(child)
            children.extend(BlenderUtils.get_children(child))
        return children
