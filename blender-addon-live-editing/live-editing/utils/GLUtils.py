from OpenGL.GL import GLfloat

GL4Float = (GLfloat * 4)
GL8Float = (GLfloat * 8)
GLMat4x4 = (GLfloat * 16)
