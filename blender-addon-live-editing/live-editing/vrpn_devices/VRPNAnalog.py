import vrpn
from . import VRPNClient


class VRPNAnalog(VRPNClient):

    def __init__(self):
        super().__init__()
        self.channels = []

    def connect(self, host, device_name):
        super().connect(host, device_name)
        print("Connecting VRPNAnalog to host", self.uri)
        self.client = vrpn.receiver.Analog(self.uri)
        self.client.register_change_handler(self, self.on_analog_values)

    def disconnect(self):
        if self.client is not None:
            try:
                self.client.unregister_change_handler(self, self.on_analog_values)
            except:
                pass
        super().disconnect()

    def on_analog_values(self, userdata, data):
        #print(self.channels)
        self.channels = data["channel"]
