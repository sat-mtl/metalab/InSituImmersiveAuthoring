import vrpn
from . import VRPNClient
from ..Constants import DEBUG_VRPN, DEBUG_VRPN_BUTTON


class VRPNButton(VRPNClient):

    def __init__(self):
        super().__init__()
        self.buttons = {}
        self._button_queues = {}
        # self._got_message = False

    def connect(self, host, device_name):
        super().connect(host, device_name)
        print("Connecting VRPNButton to host", self.uri)
        self.client = vrpn.receiver.Button(self.uri)
        self.client.register_change_handler(self, self.on_button_state)

    def disconnect(self):
        if self.client is not None:
            try:
                self.client.unregister_change_handler(self, self.on_button_state)
            except:
                pass
        super().disconnect()

    def before_update(self):
        super().before_update()

        if DEBUG_VRPN and DEBUG_VRPN_BUTTON:
            self._logger.open("before_update - resetting state")

        # Reset button states first because the python vrpn plugin's mainloop
        # is shared across devices and sometimes we get button events when calling
        # the tracker's mainloop. It shouldn't be the case but we have to work with
        # what we got
        for id, button in self.buttons.items():
            if button.pressed:
                button.pressed = False
            if button.touched:
                button.touched = False
            if button.untouched:
                button.untouched = False
            if button.released:
                button.released = False

        # self._got_message = False

        if DEBUG_VRPN and DEBUG_VRPN_BUTTON:
            self._logger.close()

    def update(self):
        super().update()

        for button_id, button_queue in self._button_queues.items():
            if len(button_queue) == 0:
                continue
            state = button_queue.pop(0)

            button = self.buttons.get(button_id)
            if button is None:
                from ..editor.controllers import ButtonState
                button = ButtonState()
                self.buttons[button_id] = button

            pressing = state is 1
            touching = state >= 1

            button.touched = touching and not button.touching
            button.untouched = not touching and button.touching
            button.touching = touching
            button.released = not pressing and (button.pressed or button.down)
            button.pressed = pressing and not button.down
            button.down = pressing
            button.up = not pressing

    def on_button_state(self, userdata, data):
        if DEBUG_VRPN_BUTTON:
            self._logger.open("on_button_state")

        if DEBUG_VRPN_BUTTON:
            self._logger.log("Button:", data["button"], "State:", data["state"])

        # if self._got_message:
        #     # Sometimes two messages come at once and it messes up button releases
        #     # XXX: Now we miss some button presses :(
        #     # TODO: Maybe we should/could cue the next button message?
        #     if DEBUG_VRPN_BUTTON:
        #         self._logger.close("Already received in this loop")
        #
        #     return

        # Add new data to button queue
        button_id = data["button"]
        button_queue = self._button_queues.get(button_id)
        if button_queue is None:
            button_queue = []
            self._button_queues[button_id] = button_queue
        button_queue.append(data["state"])

        # button = self.buttons.get(button_id)
        # if button is None:
        #     from ..editor.controllers import ButtonState
        #     button = ButtonState()
        #     self.buttons[button_id] = button
        #
        # pressing = data["state"] is 1
        # touching = data["state"] >= 1
        #
        # button.touched   = touching and not button.touching
        # button.untouched = not touching and button.touching
        # button.touching  = touching
        # button.released  = not pressing and (button.pressed or button.down)
        # button.pressed   = pressing and not button.down
        # button.down      = pressing
        # button.up        = not pressing

        # if DEBUG_VRPN_BUTTON:
        #     self._logger.log(button)

        # self._got_message = True

        if DEBUG_VRPN_BUTTON:
            self._logger.close()
