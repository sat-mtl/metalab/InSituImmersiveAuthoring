from ..Constants import ENABLE_VRPN
vrpn_enabled = ENABLE_VRPN
if vrpn_enabled:
    try:
        import vrpn as vrpnlib
    except ImportError:
        print("Unable to find VRPN Python module, VRPN devices won't be available")
        vrpn_enabled = False

if vrpn_enabled is True:
    from .VRPNClient import VRPNClient
    from .VRPNTracker import VRPNTracker
    from .VRPNAnalog import VRPNAnalog
    from .VRPNButton import VRPNButton
